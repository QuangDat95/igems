let NewsIgems1 = "template/data/img/NewsIgems1.jpg";
let NewsIgems2 = "template/data/img/NewsIgems2.jpg";
let NewsIgems3 = "template/data/img/NewsIgems3.jpg";
let NewsIgems4 = "template/data/img/NewsIgems4.jpg";
let NewsIgems5 = "template/data/img/NewsIgems5.jpg";
let NewsImg32 = "template/data/img/NewsImg3-2.png";
let NewsImg33 = "template/data/img/NewsImg3-3.png";
let NewsImg34 = "template/data/img/NewsImg3-4.png";
let NewsImg35 = "template/data/img/NewsImg3-5.png";
let NewsImg36 = "template/data/img/NewsImg3-6.png";
let NewsImg37 = "template/data/img/NewsImg3-7.png";
let NewsImg38 = "template/data/img/NewsImg3-8.png";
let NewsImg39 = "template/data/img/NewsImg3-9.png";
let NewsImg310 = "template/data/img/NewsImg3-10.png";
let NewsImg311 = "template/data/img/NewsImg3-11.png";
let NewsImg312 = "template/data/img/NewsImg3-12.png";

export const newsdata = [
  {
    _id: {
      $oid: "60182c83844989490036afeb",
    },
    id: 1,
    title: "Igems chào mừng ngày phụ nữ Việt Nam 20 – 10",
    time: "20-10-2020",
    content: (
      `<div className="FCC">
        <div>ƯU ĐÃI CỰC LỚN NHÂN DỊP KỶ NIỆM NGÀY PHỤ NỮ VIỆT NAM 20/10 </div>
        <br />
        <div>
          Đăng kí khóa học TIẾNG ANH TRỰC TUYẾN 1 KÈM 1 NHẬN QUÀ LIỀN TAY
        </div>
        <br />
        <div>
          Hưởng ứng kỉ niệm ngày phụ nữ Việt Nam 20/10. Trung tâm anh ngữ IGEMS
          dành tặng những món quà vô cùng đặc biệt tới các bạn học sinh và quý
          phụ huynh khi đăng ký khóa học. NHỮNG GIÁ TRỊ CON SẼ NHẬN ĐƯỢC KHI
          ĐĂNG KÝ KHÓA HỌC TẠI IGEMS:
        </div>
        <br />
        <div>Chương trình học đặc biệt, phù hợp với từng học viên.</div>
        <br />
        <div>
          Phương pháp học tập chuẩn Mỹ. Áp dụng bài test tính cách học tập để
          tìm ra phương pháp phù hợp.
        </div>
        <br />
        <div>Huấn luyện viên nhiệt huyết, tận tình với học sinh.</div>
        <br />
        <div>Học phí giảm tới #40%.</div>
        <br />
        <div>MÓN QUÀ ĐẶC BIỆT DÀNH CHO PHỤ HUYNH.</div>
        <br />
        <div>
          Duy nhất cho 10 phụ huynh đăng ký nhân dịp ngày phụ nữ Việt Nam 20/10.
          IGEMS dành tặng các mẹ những món quà thiết thực nhất lên tới
          #2.000.000đ
        </div>
        <br />
        <div>NỒI CHIÊN KHÔNG DẦU</div>
        <br />
        <div>MÁY XAY SINH TỐ</div>
        <br />
        <div>MÁY SẤY TÓC</div>
        <br />
        <div>CHẢO CHỐNG DÍNH</div>
        <br />
        <div>ĐĂNG KÝ NGAY ĐỂ NHẬN NHỮNG PHẦN QUÀ HẤP DẪN NHẤT.</div>
        <br />
        <div>
          Link đăng ký:{" "}
          <a href="https://forms.gle/7WnV5vMjsywRWyYS8">
            https://forms.gle/7WnV5vMjsywRWyYS8
          </a>
        </div>
        <br />
        <div>Hotline: 0344 975 599</div>
        <br />
        <div>
          Website: <a href="http://igems.com.vn">www.igems.com.vn</a>
        </div>
      </div>`
    ),
    img: NewsIgems1,
    promo: true,
  },
  {
    _id: {
      $oid: "60182d1f844989490036afec",
    },
    id: 2,
    title:
      " Tại sao bố mẹ phải định hướng cho con học tiếng Anh theo lộ trình ngay từ nhỏ?",
    time: "20-10-2020",
    content: (
      `<div className="FCC">
        {" "}
        <div>
          {" "}
          Định hướng và cho con học tiếng Anh theo lộ trình ngay từ nhỏ là một
          yếu tố tiên quyết giúp trẻ sớm đạt được thành công. Tuy nhiên hiện nay
          không mấy phụ huynh chú trọng, quan tâm đến vấn đề này dẫn đến việc
          con học tiếng Anh tràn lan nhưng không hiệu quả.{" "}
        </div>{" "}
        <br /> <div>Bố mẹ thân mến,</div>{" "}
        <div>
          {" "}
          Muốn học hay làm bất cứ thứ gì trong cuộc sống luôn cần một phương
          pháp đúng đắn và một lộ trình học, làm đúng đắn: từ nhỏ đến lớn, từ cơ
          bản đến nâng cao.{" "}
        </div>{" "}
        <div>
          {" "}
          Nếu không có lộ trình, trẻ sẽ loay hoay không biết bắt đầu từ đâu,
          không biết mình đang cần gì, phải làm gì? Con sẽ khó thành công hoặc
          nếu có thành công thì tốn rất nhiều thời gian và công sức.{" "}
        </div>{" "}
        <div>
          {" "}
          Do đó, việc định hướng và xây dựng một lộ trình học tiếng Anh phù hợp
          theo từng độ tuổi của trẻ là rất cần thiết và quan trọng. Nó sẽ giúp
          con học đúng trọng tâm và đạt kết quả tốt ngay từ đầu.{" "}
        </div>{" "}
        <br />{" "}
        <div>
          Mỗi giai đoạn trẻ cần có một cách học tiếng Anh khác nhau
        </div>{" "}
        <div>
          {" "}
          Ở giai đoạn Tiểu học, trẻ bắt đầu làm quen với môi trường học tập phổ
          thông. Sự tập trung chú ý cũng chưa cao. Tiếng Anh lại là ngôn ngữ
          khác với ngôn ngữ trẻ sử dụng hàng ngày, vì thế nếu không có phương
          pháp đúng đắn trẻ sẽ dễ mất hứng và ác cảm với môn học này.{" "}
        </div>{" "}
        <div>
          {" "}
          Con nên học tiếng Anh theo đúng tiêu chí “học mà chơi, chơi mà học”.
          Đồng thời, ngoài nghe, nói, đọc, viết; trẻ còn cần các hoạt động khác
          như múa, hát, kể chuyện hay khám quá cuộc sống qua các trải nghiệm
          khoa học thú vị… Ngoài ra, việc bắt đầu xây dựng nền móng cho các hả
          năng kết nối, làm việc nhóm… là cần thiết để trẻ tự tin phát triển sau
          này.{" "}
        </div>{" "}
        <div>
          {" "}
          Giai đoạn THCS có thể coi là thời kỳ chuyển giao. Con đã có thể đón
          nhận tiếng Anh một cách sâu sắc về mặt học thuật sau các nền tảng ban
          đầu. Đây cũng là thời điểm tạo đòn bẩy cho bậc học cao hơn. Chính vì
          thế phụ huynh cần định hướng cụ thể, rõ ràng mục tiêu học tập của con
          để chuẩn bị kĩ lưỡng và đi theo nó. Đặc biệt với các bố mẹ có mong
          muốn cho con đi du học hay apply vào các trường quốc tế, cần tập trung
          rèn luyện ngay lúc này.{" "}
        </div>{" "}
        <div>
          {" "}
          Trẻ cần được thành thục hơn, dạn dĩ hơn để tự tin nói tiếng Anh trước
          đám đông, nâng cao kĩ năng thuyết trình, giao tiếp, nhận biết và xử lý
          vấn đề…Khi phát triển được những kỹ năng này, trẻ sẽ có thêm nhiều khả
          năng để trở thành lãnh đạo.{" "}
        </div>{" "}
        <div>
          {" "}
          Lứa tuổi cấp 3 là lứa tuổi trẻ đã khá hoàn thiện về trí thông minh và
          khả năng tư duy. Đây là giai đoạn con cần tập trung cao nhất cho mục
          tiêu đã đặt ra của mình trước đó. Các kĩ năng sống và học thuật cần
          được phát triển và thành thục ở bậc cao. Trẻ cần biết suy nghĩ nhiều
          chiều, phân tích, tổng hợp, so sánh, nêu khái niệm, đặt câu hỏi… Ngoài
          ra, trẻ cũng phải có thêm các trải nghiệm thiết yếu để trở nên tự tin
          sau này.{" "}
        </div>{" "}
        <div>
          {" "}
          Mỗi giai đoạn bố mẹ cần tìm hiểu và nắm rõ đặc thù lứa tuổi của con để
          có thể cùng con xây dựng lộ trình học tiếng Anh cho phù hợp.{" "}
        </div>{" "}
      </div>`
    ),
    img: NewsIgems2,
  },
  {
    _id: {
      $oid: "60182d22844989490036afed",
    },
    id: 3,
    title: "Top 10 bài hát tiếng Anh hay nhất giúp trẻ học ngoại ngữ nhanh hơn",
    time: "20-10-2020",
    content: (
      `<div className="FCC">
        <div>
          <i>
            Học Anh ngữ thông qua sách vở hay ứng dụng đã trở nên khá phổ biến.
            Tuy nhiên, để tránh được sự nhàm chán, cha mẹ có thể cho trẻ nghe
            các bài hát tiếng Anh hay nhất như một cách học Anh ngữ hiệu quả.
          </i>
        </div>
        <br />
        <div>
          Trẻ em thường thích thú với những thứ có nhiều màu sắc, đáng yêu,
          trong khi các bài giảng trên lớp thì quá khô khan và thiếu hình ảnh
          trực quan sinh động khiến bé khó có cảm hứng trong việc học tiếng Anh.
          Vậy nên, một phương pháp được nhiều{" "}
          <span className="c-bo">trung tâm Anh ngữ cho trẻ em chất lượng</span>{" "}
          áp dụng chính là giúp các em học thông qua những bài nhạc thiếu nhi
          tiếng Anh vui nhộn.
        </div>
        <br />
        <div>
          Các em thường rất nhạy bén với âm nhạc, vì thế nếu biết cách chọn ca
          khúc phù hợp, chắc chắn bé sẽ không chỉ yêu tiếng Anh mà còn tiếp thu
          ngôn ngữ này một cách nhanh chóng và hiệu quả. Hãy cùng IGEMS điểm qua
          top các bài hát tiếng Anh hay nhất để giúp trẻ có thêm một nguồn tài
          liệu học Anh ngữ đáng tin cậy cha mẹ nhé!
        </div>
        <br />
        <b>Alphabet song</b>
        <br />
        <br />
        <div>
          Bảng chữ cái là kiến thức cơ bản nhất để trẻ bắt đầu làm quen với việc
          học tiếng Anh. Thay vì tập cho bé đánh vần từng chữ thông qua sách vở,
          các bậc phụ huynh có thể bậc cho bé nghe một bài nhạc tiếng Anh thiếu
          nhi vui nhộn về bảng chữ cái để có em có được bước đi vững chắc về mặt
          phát âm.
        </div>
        <br />
        <div>
          Bài hát Alphabet Song với giai điệu dễ nghe và thú vị, hứa hẹn mang
          đến cho các một cách học phát âm về chữ cái vừa mới lạ vừa hiệu quả.
        </div>
        <br />
        <img src={NewsIgems3} alt={NewsIgems3} /> <br />{" "}
        <i>
          {" "}
          Bài hát có liên kết từ chữ cái sẽ gần gũi để bé dễ nhớ, dễ liên tưởng
          hơn (Nguồn: YouTube – Mega Fun Kids Songs & Nursery Rhymes){" "}
        </i>{" "}
        <br /> <br /> <b>Bingo song</b> <br /> <br />{" "}
        <div>
          {" "}
          Quý phụ huynh có con nhỏ yêu thích học tiếng Anh không thể nào bỏ qua
          Bingo song – một trong các bài nhạc tiếng Anh thiếu nhi vui nhộn được
          nghe với tần suất cao. Sở hữu lời nhạc đáng yêu, dễ bắt tai chắc chắn
          bé nhà bạn sẽ vô cùng thích thú khi nghe và xem video ca nhạc thiếu
          nhi tiếng Anh nổi tiếng này.{" "}
        </div>{" "}
        <br /> <img src={NewsImg32} alt={NewsImg32} /> <br />{" "}
        <i>Học đánh vần cùng bài hát Bingo (Nguồn: YouTube – Phong Nguyen)</i>{" "}
        <br /> <br /> <b>Five Little Ducks (Bài hát tập đếm số)</b> <br />{" "}
        <br />{" "}
        <div>
          {" "}
          Ngoài tập đánh vần các chữ cái, bài hát tập đếm số cũng là một trong
          những video ca nhạc thiếu nhi tiếng Anh mà bậc cha, mẹ nên để bé nghe
          mỗi ngày. Một gợi ý cho quý phụ huynh đó chính là Five Little Ducks.
          Với giai điệu vui tươi, dễ dàng thu hút được sự chú ý của các bạn nhỏ
          với hình ảnh sinh động trong video, Five Little Ducks trở thành một
          lựa chọn hợp lý cho việc học đếm số bằng tiếng Anh của con trẻ.{" "}
        </div>{" "}
        <br /> <img src={NewsImg33} alt={NewsImg33} /> <br />{" "}
        <i>
          {" "}
          Tập đếm số cùng bài hát Five Little Ducks (Nguồn: YouTube – Super
          Simple Songs-Kids Songs){" "}
        </i>{" "}
        <br /> <br /> <b>Twinkle Twinkle Little Star</b> <br /> <br />{" "}
        <div>
          {" "}
          Danh sách các bài nhạc tiếng Anh thiếu nhi tiếp tục gọi tên Twinkle
          Twinkle Little Star – một bài hát ru phổ biến của nước Anh với phần
          lời là bài thơ của tác giả Jane Taylor được viết đầu thế kỉ 19. Giai
          điệu của bài hát có lẽ đã quá quen thuộc với các em nhỏ, bởi lẽ từ
          ngay khi bước vào học bảng chữ cái tiếng Anh thì nhiều bé đã được làm
          quen ca khúc này.{" "}
        </div>{" "}
        <br /> <img src={NewsImg34} alt={NewsImg33} /> <br />{" "}
        <i>
          {" "}
          Twinkle Twinkle Little Star sẽ giúp bé ngủ ngon hơn (Nguồn: YouTube –
          Super Simple Songs – Kids Songs){" "}
        </i>{" "}
        <br /> <br /> <b>Old MacDonald Had A Farm</b> <br /> <br />{" "}
        <div>
          {" "}
          Bài hát Old MacDonald Had A Farm ra đời vào năm 1917, tạm dịch là
          “trang trại của ông già MacDonald”, mở ra khung cảnh trang trại vùng
          nông thôn nước Mỹ của một ông già tên MacDonald với những loài vật
          khác nhau. Với bài hát này, các bé sẽ học được làm quen và học tên tất
          cả những loài động vật phổ biến trong đời sống hằng ngày.{" "}
        </div>{" "}
        <br /> <img src={NewsImg35} alt={NewsImg35} /> <br />{" "}
        <i>
          {" "}
          Một bài hát về động vật dễ thương cho bé (Nguồn: YouTube – Super
          Simple Songs – Kids Songs){" "}
        </i>{" "}
        <br /> <br /> <b>Bài hát về chủ đề gia đình</b> <br /> <br />{" "}
        <div>
          {" "}
          Ngoài các bài nhạc thiếu nhi tiếng Anh về học tập, phụ huynh cũng nên
          tìm thêm những bài hát đa dạng chủ đề cho bé nghe vì như thế không chỉ
          mở rộng vốn từ vựng của các em mà còn giúp trẻ có thêm kiến thức về
          nhiều thứ xung quanh. Bài hát có chủ đề về gia đình là ví dụ.{" "}
        </div>{" "}
        <br /> <img src={NewsImg36} alt={NewsImg36} /> <br />{" "}
        <i>Bài hát The finger family (Nguồn: YouTube – Muffin Songs)</i> <br />{" "}
        <img src={NewsImg37} alt={NewsImg33} />{" "}
        <i>
          {" "}
          Bài hát Baby shark (Nguồn: YouTube – Pinkfong! Kids’ Songs & Stories){" "}
        </i>{" "}
        <br /> <br /> <b>Bài hát về các mùa trong năm</b> <br /> <br />{" "}
        <div>
          {" "}
          Bên cạnh những bài hát về gia đình trên, IGEMS cũng gửi đến bạn 2 bài
          hát có chủ đề về các mùa trong năm là The Seasons of the Year và
          Seasons Song. Với tiết tấu vui nhộn, 2 bài hát sẽ dễ dàng giúp bé giải
          tỏa căng thẳng và tạo sự hứng khởi sau những giờ học trên lớp. Đồng
          thời, bé cũng có thể nhận biết được thêm những đặc trưng của 4 mùa
          trong năm thông qua 2 bài hát này.{" "}
        </div>{" "}
        <br /> <img src={NewsImg38} alt={NewsImg38} /> <br />{" "}
        <i>Bài hát The Seasons of the Year (Nguồn: YouTube – Netflix Jr.)</i>{" "}
        <br /> <br /> <img src={NewsImg39} alt={NewsImg39} /> <br />{" "}
        <i>Bài hát Seasons Song (Nguồn: YouTube – Have Fun Teaching)</i> <br />{" "}
        <br /> <b>Head Shoulders Knees and Toes</b> <br /> <br />{" "}
        <div>
          {" "}
          Head Shoulders Knees and Toes là bài hát về các bộ phận của cơ thể
          người. Với phần giai điệu vui nhộn và ca từ rất dễ nhớ, quý phụ huynh
          có thể mở bài hát để bé tự học bằng cách bắt chước theo những nhân vật
          trong video, qua đó các em có thể vừa nâng cao trình độ tiếng Anh cũng
          như tập luyện những động tác đơn giản giúp nâng cao sức khỏe.{" "}
        </div>{" "}
        <br /> <img src={NewsImg310} alt={NewsImg310} /> <br />{" "}
        <i>
          {" "}
          Kết hợp động tác với lời bài hát sẽ giúp trẻ nhớ lâu hơn (Nguồn:
          YouTube – Super Simple Songs – Kids Songs){" "}
        </i>{" "}
        <br /> <br /> <b>If You’re Happy</b> <br /> <br />{" "}
        <div>
          {" "}
          Bài hát If You’re Happy xoay quanh những trạng thái của con người: vui
          vẻ, tức giận, lo sợ, buồn ngủ. Lời bài hát kết hợp với hành động sẽ
          khiến bé nhanh chóng hòa nhập được với bài hát.{" "}
        </div>{" "}
        <br /> <img src={NewsImg311} alt={NewsImg311} /> <br />{" "}
        <i>
          {" "}
          Vừa học vừa thư giãn cùng âm nhạc bé sẽ tiếp thu nhanh hơn (Nguồn:
          YouTube – Super Simple Songs – Kids Songs){" "}
        </i>{" "}
        <br /> <br /> <b>Rain, Rain, Go Away</b> <br /> <br />{" "}
        <div>
          {" "}
          Bài hát có giai điệu và hình ảnh rất dễ thương, giúp bé nhớ được một
          số từ vựng về các thành viên trong gia đình, thời tiết và làm quen với
          cấu trúc “want to V”.{" "}
        </div>{" "}
        <br /> <img src={NewsImg312} alt={NewsImg312} /> <br />{" "}
        <i>
          {" "}
          Bài hát về chủ đề thời tiết (Nguồn: YouTube – ChuChu TV Nursery Rhymes
          & Kids Songs){" "}
        </i>{" "}
        <br /> <br />{" "}
        <div>
          {" "}
          Ngoài việc cho các bé đi học tiếng Anh từ sớm ở các trung tâm, quý phụ
          huynh cũng nên tìm kiếm những phương pháp dạy tiếng Anh cho trẻ em để
          có thể đồng hành hỗ trợ thêm cho bé. Hy vọng 10 bài hát tiếng Anh hay
          nhất cho trẻ em mà IGEMS giới thiệu sẽ giúp các bậc cha mẹ dễ dàng hơn
          trong việc học tiếng Anh cùng con mình.{" "}
        </div>{" "}
      </div>`
    ),
    img: NewsIgems3,
  },
  {
    _id: {
      $oid: "60182d24844989490036afee",
    },
    id: 4,
    title: "ƯU ĐÃI ĐẶC BIỆT ”KHÓA 7 BUỔI ĐÁNH THỨC TIỀM NĂNG TIẾNG ANH”",
    time: "20-10-2020",
    content: (
      `<div className="FCC">
        {" "}
        <div>
          Hé lộ khóa học “ĐÁNH THỨC TIỀM NĂNG NGÔN NGỮ” sau #7 buổi học
        </div>{" "}
        <br />{" "}
        <div>
          {" "}
          Mỗi bạn nhỏ đều có tiềm năng ngôn ngữ, và sứ mệnh của thầy cô, bố mẹ
          là đồng hành cùng con đánh thức tiềm năng của bản thân.{" "}
        </div>{" "}
        <br />{" "}
        <div>
          {" "}
          Tại khóa học “ĐÁNH THỨC TIỀM NĂNG NGÔN NGỮ” con nhận được những gì???{" "}
        </div>{" "}
        <br /> <div>CỦNG CỐ PHÁT ÂM</div> <br />{" "}
        <div>PHÁT TRIỂN TỐI ĐA KỸ NĂNG GIAO TIẾP.</div> <br />{" "}
        <div>TỰ TIN XÂY DỰNG MỘT BÀI THUYẾT TRÌNH.</div> <br />{" "}
        <div>
          {" "}
          XÂY DỰNG NIỀM ĐAM MÊ HỌC TẬP TIẾNG ANH Tại sao bố mẹ không nên bỏ lỡ
          khóa học “ĐÁNH THỨC TIỀM NĂNG” cho các bạn nhỏ thân yêu.{" "}
        </div>{" "}
        <br />{" "}
        <div>
          {" "}
          LỘ TRÌNH BÀI GIẢNG ĐƯỢC THIẾT KẾ CÁ NHÂN HÓA PHÙ HỢP VỚI TỪNG HỌC
          SINH.{" "}
        </div>{" "}
        <br /> <div>
          CỦNG CỐ, PHÁT TRIỂN TỐI ĐA KHẢ NĂNG NGÔN CỦA BÉ
        </div> <br />{" "}
        <div>
          {" "}
          THỜI GIAN, KHÔNG GIAN HỌC TẬP LINH HOẠT. BỐ MẸ TIẾT KIỆM THỜI GIAN ĐƯA
          ĐÓN CON.{" "}
        </div>{" "}
        <br /> <div>
          TIẾT KIỆM CHI PHÍ GẤP 5 LẦN SO VỚI HỌC TẠI TRUNG TÂM.
        </div>{" "}
        <br />{" "}
        <div>
          ƯU ĐÃI LỚN NHẤT CHO CÁC BẠN HỌC SINH ĐĂNG KÝ KHÓA HỌC TRƯỚC 30/9/2020.
          HỌC PHÍ CHỈ CÒN #498.000đ. Bố mẹ nhanh tay đăng ký khóa học cho con
          nhé.{" "}
        </div>{" "}
        <br /> <div>Hotline: 034 497 5599</div> <br />{" "}
        <div>
          {" "}
          Website: <a href="http://igems.com.vn">www.igems.com.vn</a>{" "}
        </div>{" "}
      </div>`
    ),
    img: NewsIgems4,
    promo: true,
  },
  {
    _id: {
      $oid: "xxcz182d24844989490036afee",
    },
    id: 5,
    title:
      "Cuộc thi thuyết trình ”MY WONDER WOMEN” chào mừng ngày Quốc tế Phụ nữ 8 - 3",
    time: "4-3-2021",
    content: (
      `<div className="FCC">
        {" "}
        <div>
          ✨✨ Các anh, chị phụ huynh của <b>IGEMS</b>thân mến! ✨✨
        </div>
        <hr />
        <div>
          Để chào mừng ngày Quốc tế Phụ nữ 8 - 3 sắp tới, trung tâm Anh ngữ trực
          tuyến <b>IGEMS</b> sẽ phát động CUỘC THI TÀI NĂNG dành cho tất cả các
          bạn học sinh với chủ đề “
          <b>
            <i>MY WONDER WOMEN</i>
          </b>
          ” nhằm tôn vinh hình ảnh người phụ nữ, đồng thời tạo cơ hội để các bạn
          học sinh thể hiện khả năng tiếng Anh của bản thân, và bày tỏ cảm nhận
          của mình với người phụ nữ mà con yêu mến!
        </div>{" "}
        <br />{" "}
        <div>
          Đây là cơ hội tuyệt vời để con thể hiện bản thân, trau dồi khả năng
          tiếng Anh và hình thành sự tự tin. Vì vậy mong các quý vị phụ huynh
          cùng phối hợp với giáo viên của con để hỗ trợ con hoàn thành xuất sắc
          phần thi của mình.
        </div>{" "}
        <hr />
        <div>
          ✨✨Để được hỗ trợ tốt nhất quý phụ huynh vui lòng liên hệ theo:
        </div>{" "}
        <br />{" "}
        <div>
          Hotline: <span class="c-bo">0325 135 112</span>
        </div>{" "}
        <br />{" "}
        <div>
          Hoặc Website: <a href="http://igems.com.vn">igems.com.vn</a>
        </div>
        <div>#IGEMS #ENGLISH #COACHING</div>
      </div>`
    ),
    img: NewsIgems5,
    promo: false,
  },
];
