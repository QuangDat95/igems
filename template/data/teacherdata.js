let T1 = "./template/data/img/T1.jpg";
let T2 = "./template/data/img/T2.jpg";
let T3 = "./template/data/img/T3.jpg";
let T4 = "./template/data/img/T4.jpg";
let T5 = "./template/data/img/T5.jpg";
let T6 = "./template/data/img/T6.jpg";
let T7 = "./template/data/img/T7.jpg";
let T8 = "./template/data/img/T8.jpg";
let T9 = "./template/data/img/T9.jpg";
let T10 = "./template/data/img/T10.jpg";
let T11 = "./template/data/img/T11.jpg";
let T12 = "./template/data/img/T12.jpg";
let T13 = "./template/data/img/T13.jpg";
let T14 = "./template/data/img/T14.jpg";
let T15 = "./template/data/img/T15.jpg";
let T16 = "./template/data/img/T16.jpg";
let T17 = "./template/data/img/T17.jpg";

export const teacherdata = [
  {
    _id: {
      $oid: "6013d678962340495ce0d08c",
    },
    id: 1,
    name: "Megan Krause",
    infor: "Cử nhân trường Design School of Southern Africa",
    img: T1,
    more: ["", "", ""],
  },
  {
    _id: {
      $oid: "6013d855962340495ce0d091",
    },
    id: 2,
    name: "Trần Ngọc Sơn",
    infor: "Cử nhân ngôn ngữ Anh - Đại học ngoại ngữ Hà Nội",
    img: T3,
    more: ["", "", ""],
  },
  {
    _id: {
      $oid: "6013d8e1962340495ce0d092",
    },
    id: 3,
    name: "Lê Nguyễn Anh Thư",
    infor: "Cử nhân khoa ngôn ngữ Anh - Trường Đại học Nha Trang",
    img: T2,
    more: [
      "Ngôn ngữ Anh – Trường Đại học Nha Trang",
      "1 năm kinh nghiệm làm việc với các bạn nhỏ",
      "Sở thích của mình là đọc sách, nghe nhạc và đặc biệt mình yêu thích công việc giảng dạy cho các bạn nhỏ, mình thích làm quen và tìm hiểu xem các bạn ấy đang quan tâm điều gì về tiếng Anh để từ đó bằng khả năng của mình sẽ giúp các bạn ấy phát triển và phát huy tối đa thế mạnh, sự tự tin và năng lực của các bạn ấy. Cảm giác yêu thích là khi đứa trẻ mà mình nhiệt tình giảng dạy đạt được thành công trong việc học.",
    ],
  },
  {
    _id: {
      $oid: "6013da27962340495ce0d093",
    },
    id: 4,
    name: "Võ Trần Thanh Phương",
    infor: "",
    img: T4,
    more: ["", "", ""],
  },
  {
    _id: {
      $oid: "6018f894700a2d3e2c91e10f",
    },
    id: 5,
    name: "Tô Thị Thu Hà",
    infor: "...",
    img: T5,
    more: ["...", "...", "..."],
  },
  {
    _id: {
      $oid: "6018f897700a2d3e2c91e110",
    },
    id: 6,
    name: "Trần Thảo Nhi",
    infor: "Củ nhân đại học Tôn Đức Thắng",
    img: T6,
    more: ["...", "...", "..."],
  },
  {
    _id: {
      $oid: "6018f898700a2d3e2c91e111",
    },
    id: 7,
    name: "Lam Quỳnh",
    infor: "...",
    img: T7,
    more: ["...", "...", "..."],
  },
  {
    _id: {
      $oid: "6018f89a700a2d3e2c91e112",
    },
    id: 8,
    name: "Steven Di Pietrantonio",
    infor: "...",
    img: "xxx.jpg",
    more: ["...", "...", "..."],
  },
  {
    _id: {
      $oid: "6018f89b700a2d3e2c91e113",
    },
    id: 9,
    name: "Dominic Ruegg",
    infor: "Cử nhân trường Emmanuel Christian Academy",
    img: T8,
    more: ["...", "...", "..."],
  },
  {
    _id: {
      $oid: "6018f89d700a2d3e2c91e114",
    },
    id: 10,
    name: "Tạ THu Hiền",
    infor:
      "Cử nhân khoa Tiếng Anh Thương mại - Trường Đại học Ngoại Thương Hà Nội",
    img: T9,
    more: [
      "Khoa Tiếng Anh Thương mại (Ngôn ngữ Anh) – Trường Đại học Ngoại Thương Hà Nội",
      "4 năm kinh nghiệm giảng dạy tiếng Anh các cấp cho người mất gốc, chương trình của Bộ GD-ĐT đến các chương trình quốc tế hệ của Cambridge và Oxford",
      "Sở thích: Tham gia các hoạt động ngoại khoá, tình nguyện, cộng tác viên của các chương trình, tổ chức quốc tế, đặc biệt là làm việc với trẻ em",
    ],
  },
  {
    _id: {
      $oid: "6018f89f700a2d3e2c91e115",
    },
    id: 11,
    name: "Hồ Thị Phương Dung",
    infor: "Cử nhân ngôn ngữ Anh - Trường đại học FPT",
    img: T10,
    more: [
      "Ngôn ngữ Anh trường đại học Fpt",
      "Có 1 năm kinh nghiệm dạy tiếng anh cho học sinh",
      "Sở thích học từ vựng tiếng anh, nghe nhạc tiếng anh, học ngoại ngữ mới như Trung, Hàn",
    ],
  },
  {
    _id: {
      $oid: "6018f8a1700a2d3e2c91e116",
    },
    id: 12,
    name: "Võ Trần Cẩm Duyên",
    infor: "Cử nhân Ngôn Ngữ Anh - Trường ĐH Nha Trang",
    img: T11,
    more: [
      "Cử nhân Ngôn Ngữ Anh Trường đại học Nha Trang",
      "1 năm kinh nghiệm làm việc với các bạn nhỏ",
      "Nghe nhạc và xem phim tiếng anh",
    ],
  },
  {
    _id: {
      $oid: "6018f8a3700a2d3e2c91e117",
    },
    id: 13,
    name: "Trần Hà Thi",
    infor: "Cử nhân khoa ngôn ngữ anh đại học ngoại ngữ Hà Nội",
    img: T12,
    more: ["...", "...", "..."],
  },
  {
    _id: {
      $oid: "6018f8a5700a2d3e2c91e118",
    },
    id: 14,
    name: "Phạm Ngọc Anh",
    infor:
      "Cử nhân Khoa ngôn ngữ Anh - Đại học ngoại ngữ - Đại học quốc gia Hà Nội",
    img: T13,
    more: [
      "1. Khoa Sư Phạm Tiếng Anh, định hướng quốc tế học, Trường Đại học Ngoại ngữ, ĐH Quốc Gia Hà Nội",
      "2. Kinh nghiệm 3 năm làm việc với trẻ nhỏ (trợ giảng, gia sư, quản lí đầu vào)",
      "3. Sở thích: tâm lý học, khoa học xã hội, nghiên cứu về các cộng đồng châu Á ở hải ngoại",
    ],
  },
  {
    _id: {
      $oid: "6018f8a7700a2d3e2c91e119",
    },
    id: 15,
    name: "Nguyễn Kiều Ngọc Diệp",
    infor: "Cử nhân khoa tiếng Anh - Học viện báo chí",
    img: T14,
    more: ["", "", ""],
  },
  {
    _id: {
      $oid: "6018f8a8700a2d3e2c91e11a",
    },
    id: 16,
    name: "Trần Thị Đăng Trâm",
    infor: "Cử nhân khoa tiếng Anh - Đại học Tôn Đức Thắng",
    img: T15,
    more: ["", "", ""],
  },
  {
    _id: {
      $oid: "602c0e9e317d1523182a343b",
    },
    id: 17,
    name: "Nguyễn Ngọc Xuân Anh",
    infor: "Cử nhân Khoa ngôn ngữ anh đại học Đồng Nai",
    img: T16,
    more: [
      "Khoa ngôn ngữ anh đại học Đồng Nai.",
      "1 năm kinh nghiệm dạy học lứa tuổi từ 5 đến 18 bao gồm dạy phát âm, giao tiếp, ngữ pháp cho trẻ nhỏ cũng như ngữ pháp theo chương trình bộ giáo dục và ôn thi cho học sinh vào các trường điểm, đi thi học sinh giỏi.",
      "Sở thích: dùng nhiều thời gian cho học sinh của mình và nhìn thấy được những tiến bộ rõ nét sau 1 quá tình học tập.",
    ],
  },
  {
    _id: {
      $oid: "602c0ea5317d1523182a343c",
    },
    id: 18,
    name: "Lý Vân Anh",
    infor: "...",
    img: T17,
    more: ["", "", ""],
  },
  {
    _id: {
      $oid: "602c0e9e317d1523182a311c",
    },
    id: 17,
    name: "Lò Thị Loan",
    infor: "Cử nhân Khoa ngôn ngữ anh đại học Đồng Nai",
    img: T16,
    more: [
      "Khoa ngôn ngữ anh đại học Đồng Nai.",
      "1 năm kinh nghiệm dạy học lứa tuổi từ 5 đến 18 bao gồm dạy phát âm, giao tiếp, ngữ pháp cho trẻ nhỏ cũng như ngữ pháp theo chương trình bộ giáo dục và ôn thi cho học sinh vào các trường điểm, đi thi học sinh giỏi.",
      "Sở thích: dùng nhiều thời gian cho học sinh của mình và nhìn thấy được những tiến bộ rõ nét sau 1 quá tình học tập.",
    ],
  },
];
