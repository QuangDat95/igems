import { teacherdata } from "./data/teacherdata.js";

let TeacherListPage = document.getElementById("TeacherListPage");

Array.from(teacherdata).forEach((item) => {
  let NewItem = document.createElement("div");

  NewItem.innerHTML = `<div class="HomeTeacherItem TCLI">
      <div
        class="HomeTeacherImg"
        style='background-image: url("${item.img}");'
      >
        <div class="HomeTeacherMore">
          <div class="HTMore">${item.more[0]}</div>
          <div class="HTMore">${item.more[1]}</div>
          <div class="HTMore">${item.more[2]}</div>
        </div>
      </div>
      <div class="HomeTeacherName">${item.name}</div>
      <div class="HomeTeacherInfor">${item.infor}</div>
    </div>`;
  TeacherListPage.appendChild(NewItem);
});
