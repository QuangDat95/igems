// Thanh SBAr orange
window.addEventListener("scroll", () => {
    let x = window.scrollY;
    let y = document.body.offsetHeight - window.innerHeight + 120;
    SBar.style.width = (x / y) * 100 + "%";
});

// Khai bao
// let SBar = document.getElementById("SBar");

// let NavBarCross = document.getElementById("NavBarCross");
// let root = document.getElementById("root");
// let BodyPage = document.getElementById("BodyPage");
// let NavBar = document.getElementById("NavBar");
// let Footer = document.getElementById("Footer");

// NavBar
// NavBar.innerHTML = `
//     <a class="NBI Logo" href=""></a>
//     <i id="NavShort" class="NavShort fas fa-bars" aria-hidden="true"></i>
//     <div class="Menu">
//       <a class="NBI p-r" href="/">
//         <div class="NBB"></div>TRANG CHỦ
//       </a>
//       <div class="NBIOut">
//         <a class="NBI" href="/introduce/ve-igems">
//           <div class="NBB"></div>GIỚI THIỆU
//         </a>
//         <div class="NBS NGT">
//           <a class="NBST" href="/introduce/ve-igems">
//             Về IGEMS
//           </a>
//           <a class="NBST NBSTE" href="/introduce/giao-vien-igems">
//             Giáo viên IGEMS
//           </a>
//         </div>
//       </div>
//       <div class="NBIOut">
//         <a class="NBI" href="/pp/phuong-phap-hoc">
//           <div class="NBB"></div>CÁC PHƯƠNG PHÁP
//         </a>
//         <div class="NBS NPP">
//           <a class="NBST" href="/pp/phuong-phap-hoc">
//             Phương pháp học
//           </a>
//           <a class="NBST" href="/pp/phuong-phap-giang-day">
//             Phương pháp giảng dạy
//           </a>
//           <a class="NBST NBSTE" href="/pp/phan-mem-zoom">
//             Phần mềm ZOOM
//           </a>
//         </div>
//       </div>
//       <div class="NBIOut">
//         <a class="NBI" href="/news/tin-tuc-va-su-kien">
//           <div class="NBB"></div>TIN TỨC
//         </a>
//         <div class="NBS NTT">
//           <a class="NBST" href="/news/tin-tuc-va-su-kien">
//             Tin tức và Sự kiện
//           </a>
//           <a class="NBST NBSTE" href="/news/cam-nhan-ve-igems">
//             Cảm nhận về IGEMS
//           </a>
//         </div>
//       </div>
//       <a class="NBI p-r" href="/contact">
//         <div class="NBB"></div>LIÊN HỆ
//       </a>
//       <a href="http://gemsedu.vn" class="NBI p-r">
//         <div class="NBB"></div>GEMS EDU
//       </a>
//       <div class="NBI p-r" id="RegisterButton">
//         <div class="NBB"></div>ĐĂNG KÝ NGAY
//       </div>
//       <a href="https://igems.com.vn/thanhtoan" class="NBI p-r">
//         <div class="NBB"></div>THANH TOÁN
//       </a>
//     </div>`;

// NavBarCross.innerHTML = `<a class="NBCI" href="/"><i
//   class="fas fa-home NBCIcon" aria-hidden="true"></i>TRANG CHỦ</a>
// <div class="NBCI" id='NBCIGioithieu'><i class="fas fa-info-circle NBCIcon" aria-hidden="true"></i>GIỚI
// THIỆU<i class="fas fa-sort-down pos-r" aria-hidden="true"></i></div>
// <div class="NBCS" id='NBCSGioithieu' style="height: 0px;"><a class="NBCI NBCISub"
//   href="/introduce/ve-igems">Về IGEMS</a><a class="NBCI NBCISub"
//   href="/introduce/giao-vien-igems">Giáo viên IGEMS</a></div>
// <div class="NBCI" id='NBCICacpp'><i class="fas fa-map-signs NBCIcon" aria-hidden="true"></i>CÁC PHƯƠNG
// PHÁP<i class="fas fa-sort-down pos-r" aria-hidden="true"></i></div>
// <div class="NBCS" id='NBCSCacpp' style="height: 0px;"><a class="NBCI NBCISub"
//   href="/pp/phuong-phap-hoc">Phương pháp
//   học</a><a class="NBCI NBCISub" href="/pp/phuong-phap-giang-day">Phương pháp giảng dạy</a><a
//   class="NBCI NBCISub" href="/pp/phan-mem-zoom">Phần mềm ZOOM</a></div>
// <div class="NBCI" id='NBCITintuc'><i class="fas fa-newspaper NBCIcon" aria-hidden="true"></i>TIN TỨC<i
//   class="fas fa-sort-down pos-r" aria-hidden="true"></i></div>
// <div class="NBCS" id='NBCSTintuc' style="height: 0px;"><a class="NBCI NBCISub"
//   href="/news/tin-tuc-va-su-kien">Tin tức
//   và Sự kiện</a><a class="NBCI NBCISub" href="/news/cam-nhan-ve-igems">Cảm nhận về IGEMS</a></div>
// <a class="NBCI" href="/contact"><i class="fas fa-address-card NBCIcon" aria-hidden="true"></i>LIÊN
// HỆ</a><a href="http://gemsedu.vn" class="NBCI"><i class="fas fa-school NBCIcon"
//   aria-hidden="true"></i>GEMS EDU</a>
// <div class="NBCI" id="NBCRegister"><i class="fas fa-user-plus NBCIcon" aria-hidden="true"></i>ĐĂNG KÝ NGAY</div><a
// href="https://igems.com.vn/thanhtoan" class="NBCI"><i class="fas fa-school NBCIcon"
//   aria-hidden="true"></i>THANH TOÁN</a>`;

// Footer.innerHTML = `<div class="F0 F1"><a class="Logo2" href="/"></a>
//   <div class="RO"></div>
//   <div>
//       <div class="RO"><i class="fas fa-building ROI" aria-hidden="true"></i>
//           <div><b>Trụ sở chính: </b>Tầng 5, tòa nhà số 2, Vương Thừa Vũ, Khương Trung, Thanh Xuân,
//               Hà Nội</div>
//       </div>
//       <div class="RO"><i class="fas fa-phone-alt ROI"
//               aria-hidden="true"></i><b>Hotline:&nbsp;</b>032 513 5112</div>
//       <div class="RO"><i class="fas fa-envelope ROI"
//               aria-hidden="true"></i><b>Email:&nbsp;</b>contact.gemsedu@gmail.com</div>
//   </div>
//   <div class="FooterContact"></div>
//   <div class="Social"><a href="https://www.facebook.com/IgemsOnlineCoaching1on1"><i
//               class="fab fa-facebook c-w" aria-hidden="true"></i></a><a
//           href="https://www.youtube.com/channel/UCV1BTC9Vxe8KI-_5zE0dZ9Q"><i
//               class="fab fa-youtube c-w" aria-hidden="true"></i></a></div>
// </div>
// <div class="F0 F2">
//   <div class="F21">
//       <div class="F2.1">
//           <div><b>PHƯƠNG PHÁP</b></div><a href="/pp/phuong-phap-hoc">
//               <div class="c-w">Phương pháp học</div>
//           </a><a href="/pp/phuong-phap-giang-day">
//               <div class="c-w">Phương pháp giảng dạy</div>
//           </a><a href="/pp/phan-mem-zoom">
//               <div class="c-w">Phần mềm Zoom</div>
//           </a><a href="http://gemsedu.vn"><b class="c-w">GEMS EDU</b></a><a href="/contact">
//               <div class="c-w fw-bd">LIÊN HỆ</div>
//           </a>
//       </div>
//       <div class="F2.2">
//           <div><b>GIỚI THIỆU</b></div><a href="/introduce/ve-igems">
//               <div class="c-w">Về IGEMS</div>
//           </a><a href="/introduce/giao-vien-igems">
//               <div class="c-w">Giáo viên IGEMS</div>
//           </a>
//           <div><b>TIN TỨC</b></div><a href="/news/tin-tuc-va-su-kien">
//               <div class="c-w">Tin tức và sự kiện</div>
//           </a><a href="/introduce/giao-vien-igems">
//               <div class="c-w">Giáo viên IGEMS</div>
//           </a>
//       </div>
//   </div>
// </div>
// </div>`;

let NavShort = document.getElementById("NavShort");
let NBCIGioithieu = document.getElementById("NBCIGioithieu");
let NBCICacpp = document.getElementById("NBCICacpp");
let NBCIKhoaHoc = document.getElementById("NBCIKhoaHoc");
let NBCITintuc = document.getElementById("NBCITintuc");
let NBCSGioithieu = document.getElementById("NBCSGioithieu");
let NBCSCacpp = document.getElementById("NBCSCacpp");
let NBCSKhoaHoc = document.getElementById("NBCSKhoaHoc");
let NBCSTintuc = document.getElementById("NBCSTintuc");

// NBCross
NavShort.addEventListener("click", (e) => {
    e.stopPropagation();
    NavBarCross.classList = "d-flex fd-c NavBarCross";
});

BodyPage.addEventListener("click", () => {
    NavBarCross.classList = "d-flex fd-c NavBarCross r-300";
});

NavBar.addEventListener("click", () => {
    NavBarCross.classList = "d-flex fd-c NavBarCross r-300";
});

NBCIGioithieu.addEventListener("click", () => {
    if (NBCSGioithieu.style.height == "0px") {
        NBCSGioithieu.style.height = "90px";
    } else {
        NBCSGioithieu.style.height = "0px";
    }

    NBCSCacpp.style.height = "0px";
    NBCSTintuc.style.height = "0px";
	NBCSKhoaHoc.style.height = "0px";
});

NBCIKhoaHoc.addEventListener("click", () => {
    if (NBCSKhoaHoc.style.height == "0px") {
        NBCSKhoaHoc.style.height = "135px";
    } else {
        NBCSKhoaHoc.style.height = "0px";
    }

    NBCSGioithieu.style.height = "0px";
    NBCSTintuc.style.height = "0px";
	NBCSCacpp.style.height = "0px";
});

NBCICacpp.addEventListener("click", () => {
    if (NBCSCacpp.style.height == "0px") {
        NBCSCacpp.style.height = "135px";
    } else {
        NBCSCacpp.style.height = "0px";
    }

    NBCSGioithieu.style.height = "0px";
    NBCSTintuc.style.height = "0px";
	NBCSKhoaHoc.style.height = "0px";
});

NBCITintuc.addEventListener("click", () => {
    let sl= document.getElementById("sl").value;
    if (NBCSTintuc.style.height == "0px") {
        NBCSTintuc.style.height = (sl*45)+"px";
    } else {
        NBCSTintuc.style.height = "0px";
    }

    NBCSCacpp.style.height = "0px";
    NBCSGioithieu.style.height = "0px";
	NBCSKhoaHoc.style.height = "0px";
});

// Go top
let GoTop = document.getElementById("GoTop");
GoTop.addEventListener("click", () => {
    window.scrollTo({
        top: 0,
        behavior: "smooth",
    });
});

// RegisterButton
let RegisterButton = document.getElementById("RegisterButton");
let FullRegister = document.getElementById("FullRegister");
let NBCRegister = document.getElementById("NBCRegister");

FullRegister.innerHTML = `<form id="frmdk" method="post" action="dangky"><div class="FormRegister FFR" id='FullFormRegister'>
<div class="FFRT">ĐĂNG KÝ HỌC THỬ MIỄN PHÍ</div><input name="name" class="FRI" type="text"
    placeholder="Tên" required>
<div class="c-r"></div><input name="age" class="FRI" type="number" placeholder="Tuổi" required>
<div class="c-r"></div><input name="email" class="FRI" type="email" placeholder="Email" required>
<div class="c-r"></div><input name="phone" class="FRI" type="number"
    placeholder="Số điện thoại" required>
<div class="c-r"></div>
<button type="submit" name="btngui" class="FRB">ĐĂNG KÝ</button>
</div></form>`;

RegisterButton.addEventListener("click", () => {
    FullRegister.classList = "FullRegister";
});

NBCRegister.addEventListener("click", () => {
    FullRegister.classList = "FullRegister";
});

let FullFormRegister = document.getElementById("FullFormRegister");
FullRegister.addEventListener("click", () => {
    FullRegister.classList = "FullRegister d-none";
});

FullFormRegister.addEventListener("click", (e) => {
    e.stopPropagation();
});

console.log(location.pathname);
if (location.pathname !== "/igems/" && location.pathname !== "/igems") {
    Array.from(document.getElementsByClassName("NBI")).forEach((element) => {
        element.classList.add("c-w");
    });

    document.getElementById("NavBar").classList = "NavBar NavBar2";

    Array.from(document.getElementsByClassName("Logo")).forEach((element) => {
        element.classList.add("Logo3");
    });
}