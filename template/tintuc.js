let SearchNews = document.getElementById("SearchNews");
let SearchButton = document.getElementById("SearchButton");
let KeyList = document.getElementById("KeyList");
let PML = document.getElementById("PML");
let SearchNews2 = document.getElementById("SearchNews2");
let SearchButton2 = document.getElementById("SearchButton2");
let KeyList2 = document.getElementById("KeyList2");
let NewsList = document.getElementById("NewsList");

import { newsdata } from "./data/newsdata.js";
import { keyword } from "./data/keyword.js";
console.log(keyword);

keyword.map((item) => {
  let NewKey = `<div class="KWI pointer">${item}</div>`;

  KeyList.innerHTML = KeyList.innerHTML + NewKey;
  KeyList2.innerHTML = KeyList2.innerHTML + NewKey;
});

let Search = (input) => {
  let newlist = [];
  for (let item of newsdata) {
    for (let i = 0; i < item.title.length; i++) {
      if (
        input.toLowerCase() ===
        item.title.slice(i, i + input.length).toLowerCase()
      ) {
        newlist.push(item);
        break;
      }
    }
  }
  renderNews(newlist);
};

Array.from(document.getElementsByClassName("KWI")).forEach((item) => {
  item.addEventListener("click", () => {
    SearchNews.value = item.innerHTML;
    SearchNews2.value = item.innerHTML;
    Search(item.innerHTML);
  });
});

let renderNews = (list) => {
  NewsList.innerHTML = "";
  for (let x of list) {
    let News = `<a class="NLI d-flex pointer" href="/news/tin-tuc-va-su-kien/${x.id}">
        <div class="HaveImg NLIimg"
            style="background-image: url(${x.img});">
        </div>
        <div class="d-flex fd-c f1 jc-sb">
            <div class="c-sgray fw-bd fs-30 p-20 ta-l NLIT">${x.title}</div>
            <div class="c-gray p-20 ta-l mb-20">${x.time}</div>
        </div>
    </a>`;
    NewsList.innerHTML = NewsList.innerHTML + News;
  }
};

renderNews(newsdata);

SearchNews.addEventListener("keyup", () => {
  if (SearchNews.value !== "") {
    SearchButton.classList = "fw-bd c-w bg-o p-10 SIB pointer";
    SearchButton2.classList = "fw-bd c-w bg-o p-10 SIB pointer";
    SearchButton.innerText = "Clear";
    SearchButton2.innerText = "Clear";
  } else {
    SearchButton.classList = "fw-bd c-bo bg-w p-10 SIB pointer";
    SearchButton2.classList = "fw-bd c-bo bg-w p-10 SIB pointer";
    SearchButton.innerText = "Search";
    SearchButton2.innerText = "Search";
  }
  SearchNews2.value = SearchNews.value;
  Search(SearchNews.value);
});

SearchNews2.addEventListener("keyup", () => {
  if (SearchNews.value !== "") {
    SearchButton.classList = "fw-bd c-w bg-o p-10 SIB pointer";
    SearchButton2.classList = "fw-bd c-w bg-o p-10 SIB pointer";
    SearchButton.innerText = "Clear";
    SearchButton2.innerText = "Clear";
  } else {
    SearchButton.classList = "fw-bd c-bo bg-w p-10 SIB pointer";
    SearchButton2.classList = "fw-bd c-bo bg-w p-10 SIB pointer";
    SearchButton.innerText = "Search";
    SearchButton2.innerText = "Search";
  }
  SearchNews.value = SearchNews2.value;
  Search(SearchNews.value);
});

SearchButton.addEventListener("click", () => {
  SearchButton.classList = "fw-bd c-bo bg-w p-10 SIB pointer";
  SearchButton2.classList = "fw-bd c-bo bg-w p-10 SIB pointer";
  SearchNews.value = "";
  SearchNews2.value = "";
  Search("");
});

SearchButton2.addEventListener("click", () => {
  SearchButton.classList = "fw-bd c-bo bg-w p-10 SIB pointer";
  SearchButton2.classList = "fw-bd c-bo bg-w p-10 SIB pointer";
  SearchNews.value = "";
  SearchNews2.value = "";
  Search("");
});
