
let Tech1 = document.getElementById("Tech1");
let Tech2 = document.getElementById("Tech2");
let Tech3 = document.getElementById("Tech3");
let Tech4 = document.getElementById("Tech4");
let Tech5 = document.getElementById("Tech5");
let HTTBar = document.getElementById("HTTBar");
let HFT2C = document.getElementById("HFT2C");
let HFSB1 = document.getElementById("HFSB1");
let HFSB2 = document.getElementById("HFSB2");
let HFSB3 = document.getElementById("HFSB3");

Tech1.addEventListener("click", () => {
  HTTBar.classList = "HTTBar HTTBar1";
  Tech4.innerText = "TIÊN PHONG CÔNG NGHỆ GIÁO DỤC";
  Tech5.innerText =
    "Với phương pháp giáo dục 1 kèm 1 giúp tăng 5 lần thời lượng tương tác của học viên với thầy cô. Giúp học viên cải thiện rõ 4 kĩ năng đặc biệt là giao tiếp. Đặc biệt ở IGEMS là có app quản lý tiến độ học tập.";
});

Tech2.addEventListener("click", () => {
  HTTBar.classList = "HTTBar HTTBar2";
  Tech4.innerText = "LỘ TRÌNH RIÊNG BIỆT";
  Tech5.innerText =
    "Mỗi học viên sẽ được thiết kế lộ trình học tập riêng biệt bởi những chuyên gia hàng đầu, giúp học viên đạt hiệu quả cao nhất trong buổi học và đặc biệt là hứng thú với buổi học.";
});

Tech3.addEventListener("click", () => {
  HTTBar.classList = "HTTBar HTTBar3";
  Tech4.innerText = "PHƯƠNG PHÁP ĐÀO TẠO CHUẨN MỸ";
  Tech5.innerText =
    "IGEMS áp dụng phương pháp giảng dạy của đối tác độc quyền QUANTUM LEARNING – tổ chức giáo dục hàng đầu thế giới với hơn 40 năm phát triển.";
});

// Home Feel
HFSB1.addEventListener("click", () => {
  HFSB1.classList = "HFSB bg-w";
  HFSB2.classList = "HFSB bg-g";
  HFSB3.classList = "HFSB bg-g";
  HFT2C.classList = "HFT2C HF1";
});

HFSB2.addEventListener("click", () => {
  HFSB2.classList = "HFSB bg-w";
  HFSB1.classList = "HFSB bg-g";
  HFSB3.classList = "HFSB bg-g";
  HFT2C.classList = "HFT2C HF2";
});

HFSB3.addEventListener("click", () => {
  HFSB3.classList = "HFSB bg-w";
  HFSB2.classList = "HFSB bg-g";
  HFSB1.classList = "HFSB bg-g";
  HFT2C.classList = "HFT2C HF3";
});



let HTTB1 = document.getElementById("HTTB1");
HTTB1.addEventListener("click", () => {
  window.scrollTo({
    top: 0,
    behavior: "smooth",
  });
});
