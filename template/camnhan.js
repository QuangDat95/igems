let SlideImg = document.getElementById("SlideImg");
import { feeldata } from "./data/feeldata.js";

let FeelIgems = document.getElementById("FeelIgems");
let SIIC = document.getElementById("SIIC");
let BSlideList = document.getElementById("BSlideList");

feeldata.map((item) => {
  let NewImg = `<img src="${item}" alt="${item}"
    class="FeelIgemsImg" id="${feeldata.indexOf(item) + 1}">`;

  let New2 = `<div class="SII SIIL" id="F${
    feeldata.indexOf(item) + 1
  }"><img src="${item}" class="SIIimg"
    alt="${item}"></div>`;

  let New3 = `<div class="BSlide" id="B${feeldata.indexOf(item) + 1}"></div>`;

  FeelIgems.innerHTML = FeelIgems.innerHTML + NewImg;
  SIIC.innerHTML = SIIC.innerHTML + New2;
  BSlideList.innerHTML = BSlideList.innerHTML + New3;
});

let Now;
let BSP = document.getElementById("BSP");
let BSN = document.getElementById("BSN");

Array.from(document.getElementsByClassName("FeelIgemsImg")).forEach((item) => {
  item.addEventListener("click", () => {
    SlideImg.classList = "FullRegister";
    document.getElementById(`F${item.id}`).classList = "SII";
    document.getElementById(`B${item.id}`).classList = "BSlide bc-bb";
    Now = Number(item.id);
    Array.from(document.getElementsByClassName("SII")).forEach((item2) => {
      if (Number(item2.id.slice(1, item2.id.length)) < Now) {
        item2.classList = "SII SIIL";
      }
      if (Number(item2.id.slice(1, item2.id.length)) > Now) {
        item2.classList = "SII SIIR";
      }
    });
  });
});

Array.from(document.getElementsByClassName("BSlide")).forEach((item) => {
  item.addEventListener("click", () => {
    Now = Number(item.id.slice(1, item.id.length));
    item.classList = "BSlide bc-bb";
    Array.from(document.getElementsByClassName("BSlide")).forEach((item2) => {
      if (Number(item2.id.slice(1, item2.id.length)) !== Now) {
        item2.classList = "BSlide";
      }
    });

    document.getElementById(
      `F${Number(item.id.slice(1, item.id.length))}`
    ).classList = "SII";
    Array.from(document.getElementsByClassName("SII")).forEach((item2) => {
      if (Number(item2.id.slice(1, item2.id.length)) < Now) {
        item2.classList = "SII SIIL";
      }
      if (Number(item2.id.slice(1, item2.id.length)) > Now) {
        item2.classList = "SII SIIR";
      }
    });
  });
});

BSP.onclick = () => {
  if (Now > 1) {
    Now = Now - 1;
    document.getElementById(`F${Now}`).classList = "SII";
    document.getElementById(`B${Now}`).classList = "BSlide bc-bb";

    Array.from(document.getElementsByClassName("SII")).forEach((item2) => {
      if (Number(item2.id.slice(1, item2.id.length)) < Now) {
        item2.classList = "SII SIIL";
      }
      if (Number(item2.id.slice(1, item2.id.length)) > Now) {
        item2.classList = "SII SIIR";
      }
    });
    Array.from(document.getElementsByClassName("BSlide")).forEach((item3) => {
      if (Number(item3.id.slice(1, item3.id.length)) !== Now) {
        item3.classList = "BSlide";
      }
    });
  }
};

BSN.onclick = () => {
  if (Now < feeldata.length) {
    Now = Now + 1;
    document.getElementById(`F${Now}`).classList = "SII";
    document.getElementById(`B${Now}`).classList = "BSlide bc-bb";

    Array.from(document.getElementsByClassName("SII")).forEach((item2) => {
      if (Number(item2.id.slice(1, item2.id.length)) < Now) {
        item2.classList = "SII SIIL";
      }
      if (Number(item2.id.slice(1, item2.id.length)) > Now) {
        item2.classList = "SII SIIR";
      }
    });
    Array.from(document.getElementsByClassName("BSlide")).forEach((item3) => {
      if (Number(item3.id.slice(1, item3.id.length)) !== Now) {
        item3.classList = "BSlide";
      }
    });
  }
};

let SIC = document.getElementById("SIC");

SlideImg.onclick = () => {
  SlideImg.classList = "FullRegister d-none";
};

SIC.onclick = (e) => {
  e.stopPropagation();
};
