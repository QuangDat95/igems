let I2BI1 = document.getElementById("I2BI1");
let I2BI2 = document.getElementById("I2BI2");
let I2BI3 = document.getElementById("I2BI3");
let I2BI4 = document.getElementById("I2BI4");

let I2TextBar = document.getElementById("I2TextBar");
let I2Content = document.getElementById("I2Content");

I2BI1.addEventListener("click", () => {
  I2TextBar.classList = "I2TextBar I2TextBar1";
  I2Content.innerText =
    "Tham gia đào tạo liên tục và được giám sát chất lượng giảng dạy bới các chuyên gia sư phạm hàng đầu.";
});

I2BI2.addEventListener("click", () => {
  I2TextBar.classList = "I2TextBar I2TextBar2";
  I2Content.innerText =
    "Các thầy cô ứng tuyển được yêu cầu gửi hồ sơ ứng tuyển và video giới thiệu bản thân bằng tiếng anh gửi về cho trung tâm. Các ứng viên có trình độ chuyên môn tốt sẽ được lựa chọn.";
});

I2BI3.addEventListener("click", () => {
  I2TextBar.classList = "I2TextBar I2TextBar3";
  I2Content.innerText =
    "Sau khi đã qua vòng phỏng vấn nghiêm ngặt, thầy cô sẽ được chia sẻ về kinh nghiệm giảng dạy cũng như cách xử lý tình huống khi dạy online. Các giáo viên được dạy thử DEMO thường xuyên để phân loại trình độ, đánh giá khả năng với giảng dạy trực tuyến.";
});

I2BI4.addEventListener("click", () => {
  I2TextBar.classList = "I2TextBar I2TextBar4";
  I2Content.innerText =
    "Tham gia đào tạo liên tục và được giám sát chất lượng giảng dạy bới các chuyên gia sư phạm hàng đầu.";
});


