let PPSI1 = document.getElementById("PPSI1");
let PPSI11 = document.getElementById("PPSI11");
let PPSI2 = document.getElementById("PPSI2");
let PPSI3 = document.getElementById("PPSI3");
let PPSI4 = document.getElementById("PPSI4");
let PPSI5 = document.getElementById("PPSI5");
let PPSI22 = document.getElementById("PPSI22");
let PPSI33 = document.getElementById("PPSI33");
let PPSI44 = document.getElementById("PPSI44");
let PPSI55 = document.getElementById("PPSI55");

Array.from(PPSI1.children).forEach((item) => {
  item.addEventListener("mouseenter", () => {
    PPSI11.classList = "fas fa-lightbulb fs-60 tfs-130 bottom-103x c-o PPSIcon";
  });

  item.addEventListener("mouseleave", () => {
    PPSI11.classList = "far fa-lightbulb c-w fs-60 PPSIcon";
  });
});

Array.from(PPSI2.children).forEach((item) => {
  item.addEventListener("mouseenter", () => {
    PPSI22.classList =
      "fas fa-book-open fs-60 tfs-130 bottom-103x l-40x c-o PPSIcon";
  });

  item.addEventListener("mouseleave", () => {
    PPSI22.classList = "fas fa-book c-w fs-60 PPSIcon";
  });
});
Array.from(PPSI3.children).forEach((item) => {
  item.addEventListener("mouseenter", () => {
    PPSI33.classList =
      "fas fa-handshake fs-60 tfs-130 bottom-103x c-o l-38x PPSIcon";
  });

  item.addEventListener("mouseleave", () => {
    PPSI33.classList = "far fa-handshake c-w fs-60 PPSIcon l-38x";
  });
});
Array.from(PPSI4.children).forEach((item) => {
  item.addEventListener("mouseenter", () => {
    PPSI44.classList =
      "fas fa-book-reader fs-60 tfs-130 bottom-103x l-40x c-o PPSIcon";
  });

  item.addEventListener("mouseleave", () => {
    PPSI44.classList = "fas fa-user c-w fs-60 l-40x PPSIcon";
  });
});
Array.from(PPSI5.children).forEach((item) => {
  item.addEventListener("mouseenter", () => {
    PPSI55.classList = "fas fa-id-badge fs-60 tfs-130 bottom-103x c-o PPSIcon";
  });

  item.addEventListener("mouseleave", () => {
    PPSI55.classList = "far fa-id-badge c-w fs-60 PPSIcon";
  });
});
