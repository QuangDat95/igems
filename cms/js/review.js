$(function () {
    $("#example").DataTable({
        "fixedHeader": true,
        "ordering": false,
        "paging": true,
        "processing": true,
        "ajax": "review/datatable",
        columns: [
              { data: "id"},
              { data: "name" },
              { data: "hinhanh"},
              { data: "ordernum" },
              { data: "trangthai"},
              { data: "sua" },
              { data: "xoa" },
          ],
    });

    $('#form-client').on("submit", function(e) {
        var formData = new FormData(this);
        $.ajax({
            method: "POST",
            url:"review/save",
            data: formData,
            mimeType: "multipart/form-data",
            cache: false, // do not cache this request
            contentType: false, // prevent missing boundary string
            processData: false, // do not transform to query string
            dataType: "json",
        }).done(function(response) {
            if (response.success) {
                $("#example").DataTable().ajax.reload(null, false);
                $('#largeModal').modal('hide');
            }
            else
                alert(response.msg);
        });
        event.preventDefault(); // <- avoid reloading
    });
});

function add() {
    document.getElementById("form-client").reset();
    document.getElementById("id").value = 0;
    document.getElementById("hinhanh_preview").innerHTML = '';
}


function edit(index) {
    var table = $('#example').DataTable();
    document.getElementById("hinhanh_preview").innerHTML = table.cell(index, 2).data();
    // document.getElementById("image_edit").value = image;
    if (table.cell(index,4).data() == 'Hiển thị') {
        $("input[name=status][value='1']").prop("checked", true);
    } else {
        $("input[name=status][value='0']").prop("checked", true);
    }
    document.getElementById("id").value = table.cell(index, 0).data();
    document.getElementById("name").value = table.cell(index, 1).data();
    document.getElementById("ordernum").value = table.cell(index, 3).data();
}
