$(function () {
    $("#example").DataTable({
        "fixedHeader": true,
        "ordering": false,
        "paging": true,
        "processing": true,
        "ajax": "baochi/datatable",
        columns: [
              { data: "id"},
              { data: "name" },
              { data: "hinhanh"},
              { data: "hinh_anh", "visible": false},
              { data: "url" },
              { data: "sua" },
              { data: "xoa" },
          ],
    });

  $('#form-client').on("submit", function(e) {
      var formData = new FormData(this);
      $.ajax({
          method: "POST",
          url:"baochi/save",
          data: formData,
          mimeType: "multipart/form-data",
          cache: false, // do not cache this request
          contentType: false, // prevent missing boundary string
          processData: false, // do not transform to query string
          dataType: "json",
      }).done(function(response) {
          if (response.success) {
              $("#example").DataTable().ajax.reload(null, false);
              $('#largeModal').modal('hide');
          }
          else
              alert(response.msg);
      });
      event.preventDefault(); // <- avoid reloading
  });
});

function add() {
    document.getElementById("form-client").reset();
    document.getElementById("id").value=0;
}


function edit(index) {
     var table = $('#example').DataTable();
     document.getElementById("id").value=table.cell(index,0).data();
     document.getElementById("name").value=table.cell(index,1).data();
     $("#hinhanh_preview").attr('src',table.cell(index,3).data());
     document.getElementById("url").value=table.cell(index,4).data();
}

function del(id) {
    if (confirm("Bạn có chắc chắn muốn xóa?"))
        $.post("baochi/delrow", {id:id}, function(data){
            if (data.success) {
                $("#example").DataTable().ajax.reload(null, false);
            }
            else {
                document.getElementById('msg').innerHTML = data.msg;
                $('#thongbao').modal('show');
            }
        },'json');
}
