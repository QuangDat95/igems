$(function () {
    $("#example").DataTable({
        "fixedHeader": true,
        "ajax": "giaovien/datatable",
        "paging": true,
        "searching": true,
        "ordering": false,
        columns: [
              { data: "id"},
              { data: "name" },
              { data: "hinhanh" },
              { data: "country"},
              { data: "thu_tu" },
              { data: "nang_luc"},
              { data: "mo_ta" },
              { data: "kinh_nghiem" },
              { data: "sua" },
              { data: "xoa" },
          ],
    });

    $('#form-client').on("submit", function(e) {
        var formData = new FormData(this);
        // var nguoinhan = JSON.stringify($("#email-to").val())
        // formData.append('nguoinhan', nguoinhan);
        $.ajax({
            method: "POST",
            url:"giaovien/save",
            data: formData,
            mimeType: "multipart/form-data",
            cache: false, // do not cache this request
            contentType: false, // prevent missing boundary string
            processData: false, // do not transform to query string
            dataType: "json",
        }).done(function(response) {
            if (response.success) {
                $("#example").DataTable().ajax.reload(null, false);
                $('#largeModal').modal('hide');
            }
            else
                alert(response.msg);
        });
        event.preventDefault(); // <- avoid reloading
    });
});

function add() {
    document.getElementById("form-client").reset();
    document.getElementById("id").value = 0;
}

function edit(id) {
    $('#hinhanh').val('');
    $.post("giaovien/getrow", {id:id}, function(data){
        if (data.success) {
            $('#largeModal').modal('show');
            $("#id").val(data.row['id']);
            $("#name").val(data.row['name']);
            $("#mota").val(data.row['mo_ta']);
            $("#country").val(data.row['country']);
            $("#thu_tu").val(data.row['thu_tu']);
            $("#hinhanh_preview").attr('src',data.row['hinh_anh']);
            $("#nangluc").val(data.row['nang_luc']);
            $("#kinh_nghiem").val(data.row['kinh_nghiem']);
        }
        else {
            document.getElementById('msg').innerHTML = data.msg;
            $('#thongbao').modal('show');
        }
    },'json');
}


function del(id) {
    if (confirm("Bạn có chắc chắn muốn xóa?")) window.location.href = "giaovien/del?id=" + id;
}
