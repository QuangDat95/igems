$(function () {
    $("#example").DataTable({
        fixedHeader: true,
        ordering: false
    });
});
function add() {
    document.getElementById("form-client").reset();
    document.getElementById("id").value = 0;
}


function edit(index, status) {
    var table = $('#example').DataTable();
    document.getElementById("url").value = table.cell(index, 2).data();
    document.getElementById("ordernum").value = table.cell(index, 3).data();
    if (status == 1) {
        $("input[name=status][value='1']").prop("checked", true);
    } else {
        $("input[name=status][value='0']").prop("checked", true);
    }
    document.getElementById("id").value = table.cell(index, 0).data();
    document.getElementById("name").value = table.cell(index, 1).data();
}
