$(function () {
    $("#example").DataTable({
        "fixedHeader": true,
        "ordering": false,
        "paging": true,
        "processing": true,
        // "serverSide": true,
        "ajax": "cacconso/datatable",
        columns: [
              { data: "id"},
              { data: "name"},
              { data: "luot_xem" },
              { data: "sua" },
              { data: "xoa" },
          ],
    });

    $('#form-client').on("submit", function(e) {
        var formData = new FormData(this);
        $.ajax({
            method: "POST",
            url:"cacconso/save",
            data: formData,
            mimeType: "multipart/form-data",
            cache: false, // do not cache this request
            contentType: false, // prevent missing boundary string
            processData: false, // do not transform to query string
            dataType: "json",
        }).done(function(response) {
            if (response.success) {
                $("#example").DataTable().ajax.reload(null, false);
                $('#largeModal').modal('hide');
            }
            else
                alert(response.msg);
        });
        event.preventDefault(); // <- avoid reloading
    });
});

function add() {
    document.getElementById("form-client").reset();
    document.getElementById("id").value=0;
}

function del(id) {
    if (confirm("Bạn có chắc chắn muốn xóa?"))
        $.post("cacconso/delrow", {id:id}, function(data){
            if (data.success) {
                $("#example").DataTable().ajax.reload(null, false);
            }
            else {
                document.getElementById('msg').innerHTML = data.msg;
                $('#thongbao').modal('show');
            }
        },'json');
}

function edit(id) {
    $.post("cacconso/getrow", {id:id}, function(data){
        if (data.success) {
            $("#id").val(data.row['id']);
            $("#name").val(data.row['name']);
            $("#luot_xem").val(data.row['luot_xem']);
        }
        else {
            document.getElementById('msg').innerHTML = data.msg;
            $('#thongbao').modal('show');
        }
    },'json');
}

// function edit(index) {
//     var table = $('#example').DataTable();
//     var opt=table.cell(index,8).data();
//     document.getElementById("opt"+opt).selected = "true";
//     document.getElementById("url").value=table.cell(index,10).data();
//     document.getElementById("vitri").value=table.cell(index,9).data();
//     tinymce.get("noidung").setContent(table.cell(index,6).data());
//     tinymce.get("mota").setContent(table.cell(index,5).data());
//     document.getElementById("id").value=table.cell(index,0).data();
//     document.getElementById("name").value=table.cell(index,3).data();
//     document.getElementById("image_edit").value=table.cell(index,13).data();
//     document.getElementById("hinhanh_preview").innerHTML=table.cell(index,7).data();
// }
