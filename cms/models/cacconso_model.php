<?php
class cacconso_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata()
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 ";
        $query           = $this->db->query("SELECT *,
            CONCAT('<img src=\"',hinh_anh,'\" height=\"50\">') AS hinhanh,
            (SELECT name FROM danhmuc WHERE id=danh_muc) AS danhmuc
            FROM cacconso $dieukien ORDER BY id DESC ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function danhmuc()
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 ";
        $query           = $this->db->query("SELECT id,name FROM danhmuc $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getrow($id)
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 AND id=$id ";
        $query           = $this->db->query("SELECT * FROM cacconso $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function save($id, $data)
    {

        if($id>0)
            $query = $this->update("cacconso", $data, " id = $id ");
        else {
            $data['tinh_trang']=1;
            $query = $this->insert("cacconso", $data);
        }
        return $query;
    }

    function del($id)
    {
        $query = $this->db->query("UPDATE cacconso SET tinh_trang=0 WHERE id=$id ");
        return $query;
    }

}

?>
