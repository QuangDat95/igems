<?php
class khoahoc_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata()
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 ";
        $query           = $this->db->query("SELECT *,
            CONCAT('<img src=\"',hinh_anh,'\" height=\"50\">') AS hinhanh,
            (SELECT name FROM danhmuc WHERE id=danh_muc) AS danhmuc
            FROM khoahoc $dieukien ORDER BY id DESC ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function danhmuc()
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 ";
        $query           = $this->db->query("SELECT id,name FROM danhmuc $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getrow($id)
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 AND id=$id ";
        $query           = $this->db->query("SELECT * FROM khoahoc $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function save($id, $data)
    {
        $url = $data['url'];
        $dieukien = " WHERE tinh_trang=1 AND url LIKE '$url%' AND id!=$id ";
        $query  = $this->db->query("SELECT url FROM khoahoc $dieukien ORDER BY url DESC LIMIT 1 ");
        $temp  = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($temp AS $item)
            $data['url']=$temp[0]['url'].'.1';
        if($id>0)
            $query = $this->update("khoahoc", $data, " id = $id ");
        else {
            $data['tinh_trang']=1;
            $data['ngay_dang']=date("Y-m-d");
            $data['author']=$_SESSION['user']['id'];
            $query = $this->insert("khoahoc", $data);
        }
        return $query;
    }

    function del($id)
    {
        $query = $this->db->query("UPDATE khoahoc SET tinh_trang=0 WHERE id=$id ");
        return $query;
    }

}

?>
