<?php
class sanpham_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata()
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 ";
        $query           = $this->db->query("SELECT * FROM sanpham $dieukien ORDER BY id DESC ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getrow($id)
    {
        $result   = array();
        $dieukien = " WHERE id=$id ";
        $query           = $this->db->query("SELECT * FROM sanpham $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    // function danhmuc()
    // {
    //     $result   = array();
    //     $dieukien = " WHERE tinh_trang=1 ";
    //     $query           = $this->db->query("SELECT id,name FROM danhmucsp $dieukien ");
    //     if ($query)
    //         $result  = $query->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }



    function save($id, $data)
    {
        $query = $this->update("sanpham", $data, "id = $id");
        return $query;
    }

    function del($id)
    {
        $query = $this->db->query("UPDATE sanpham SET tinh_trang=0 WHERE id=$id ");
        return $query;
    }

}

?>
