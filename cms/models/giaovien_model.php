<?php
class giaovien_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata()
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 ";
        $query           = $this->db->query("SELECT *, CONCAT('<img src=\"',hinh_anh,'\" height=\"60\">') AS hinhanh
           FROM giaovien $dieukien ORDER BY id DESC ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getrow($id)
    {
        $result   = array();
        $dieukien = " WHERE id=$id ";
        $query           = $this->db->query("SELECT * FROM giaovien $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    function save($id, $data)
    {
        if($id>0)
            $query = $this->update("giaovien", $data, " id = $id ");
        else {
            $data['tinh_trang']=1;
            $query = $this->insert("giaovien", $data);
        }
        return $query;
    }

    function del($id)
    {
        $query = $this->db->query("UPDATE giaovien SET tinh_trang=0 WHERE id=$id ");
        return $query;
    }
}

?>
