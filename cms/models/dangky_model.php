<?php
class dangky_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata()
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 ";
        $query           = $this->db->query("SELECT *,
            DATE_FORMAT(ngay_gio,'%d/%m/%Y %H:%i:%s') AS thoigian
           FROM lienhe $dieukien ORDER BY id DESC ");
        if ($query) 
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function save($id, $data)
    {
        $query = $this->update("lienhe", $data, " id = $id ");
        return $query;
    }
}

?>
