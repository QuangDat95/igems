<?php
class review_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata()
    {
        $result   = array();
        $query           = $this->db->query("SELECT *
           FROM review ORDER BY id DESC ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function save($id, $data)
    {
        if($id>0)
            $query = $this->update("review", $data, " id = $id ");
        else {
            $query = $this->insert("review", $data);
        }
        return $query;
    }
    function del($id)
    {
        $query=$this->delete("review","id = $id");
        return $query;
    }
}
