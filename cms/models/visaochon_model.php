<?php

class visaochon_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata()
    {
        $result   = array();
        $query           = $this->db->query("SELECT *
           FROM visaochon ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function save($id, $data)
    {
        if($id>0)
            $query = $this->update("visaochon", $data, " id = $id ");
        else {
            $query = $this->insert("visaochon", $data);
        }
        return $query;
    }

    function delObj($id)
    {
        $query=$this->delete("visaochon","id = $id");
        return $query;
    }
}