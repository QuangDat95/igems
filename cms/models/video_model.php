<?php

class video_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata()
    {
        $result   = array();
        $query           = $this->db->query("SELECT *
           FROM video ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function save($id, $data)
    {
        if($id>0)
            $query = $this->update("video", $data, " id = $id ");
        else {
            $query = $this->insert("video", $data);
        }
        return $query;
    }

    function delObj($id)
    {
        $query=$this->delete("video","id = $id");
        return $query;
    }
}