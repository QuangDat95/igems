<script type="text/javascript" src="js/visaochon.js"></script>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Quản lý Video</strong>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#largeModal" onclick="add()"><i class="fa fa-plus"></i>&nbsp; Add</button>
                    </div>
                    <div class="card-body">
                        <table id="example" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên</th>
                                <th>Hình ảnh</th>
                                <th>Thứ tự</th>
                                <th>Trạng thái</th>
                                <th style="display: none">Image</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=0;
                            foreach($this->data as $row) {
                                ?>
                                <tr>
                                    <td><?php echo $row['id'];?></td>
                                    <td><?php echo $row['name'];?></td>
                                    <td><img src="../<?php echo $row['image'];?>" width="80"></td>
                                    <td><?php echo $row['ordernum'];?></td>
                                    <td>
                                        <?php
                                        if($row['status']==1){
                                            echo "Hiện thị";
                                        }else{
                                            echo 'Không hiện thị';
                                        }
                                        ?>
                                    </td>
                                    <td style="display: none"><?php echo $row['image'];?></td>
                                    <td><a href="javascript:void(0)" data-toggle="modal" data-target="#largeModal" onclick="edit(<?php echo $i;?>,<?php echo $row['status'] ?>)"><i class="fa fa-edit"></i></a></td>
                                    <td><a href="visaochon/del?id=<?php echo $row['id'];?>"><i class="fa fa-trash-o"></i></a>  </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Vì sao chọn Igems</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-client" method="post" enctype="multipart/form-data" action="visaochon/save">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row form-group">
                                <input type="hidden" id="id" name="id">
                                <div class="col col-md-3">
                                    <label for="thanhpho" class=" form-control-label">Tên Video</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="name" name="name" class="form-control" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="banner" class=" form-control-label">Hình ảnh</label></div>
                                <div class="col-12 col-md-9">
                                    <div id="hinhanh_preview"></div>
                                    <input type="hidden" value="" name="image_edit" id="image_edit">
                                    <input type="file" id="image" name="image">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="quanhuyen" class=" form-control-label">Thứ tự</label></div>
                                <div class="col-12 col-md-9">
                                    <input type="number" id="ordernum" name="ordernum" placeholder="Thứ tự" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="status" class=" form-control-label">Trạng thái</label></div>
                                <div class="col-12 col-md-9">
                                    <input type="radio" class="status" name="status" value="1" checked> Hiện thị
                                    <input type="radio" class="status" name="status" value="0"> Không hiện thị
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i>&nbsp;Cancel</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
