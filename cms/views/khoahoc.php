<script type="text/javascript" src="js/khoahoc.js"></script>
<script type="text/javascript" src="libs/tinymce/tinymce.min.js"></script>
        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Quản lý khóa học</strong>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#largeModal" onclick="add()"><i class="fa fa-plus"></i>&nbsp; Add</button>
                            </div>
                            <div class="card-body">
                              <table id="example" class="table table-striped table-bordered" style="width: 100%">
                                  <thead>
                                      <tr>
                                            <th>ID</th>
                                            <th>Ngày đăng</th>
                                            <th>Cập nhật cuối</th>
                                            <th>Tên khóa học</th>
                                            <th>Url</th>
                                            <th>Hình ảnh</th>
                                            <th></th>
                                            <th></th>
                                      </tr>
                                  </thead>

                              </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

        <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="largeModalLabel">Thông tin khóa học</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="form-client" method="post" enctype="multipart/form-data" action="khoahoc/save">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row form-group">
                                    <input type="hidden" id="id" name="id">
                                    <div class="col col-md-3"><label for="name" class=" form-control-label">Tên khóa học</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="name" name="name" placeholder="Tên khóa học" class="form-control" required>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="hinhanh" class=" form-control-label">Hình ảnh</label></div>
                                    <div class="col-12 col-md-9">
                                        <img id="hinhanh_preview" src="" height="60">
                                        <!-- <input type="hidden" value="" name="image_edit" id="image_edit"> -->
                                        <input type="file" id="hinhanh" name="hinhanh">
                                    </div>
                                </div>
                            </div><div class="col-sm-6">
                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="vitri" class=" form-control-label">Vị trí home</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="vitri" name="vitri" placeholder="Vị trí xuất hiện trên trang chủ" class="form-control">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="url" class=" form-control-label">URL</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="url" name="url" placeholder="Để trống sẽ cập nhật theo tên khóa học" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-12 col-md-12">
                            <h5>Mô tả</h5>
                            <textarea rows="3" class="form-control1 control2" name="mota" id="mota"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-12 col-md-12">
                            <h5>Nội dung</h5>
                            <textarea rows="3" class="form-control1 control2" name="noidung" id="noidung"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i>&nbsp;Cancel</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>


        <script>
        	tinymce.init({
                        mode: "textareas",
        								entity_encoding : "raw",
                        plugins: ["advlist autolink lists link image charmap print preview anchor",
                                    "searchreplace visualblocks code fullscreen textcolor", "media",
                                    "insertdatetime media table contextmenu paste jbimages","fullscreen","moxiemanager"],
                        image_advtab: true,
                        paste_data_images: true,
                        browser_spellcheck : true,
                        relative_urls:false,
                        remove_script_host : false,
                        //convert_urls : true,
                        image_dimensions: false,
                        forced_root_block : false,
                        force_br_newlines : true,
                        force_p_newlines : false,
                        toolbar: " undo redo | styleselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media insertfile |  fontsizeselect | forecolor backcolor | fullscreen"
        		});

          $(document).on('focusin', function(e) {
                if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
              }
            });

            function them() {
          		var id=document.getElementById("danhmuc").value;
          		var row=document.getElementById("danhmuc");
          		var text=row.options[row.selectedIndex].text;
          		var danhsach=document.getElementById('danhsach');
          		var inner=danhsach.innerHTML+'<input type="checkbox" name="danhsach[]" value="'+id+'" checked=""> '+text+' ';
          		danhsach.innerHTML=inner;
          	}

        </script>
