<?php
class khoahoc extends controller
{
   function __construct()
   {
       parent::__construct();
   }

   function index()
   {
       require('layouts/header.php');
       // $this->view->data = $this->model->getdata();
       $this->view->danhmuc = $this->model->danhmuc();
       $this->view->render('khoahoc');
       require('layouts/footer.php');
   }

   function datatable()
   {
       $datatable['draw']=1;
       $datatable['recordsTotal']=1;
       $datatable['recordsFiltered']=1;
       $ketqua = [];
       $datatable['data']=$this->model->getdata();
       foreach ($datatable['data'] AS $key=>$row) {
           $datatable['data'][$key]['hinhanh']='<img src="'.$row['hinh_anh'].'">';
           $datatable['data'][$key]['sua']='<a href="javascript:void(0)" data-toggle="modal" data-target="#largeModal" onclick="edit('.$row['id'].')"><i class="fa fa-edit"></i>';
           $datatable['data'][$key]['xoa']='<a href="javascript:void(0)" data-toggle="modal" data-target="#staticModal"  onclick="del('.$row['id'].')"><i class="fa fa-trash-o"></i>';
       }
       echo json_encode($datatable,true);
   }

   function getrow()
   {
       $id = $_REQUEST['id'];
       $data = $this->model->getrow($id);
       if (count($data)>0) {
           $jsonObj['row'] = $data[0];
           $jsonObj['success'] = true;
       } else {
           $jsonObj['err'] = 'Lỗi đọc dữ liệu từ máy chủ';
           $jsonObj['success'] = false;
       }
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('json');
   }

   function save()
   {
       $id = $_REQUEST['id'];
       $name = $_REQUEST['name'];
       $url = ($_REQUEST['url']!='')?$_REQUEST['url']:functions::convertname($name);
       $tenFile = functions::convertname($name);
       $mota = $_REQUEST['mota'];
       $noidung = $_REQUEST['noidung'];
       $vitri = $_REQUEST['vitri'];
       $data = ['name'=>$name, 'com'=>$vitri,'mo_ta'=>$mota,'noi_dung'=>$noidung,'url'=>$url, 'updated'=>date("Y-m-d")];
       if (isset($_FILES['hinhanh']['name']) && ($_FILES['hinhanh']['name'] != '')) {
           $dir = ROOT_DIR . '/uploads/khoahoc/';
           $file = functions::uploadfile('hinhanh', $dir, $tenFile);
           $hinhanh = HOME.'/uploads/khoahoc/' . $file;
           $data['hinh_anh'] = $hinhanh;
       }
       if ($this->model->save($id,$data)) {
           $response['msg'] = 'Cập nhật thành công';
           $response['success'] = true;
       } else {
           $response['msg'] = 'Lỗi khi cập nhật vào database';
           $response['success'] = false;
       }
       echo json_encode($response);
   }

   function delrow()
   {
       $id = $_REQUEST['id'];
       if ($this->model->del($id)) {
           $response['msg'] = 'Đã xóa bản ghi';
           $response['success'] = true;
       } else {
           $response['msg'] = 'Lỗi khi cập nhật database';
           $response['success'] = false;
       }
       echo json_encode($response);
   }
}
?>
