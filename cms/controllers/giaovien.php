<?php
class giaovien extends controller
{
   function __construct()
   {
       parent::__construct();
       if ($_SESSION['user']['id'] > 1)
            header('Location: ' . URL);
   }

   function index()
   {
       require('layouts/header.php');
       $this->view->render('giaovien');
       require('layouts/footer.php');
   }

   function datatable()
   {
       $datatable['draw']=1;
       $datatable['recordsTotal']=1;
       $datatable['recordsFiltered']=1;
       $ketqua = [];
       $datatable['data']=$this->model->getdata(0);
       foreach ($datatable['data'] AS $key=>$row) {
           $datatable['data'][$key]['sua']='<a href="javascript:void(0)" onclick="edit('.$row['id'].')"><i class="fa fa-edit"></i>';
           $datatable['data'][$key]['xoa']='<a href="javascript:void(0)" data-toggle="modal" data-target="#staticModal"  onclick="del('.$row['id'].')"><i class="fa fa-trash-o"></i>';
       }
       echo json_encode($datatable,true);
   }

   function getrow()
   {
       $id = $_REQUEST['id'];
       $data = $this->model->getrow($id);
       if (count($data)>0) {
           $jsonObj['row'] = $data[0];
           $jsonObj['success'] = true;
       } else {
           $jsonObj['msg'] = 'Lỗi đọc dữ liệu từ máy chủ'.$id;
           $jsonObj['success'] = false;
       }
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('json');
   }
   

   function save()
   {
       $id = $_REQUEST['id'];
       $name = $_REQUEST['name'];
       $mota = $_REQUEST['mota'];
       $nangluc = $_REQUEST['nangluc'];
       $kinhnghiem = $_REQUEST['kinh_nghiem'];
       $country = $_REQUEST['country'];
       $thutu = $_REQUEST['thu_tu'];
       $tenFile = functions::convertname($name);
       $data = [
           'name'=>$name,
           'nang_luc'=>$nangluc,
           'mo_ta'=>$mota,
           'kinh_nghiem' =>$kinhnghiem,
           'country' =>$country,
           'thu_tu' =>$thutu,
           'tinh_trang'=>1
       ];
       if (isset($_FILES['hinhanh']['name']) && ($_FILES['hinhanh']['name'] != '')) {
           $dir = ROOT_DIR . '/uploads/giaovien/';
           $file = functions::uploadfile('hinhanh', $dir, $tenFile);
           $hinhanh = HOME.'/uploads/giaovien/' . $file;
           $data['hinh_anh'] = $hinhanh;
       }
       //
       // require 'layouts/header.php';
       if ($this->model->save($id,$data)) {
           // $this->view->thongbao = 'Cập nhật thành công! <a href="giaovien">Nhấn vào đây để quay lại</a>';
           // $this->view->render('thongbao');
           $response['msg'] = 'Cập nhật thành công';
           $response['success'] = true;
       } else {
           // $this->view->thongbao = 'Cập nhật không thành công! <a href="giaovien">Nhấn vào đây để quay lại</a>';
           // $this->view->render('canhbao');
           $response['msg'] = 'Lỗi khi cập nhật vào database';
           $response['success'] = false;
       }
       // require 'layouts/footer.php';

       echo json_encode($response);
   }

   function del()
   {
       $id = $_REQUEST['id'];
       require 'layouts/header.php';
       if ($this->model->del($id)) {
          $this->view->thongbao = 'Cập nhật thành công! <a href="giaovien">Nhấn vào đây để quay lại</a>';
           $this->view->render('thongbao');
       } else {
           $this->view->thongbao = 'Có lỗi khi xóa bản ghi này! <a href="giaovien">Nhấn vào đây để quay lại</a>';
           $this->view->render('canhbao');
       }
       require 'layouts/footer.php';
   }

}
?>
