<?php
class review extends controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        require('layouts/header.php');
        $this->view->render('review');
        require('layouts/footer.php');
    }

    function datatable()
    {
        $datatable['draw']=1;
        $datatable['recordsTotal']=1;
        $datatable['recordsFiltered']=1;
        $ketqua = [];
        $datatable['data']=$this->model->getdata();
        foreach ($datatable['data'] AS $key=>$row) {
            $datatable['data'][$key]['hinhanh']='<img src="../'.$row['image'].'">';
            $datatable['data'][$key]['trangthai']=($row['status']==1)?'Hiển thị':'';
            $datatable['data'][$key]['sua']='<a href="javascript:void(0)" data-toggle="modal" data-target="#largeModal" onclick="edit('.$key.')"><i class="fa fa-edit"></i>';
            $datatable['data'][$key]['xoa']='<a href="javascript:void(0)" data-toggle="modal" data-target="#staticModal"  onclick="del('.$row['id'].')"><i class="fa fa-trash-o"></i>';
        }
        echo json_encode($datatable,true);
    }

    function save()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $ordernum = $_REQUEST['ordernum'];
        $status = $_REQUEST['status'];
        $data = ['name' => $name,'ordernum' => $ordernum,'status' => $status];
        if (isset($_FILES['image']['name']) && ($_FILES['image']['name'] != '')) {
            $dir = ROOT_DIR . '/uploads/review/';
            $fname = functions::convertname($name);
            $file = functions::uploadfile('image', $dir, $fname);
            $hinhanh = 'uploads/review/' . $file;
            $data['image'] = $hinhanh;
        }
        if ($id > 0)
            $data['updated_at'] = date("Y-m-d H:i:s");
        else
            $data['created_at'] = date("Y-m-d H:i:s");
        if ($this->model->save($id,$data)) {
            $response['msg'] = 'Cập nhật thành công';
            $response['success'] = true;
        } else {
            $response['msg'] = 'Lỗi khi cập nhật vào database';
            $response['success'] = false;
        }
        echo json_encode($response);
        // require 'layouts/header.php';
        // if ($this->model->save($id, $data)) {
        //     $this->view->thongbao = 'Cập nhật thành công! <a href="banner">Nhấn vào đây để quay lại</a>';
        //     $this->view->render('thongbao');
        // } else {
        //     $this->view->thongbao = 'Cập nhật không thành công! <a href="banner">Nhấn vào đây để quay lại</a>';
        //     $this->view->render('canhbao');
        // }
        // require 'layouts/footer.php';
    }

    function del()
    {
        $id = $_REQUEST['id'];
        require 'layouts/header.php';
        if ($this->model->del($id)) {
            $this->view->thongbao = 'Cập nhật thành công! <a href="giaovien">Nhấn vào đây để quay lại</a>';
            $this->view->render('thongbao');
        } else {
            $this->view->thongbao = 'Có lỗi khi xóa bản ghi này! <a href="giaovien">Nhấn vào đây để quay lại</a>';
            $this->view->render('canhbao');
        }
        require 'layouts/footer.php';
    }
}
