<?php
class sanpham extends controller
{
   function __construct()
   {
       parent::__construct();
   }

   function index()
   {
       require('layouts/header.php');
       // $this->view->danhmuc = $this->model->danhmuc();
       $this->view->render('sanpham');
       require('layouts/footer.php');
   }

   function datatable()
   {
       $datatable['draw']=1;
       $datatable['recordsTotal']=1;
       $datatable['recordsFiltered']=1;
       $ketqua = [];
       $datatable['data']=$this->model->getdata(0);
       foreach ($datatable['data'] AS $key=>$row) {
           $datatable['data'][$key]['sua']='<a href="javascript:void(0)" onclick="edit('.$row['id'].')"><i class="fa fa-edit"></i>';
       }
       echo json_encode($datatable,true);
   }

   function getrow()
   {
       $id = $_REQUEST['id'];
       $data = $this->model->getrow($id);
       if (count($data)>0) {
           $jsonObj['row'] = $data[0];
           $jsonObj['success'] = true;
       } else {
           $jsonObj['err'] = 'Lỗi đọc dữ liệu từ máy chủ';
           $jsonObj['success'] = false;
       }
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('json');
   }

   function save()
   {
       $id = $_REQUEST['id'];
       $name = $_REQUEST['name'];
       // $name_en = $_REQUEST['name_en'];
       // $url = ($_REQUEST['url']!='')?$_REQUEST['url']:functions::convertname($name);
       // $masp = $_REQUEST['masp'];
       // $hinhanh = $_REQUEST['hinhanh'];
       // $slide1 = $_REQUEST['linkanh1'];
       // $slide2 = $_REQUEST['linkanh2'];
       // $slide3 = $_REQUEST['linkanh3'];
       // $gianiemyet = str_replace(",","",$_REQUEST['gianiemyet']);
       // $giaban = str_replace(",","",$_REQUEST['giaban']);
       // $danhmuc = $_REQUEST['danhmuc'];
       $mota = $_REQUEST['mota'];
       $mota_en = $_REQUEST['mota_en'];
       $thanhphan = $_REQUEST['thanhphan'];
       $thanhphan_en = $_REQUEST['thanhphan_en'];
       $tinh_nang = $_REQUEST['tinh_nang'];
       $data = ['name'=>$name, 'mo_ta'=>$mota,'mo_ta_en'=>$mota_en,'noi_dung'=>$thanhphan,'noi_dung_en'=>$thanhphan_en,'tinh_nang' => $tinh_nang];
       if ($this->model->save($id,$data)) {
           $response['msg'] = 'Cập nhật thành công';
           $response['success'] = true;
       } else {
           $response['msg'] = 'Lỗi khi cập nhật vào database';
           $response['success'] = false;
       }
       echo json_encode($response);
   }

   function del()
   {
       $id = $_REQUEST['id'];
       require 'layouts/header.php';
       if ($this->model->del($id)) {
           $this->view->thongbao = 'Đã xóa bản ghi! <a href="sanpham">Nhấn vào đây để quay lại</a>';
           $this->view->render('thongbao');
       } else {
           $this->view->thongbao = 'Có lỗi khi xóa bản ghi này! <a href="sanpham">Nhấn vào đây để quay lại</a>';
           $this->view->render('canhbao');
       }
       require 'layouts/footer.php';
   }
}
?>
