<?php
class baochi extends controller
{
   function __construct()
   {
       parent::__construct();
   }

   function index()
   {
       require('layouts/header.php');
       $this->view->render('baochi');
       require('layouts/footer.php');
   }

   function datatable()
   {
       $datatable['draw']=1;
       $datatable['recordsTotal']=1;
       $datatable['recordsFiltered']=1;
       $ketqua = [];
       $datatable['data']=$this->model->getdata();
       foreach ($datatable['data'] AS $key=>$row) {
           $datatable['data'][$key]['hinhanh']='<img src="'.$row['hinh_anh'].'">';
           $datatable['data'][$key]['sua']='<a href="javascript:void(0)" data-toggle="modal" data-target="#largeModal" onclick="edit('.$key.')"><i class="fa fa-edit"></i>';
           $datatable['data'][$key]['xoa']='<a href="javascript:void(0)" data-toggle="modal" data-target="#staticModal"  onclick="del('.$row['id'].')"><i class="fa fa-trash-o"></i>';
       }
       echo json_encode($datatable,true);
   }

   function save()
   {
       $id = $_REQUEST['id'];
       $name = $_REQUEST['name'];
       $tenFile = functions::convertname($name);
       $url = $_REQUEST['url'];
       $data = ['name'=>$name,'url'=>$url];
       if (isset($_FILES['hinhanh']['name']) && ($_FILES['hinhanh']['name'] != '')) {
           $dir = ROOT_DIR . '/uploads/home/';
           $file = functions::uploadfile('hinhanh', $dir, $tenFile);
           $hinhanh = HOME.'/uploads/home/' . $file;
           $data['hinh_anh'] = $hinhanh;
       }
       if ($this->model->save($id,$data)) {
           $response['msg'] = 'Cập nhật thành công';
           $response['success'] = true;
       } else {
           $response['msg'] = 'Lỗi khi cập nhật vào database';
           $response['success'] = false;
       }
       echo json_encode($response);
   }

   function delrow()
   {
       $id = $_REQUEST['id'];
       if ($this->model->del($id)) {
           $response['msg'] = 'Đã xóa bản ghi';
           $response['success'] = true;
       } else {
           $response['msg'] = 'Lỗi khi cập nhật database';
           $response['success'] = false;
       }
       echo json_encode($response);
   }

}
?>
