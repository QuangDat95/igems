<?php
class baiviet extends controller
{
   function __construct()
   {
       parent::__construct();
   }

   function index()
   {
       require('layouts/header.php');
       // $this->view->data = $this->model->getdata();
       $this->view->danhmuc = $this->model->danhmuc();
       $this->view->render('baiviet');
       require('layouts/footer.php');
   }

   function datatable()
   {
       $datatable['draw']=1;
       $datatable['recordsTotal']=1;
       $datatable['recordsFiltered']=1;
       $ketqua = [];
       $datatable['data']=$this->model->getdata();
       foreach ($datatable['data'] AS $key=>$row) {
           $datatable['data'][$key]['hinhanh']='<img src="'.$row['hinh_anh'].'">';
           $datatable['data'][$key]['sua']='<a href="javascript:void(0)" data-toggle="modal" data-target="#largeModal" onclick="edit('.$row['id'].')"><i class="fa fa-edit"></i>';
           $datatable['data'][$key]['xoa']='<a href="javascript:void(0)" data-toggle="modal" data-target="#staticModal"  onclick="del('.$row['id'].')"><i class="fa fa-trash-o"></i>';
       }
       echo json_encode($datatable,true);
   }



   // function getdata()
   //  {
   //      // Search
   //      $keyword = isset($_REQUEST['search']['value']) ? $_REQUEST['search']['value'] : '';
   //      // Order
   //      $offset = $_REQUEST['start'];
   //      $rows = $_REQUEST['length'];
   //      $query = $this->model->getdata2($keyword, $offset, $rows);
   //      $totalData = $query['total'];
   //      $totalFilter = $totalData;
   //      $data = array();
   //     foreach ($query['rows'] as $key => $value) {
   //         $subdata = array();
   //         $subdata[] = $value['id'];
   //         $subdata[] = $value['ngay_dang'];
   //         $subdata[] = $value['updated'];
   //         $subdata[] = $value['name'];
   //         $subdata[] = $value['url'];
   //         $subdata[] = $value['mo_ta'];
   //         $subdata[] = $value['noi_dung'];
   //         $subdata[] = '<img src="'.$value['hinh_anh'].'">';
   //         $subdata[] = $value['danh_muc'];
   //         $subdata[] = $value['com'];
   //         $subdata[] = $value['url'];
   //         $subdata[] = $value['danhmuc'];
   //         $subdata[] = $value['nguoinhap'];
   //         $subdata[] = $value['hinh_anh'];
   //         $subdata[] = '<a href="javascript:void(0)" data-toggle="modal" data-target="#largeModal" onclick="edit('.$key.')"><i class="fa fa-edit"></i></a>';
   //         $subdata[] = '<a href="javascript:void(0)" onclick="del('.$value['id'].')"><i class="fa fa-trash-o"></i></a>  ';
   //         $data[] = $subdata;
   //     }
   //      $json_data = array(
   //          "draw" => intval($_REQUEST['draw']),
   //          "recordsTotal" => intval($totalData),
   //          "recordsFiltered" => intval($totalFilter),
   //          "data" => $data
   //      );
   //
   //      echo json_encode($json_data);
   //  }

   function getrow()
   {
       $id = $_REQUEST['id'];
       $data = $this->model->getrow($id);
       if (count($data)>0) {
           $jsonObj['row'] = $data[0];
           $jsonObj['success'] = true;
       } else {
           $jsonObj['err'] = 'Lỗi đọc dữ liệu từ máy chủ';
           $jsonObj['success'] = false;
       }
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('json');
   }

   function save()
   {
       $id = $_REQUEST['id'];
       $name = $_REQUEST['name'];
       $url = ($_REQUEST['url']!='')?$_REQUEST['url']:functions::convertname($name);
       $tenFile = functions::convertname($name);
       $danhmuc = $_REQUEST['danhmuc'];
       $mota = $_REQUEST['mota'];
       $noidung = $_REQUEST['noidung'];
       $vitri = $_REQUEST['vitri'];
       $data = ['name'=>$name, 'com'=>$vitri,'mo_ta'=>$mota,'noi_dung'=>$noidung,'url'=>$url,'danh_muc'=>$danhmuc, 'updated'=>date("Y-m-d")];
       if (isset($_FILES['hinhanh']['name']) && ($_FILES['hinhanh']['name'] != '')) {
           $dir = ROOT_DIR . '/uploads/baiviet/';
           $file = functions::uploadfile('hinhanh', $dir, $tenFile);
           $hinhanh = HOME.'/uploads/baiviet/' . $file;
           $data['hinh_anh'] = $hinhanh;
       }
       if ($this->model->save($id,$data)) {
           $response['msg'] = 'Cập nhật thành công';
           $response['success'] = true;
       } else {
           $response['msg'] = 'Lỗi khi cập nhật vào database';
           $response['success'] = false;
       }
       echo json_encode($response);
   }

   function del()
   {
       $id = $_REQUEST['id'];
       require 'layouts/header.php';
       if ($this->model->del($id)) {
           $this->view->thongbao = "<script>window.location.href = 'baiviet';</script>";
           $this->view->render('thongbao');
       } else {
           $this->view->thongbao = 'Có lỗi khi xóa bản ghi này! <a href="data">Nhấn vào đây để quay lại</a>';
           $this->view->render('canhbao');
       }
       require 'layouts/footer.php';
   }
}
?>
