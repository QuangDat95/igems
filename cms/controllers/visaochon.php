<?php

class visaochon extends controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        require('layouts/header.php');
        $this->view->data = $this->model->getdata();
        $this->view->render('visaochon');
        require('layouts/footer.php');
    }

    function save()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $url = $_REQUEST['url'];
        if ($_REQUEST['ordernum'] < 0) {
            $ordernum = 0;
        } else {
            $ordernum = $_REQUEST['ordernum'];
        }
        $status = $_REQUEST['status'];
        if (isset($_FILES['image']['name']) && ($_FILES['image']['name'] != '')) {
            $dir = ROOT_DIR . '/uploads/visaochon/';
            $fname = functions::convertname($name);
            $file = functions::uploadfile('image', $dir, $fname);
            $hinhanh = 'uploads/visaochon/' . $file;
        } else {
            if (!empty($_REQUEST['image_edit'])) {
                $hinhanh = $_REQUEST['image_edit'];
            } else {
                $hinhanh = '';
            }
        }
        if ($id > 0) {
            $data = [
                'name' => $name,
                'image' => $hinhanh,
                'ordernum' => $ordernum,
                'status' => $status,
                'updated_at' => date("Y-m-d H:i:s")
            ];
        } else {
            $data = [
                'name' => $name,
                'image' => $hinhanh,
                'ordernum' => $ordernum,
                'status' => $status,
                'created_at' => date("Y-m-d H:i:s")
            ];
        }
        require 'layouts/header.php';
        if ($this->model->save($id, $data)) {
            $this->view->thongbao = 'Cập nhật thành công! <a href="banner">Nhấn vào đây để quay lại</a>';
            $this->view->render('thongbao');
        } else {
            $this->view->thongbao = 'Cập nhật không thành công! <a href="banner">Nhấn vào đây để quay lại</a>';
            $this->view->render('canhbao');
        }
        require 'layouts/footer.php';
    }

    function del()
    {
        $id = $_REQUEST['id'];
        require 'layouts/header.php';
        if ($this->model->delObj($id)) {
            $this->view->thongbao = 'Cập nhật thành công! <a href="giaovien">Nhấn vào đây để quay lại</a>';
            $this->view->render('thongbao');
        } else {
            $this->view->thongbao = 'Có lỗi khi xóa bản ghi này! <a href="giaovien">Nhấn vào đây để quay lại</a>';
            $this->view->render('canhbao');
        }
        require 'layouts/footer.php';
    }
}