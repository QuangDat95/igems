<?php
class banner extends controller
{
   function __construct()
   {
       parent::__construct();
   }

   function index()
   {
       require('layouts/header.php');
       $this->view->data = $this->model->getdata();
       $this->view->render('banner');
       require('layouts/footer.php');
   }

   function save()
   {
     $id = $_REQUEST['id'];
       $name = $_REQUEST['name'];
       $url = $_REQUEST['url'];
       $hinhanh = $_REQUEST['hinh_anh'];
       if ($id > 0) {
           if (isset($_FILES['hinh_anh']['name']) && ($_FILES['hinh_anh']['name'] != '')) {
               $dir   = ROOT_DIR . '/uploads/banner/';
               $fname = $_FILES['hinh_anh']['name'];
               $file  = functions::uploadfile('hinh_anh', $dir, $fname);
               $hinh_anh  = 'uploads/banner/' . $file;
               $data = [
                   'url' => $url,
                   'hinh_anh' => $hinhanh,
                   'name' => $name
               ];
           } else
               $data = [
                   'url' => $url
               ];
       } else {
           if (isset($_FILES['hinh_anh']['name']) && ($_FILES['hinh_anh']['name'] != '')) {
               $dir   = ROOT_DIR . '/uploads/banner/';
               $fname = $_FILES['hinh_anh']['name'];
               $file  = functions::uploadfile('hinh_anh', $dir, $fname);
               $hinh_anh  = 'uploads/banner/' . $file;
           } else
               $hinh_anh  = '';
           $data = [

               'url' => $url,
               'name' => $name,
               'hinh_anh' => $hinh_anh
           ];
       }

       require 'layouts/header.php';
       if ($this->model->save($id, $data)) {
           $this->view->thongbao = 'Cập nhật thành công! <a href="banner">Nhấn vào đây để quay lại</a>';
           $this->view->render('thongbao');
       } else {
           $this->view->thongbao = 'Cập nhật không thành công! <a href="banner">Nhấn vào đây để quay lại</a>';
           $this->view->render('canhbao');
       }
       require 'layouts/footer.php';
   }

}
?>
