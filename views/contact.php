<!-- Body -->
<div class="BG"></div>
<div class="BodyPage" id='BodyPage'>
    <div class="Contact bc-s">
        <div class="ContactForm d-flex fd-c ta-l">
            <h1 class="d-none"><?=$page['title'] ?></h1>
            <h2 class="m-0 fsz-u fw-n CTT"><b><?= $thongtin[0]['value'] ?></b></h2>
            <h2 class="fsz-u fw-n fs-20"><b>Gems Edu Hà Nội</b></h2>
            <div>
                <div class="RO"><i class="fas fa-building ROI" aria-hidden="true"></i>
                    <h3 class="m-0 fsz-u fw-n"><b>Trụ sở chính: </b><?= $thongtin[1]['value'] ?></h3>
                </div>
            </div>
            <div class="FooterContact">
                <div class="RO"><i class="fas fa-phone-alt ROI" aria-hidden="true"></i>
                    <h3 class="m-0 fsz-u fw-n">
                        <b>Hotline:&nbsp;</b><span class="c-b"><?= $thongtin[2]['value'] ?></span>
                    </h3>
                </div>
                <div class="RO"><i class="fas fa-envelope ROI" aria-hidden="true"></i>
                    <h3 class="m-0 fsz-u fw-n"><b>Email:&nbsp;</b><span class="c-b"><?= $thongtin[3]['value'] ?></span></h3>
                </div>
            </div>
            <div class="MapGG">
                <?= $thongtin[14]['value'] ?>
            </div>
        </div>
        <img class="d-none" src="template/img/Background.jpg" alt="Tiếng Anh trực tuyến số 1 Việt Nam 1">
        <img class="d-none" src="template/img/Background.jpg" alt="Tiếng Anh trực tuyến số 1 Việt Nam 2">
        <img class="d-none" src="template/img/Background.jpg" alt="Tiếng Anh trực tuyến số 1 Việt Nam 3">
        <form method="post" action="dangky">
            <div class="FormRegister ContactForm CTF2">
                <div class="FRT CFT">
                    <div class="FRT1 CRT1">LIÊN HỆ</div>
                    <div class="FRT2 CRT2">Hãy nhập đầy đủ thông tin để chúng tôi phản hồi sớm nhất cho quý
                        khách!</div>
                </div><input name="name" class="FRI CFI" type="text" placeholder="Tên" required>
                <div class="c-r"></div><input name="phone" class="FRI CFI" type="number" placeholder="Số điện thoại" required>
                <div class="c-r"></div><input name="email" class="FRI CFI" type="email" placeholder="Email" required>
                <div class="c-r"></div><textarea name="data" class="FRI CFTA" type="text" placeholder="Nội dung"></textarea>
                <div class="c-r"></div>
                <button type="submit" name="btngui" class="FRB CRB" style="border: none;outline: none;">
                    <i class="fas fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Gửi thông tin
                </button>
            </div>
        </form>
    </div>