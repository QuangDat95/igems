
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="format-detection" content="telephone=no,email=no,address=no" />
    <title><?=$thongtin[0]['value']?></title>
    <link rel="shortcut icon" type="image/x-icon" href="template/bin.bnbstatic.com/static/images/common/favicon.ico">
    <style>
      html,
      body {
        font-family: BinancePlex, Arial, PingFangSC-Regular, "Microsoft YaHei",
          sans-serif;
      }
      body {
        margin: 0;
      }
      .not-fount-container {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        flex-direction: column;
        background: #fafafa;
      }
      .not-fount-image {
        width: 340px;
      }
      .not-fount-tips {
        margin-top: 65px;
        display: flex;
        align-items: center;
      }
      .not-fount-tips-bold {
        font-size: 24px;
        line-height: 28px;
        color: #1e2026;
        font-weight: 500;
      }
      .link {
        color: #d0980b;
        font-size: 14px;
        line-height: 20px;
      }
      img {
        width: 100%;
        display: block;
      }
      .not-found-board-logo {
        width: 56px;
      }
      .not-found-board-left {
        display: flex;
      }
      .not-found-board-text {
        margin-left: 16px;
        color: #1e2026;
        font-weight: bold;
        margin-top: 6px;
        font-size: 16px;
      }
      .not-found-board-text-inner {
        display: flex;
      }
      .text-inner-icons {
        margin-left: 16px;
        display: flex;
        align-items: center;
      }
      .text-inner-icons span {
        display: inline-block;
        width: 16px;
        height: 16px;
        margin-right: 10px;
        background-position: center;
        background-repeat: no-repeat;
      }
      .icon-mac {
        background-image: url("template/bin.bnbstatic.com/static/images/electron/macicon.svg");
      }
      .icon-linux {
        background-image: url("template/bin.bnbstatic.com/static/images/electron/linux.svg");
      }
      .icon-windows {
        background-image: url("template/bin.bnbstatic.com/static/images/electron/windows.svg");
      }
      .not-found-board-right {
        margin-top: 6px;
      }
      button {
        padding: 10px 16px;
        border: none;
        outline: none;
        background: linear-gradient(180deg, #f8d12f 0%, #f0b90b 100%);
        border-radius: 4px;
        cursor: pointer;
        font-size: 14px;
        color: #1e2026;
        font-weight: 500;
      }
      @media only screen and (max-device-width: 1024px) {
        .not-found-board {
          display: none;
        }
      }
      @media only screen and (min-device-width: 1025px) {
        .not-found-board {
          background: #ffffff;
          padding: 0 16px;
          height: 80px;
          box-shadow: 0px 0px 1px rgba(20, 21, 26, 0.1),
            0px 3px 6px rgba(71, 77, 87, 0.04),
            0px 1px 2px rgba(20, 21, 26, 0.04);
          border-radius: 4px;
          width: 496px;
          margin-top: 56px;
          display: flex;
          justify-content: space-between;
          align-items: center;
        }
      }
    </style>
  </head>
  <body>
    <div class="not-fount-container">
      <div class="not-fount-image">
        <img
          width="100%"
          src="<?=HOME?>/template/404-error.png"
          alt="Igems"
        />
      </div>
      <div class="not-fount-tips">
        <span class="not-fount-tips-bold"
          >Xin lỗi! Không thể tìm thấy trang bạn đang tìm kiếm.</span
        >
        <span style="margin-left: 16px"
          ><a href="<?=HOME?>" class="link"
            >Về trang chủ</a
          ></span
        >
      </div>
      
    </div>

  </body>
</html>
