<?php
	$menu= $data->getdanhmucsp();
?>
<!--================ Start banner Area =================-->
	<section class="banner-area relative">
		<div class="container">
			<div class="row height align-items-center justify-content-center">
				<div class="banner-content col-lg-5">
					<h1>food menu</h1>
					<hr>
					<div class="breadcrmb">
						<p>
							<a href="index.html">home</a>
							<span class="lnr lnr-arrow-right"></span>
							<a href="menu.html">menu</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================ End banner Area =================-->

	

	<!--================ Menu Area =================-->
	<section class="menu-area" id="menu_area">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-5 col-md-5">
					<?php if (isset($_SESSION['en']) && $_SESSION['en']>0) { ?>
					<div class="section-title relative">
						<h1>
							Menu
						</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
							magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat.</p>
						<!-- <a href="#" class="primary-btn text-uppercase">See Full Menu</a> -->
					</div>
					<?php }else{ ?>
						<div class="section-title relative">
						<h1>
							Thực đơn
						</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
							magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat.</p>
						<!-- <a href="#" class="primary-btn text-uppercase">See Full Menu</a> -->
					</div>
					<?php } ?>
				</div>
				<div class="col-lg-7 col-md-7">
					<div class="menu-list">
						<?php if (isset($_SESSION['en']) && $_SESSION['en']>0) { ?>
						<div class="single-menu">
							<h3>Menu</h3>
							<ul class="list">
								<?php foreach ($menu as  $value) { ?>
								<li>
									<a href="product/1/<?=$value['url']?>"><p class="menu-item"><?=$value['name_en']?> ........... <span>$25.00</span></p></a>
									<p>(<?=$value['mo_ta_en']?>)</p>
								</li>
								<?php } ?>
							</ul>
							<h3 style="margin-top: 30px;">THE END</h3>
						</div>
						<?php }else{ ?>
							<div class="single-menu">
							<h3>Thực đơn</h3>
							<ul class="list">
								<?php foreach ($menu as  $value) { ?>
								<li>
									<a href="product/1/<?=$value['url']?>"><p class="menu-item"><?=$value['name']?> ........... <span>$25.00</span></p></a>
									<p>(<?=$value['mo_ta']?>)</p>
								</li>
								<?php } ?>
							</ul>
							<h3 style="margin-top: 30px;">THE END</h3>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Menu Area =================-->

	<!--================ Start Call To Action Area =================-->
	<section class="container section-gap">
		<div class="callto-action-area relative">
			<div class="row d-flex justify-content-center">
				<div class="col-lg-12 p-0">
					<div class="cta-owl owl-carousel">
						<?php foreach ($menu as  $value) { ?>
						<a href="product/1/<?=$value['url']?>">
						<div class="item">
							<div class="cta-img">
								<img src="<?=$value['hinh_anh']?>" class="img-fluid" alt="<?=$value['name']?>" style="height: 403px;object-fit: cover;">
							</div>
							<?php if (isset($_SESSION['en']) && $_SESSION['en']>0) { ?>
							<div class="text-box text-center">
								<h3 class="mb-10"><?=$value['name_en']?></h3>
								<p>
									<?=$value['mo_ta_en']?>
								</p>
							</div>
							<?php }else{ ?>
								<div class="text-box text-center">
								<h3 class="mb-10"><?=$value['name']?></h3>
								<p>
									<?=$value['mo_ta']?>
								</p>
							</div>
							<?php } ?>
						</div>
						</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================ End Call To Action Area =================-->