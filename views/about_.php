<?php
   $giaovien= $data->giaovien();
?>
   <!-- Body -->
            <div class="BG"></div>
            <div class="BodyPage" id='BodyPage'>
                <div class="BodySub bc-s Intro">
                    <h1 class="d-none"><?=$page['title'] ?></h1>
                    <h2 class="HCT c-bo">GIỚI THIỆU VỀ IGEMS</h2>
                    <img class="d-none" src="template/img/Intro1.jpg" alt="<?=$danhmuc['mo_ta']?>">
                    <img class="d-none" src="template/img/Intro1.jpg" alt="<?=$danhmuc['mo_ta']?>">
                    <img class="d-none" src="template/img/Intro1.jpg" alt="<?=$danhmuc['mo_ta']?>">
                    <div class="HFC">
                        <div class="I1">
                            <h2 class="c-sgray fw-bd fs-30">IGEMS là trung tâm Coaching tiếng Anh online hàng đầu Việt
                                Nam thuộc hệ thống anh ngữ quốc tế GEMS EDU</h2>
                            <h2 class="c-gray fs-20 mt-20 fw-0">Chương trình học tiếng Anh trực tuyến 1 kèm 1 hàng đầu Việt
                                Nam tới từ hệ thống Anh ngữ quốc tế GEMS EDU đối tác độc quyền tại Việt Nam của tổ chứ
                                giáo dục QUANTUM LEARNING ở Mỹ (QUANTUM LEARNING là tổ chức giáo dục hàng đầu thế giới
                                với hơn 40 năm phát triển tại 85 quốc gia).<br>Mang lại hiệu quả học tập gấp 5 lần khi
                                học tại trung tâm và giá chỉ bằng 1/2.</h2>
                        </div>
                        <div class="I2"></div>
                    </div>
                    <h3 class="HCT c-bo">ĐỘI NGŨ GIÁO VIÊN</h3>
                    <div class="HFC">
                        <div class="I2 I3"></div>
                        <div class="I1">
                            <div class="c-sgray fw-bd fs-30">Thầy cô truyền cảm hứng cho học viên.</div>
                            <div class="c-gray fs-20 mt-20">Đội ngũ huấn luyện viên đều là những thầy cô có kinh nghiệm
                                giảng dạy. Luôn sáng tạo những bà học thú vị, đặc biệt trong việc giảng dạy trực tuyến
                                cho các học viên.</div>
                        </div>
                    </div>
                    <div class="HomeTeacher">
                        <h3 class="HCT">ĐỘI NGŨ GIÁO VIÊN TUYỆT VỜI</h3>
                        <div class="HomeTeacherList">
                            <?php foreach ($giaovien as $value) { ?>
                            <div class="HomeTeacherItem">
                                <div class="HomeTeacherImg" style="background-image: url(<?=$value['hinh_anh']?>);">
                                    <div class="HomeTeacherMore">
                                        <div class="HTMore"><?=$value['mo_ta']?></div>
                                    </div>
                                </div>
                                <div class="HomeTeacherName"><?=$value['name']?></div>
                                <div class="HomeTeacherInfor"><?=$value['nang_luc']?></div>
                            </div>
                            <?php } ?>
                            <!-- <div class="HomeTeacherItem">
                                <div class="HomeTeacherImg" style="background-image: url(template/data/img/T1.jpg);">
                                    <div class="HomeTeacherMore"></div>
                                </div>
                                <div class="HomeTeacherName">Megan Krause</div>
                                <div class="HomeTeacherInfor">Cử nhân trường Design School of Southern Africa</div>
                            </div>
                            <div class="HomeTeacherItem">
                                <div class="HomeTeacherImg" style="background-image: url(template/data/img/T3.jpg);">
                                    <div class="HomeTeacherMore"></div>
                                </div>
                                <div class="HomeTeacherName">Trần Ngọc Sơn</div>
                                <div class="HomeTeacherInfor">Cử nhân ngôn ngữ Anh - Đại học ngoại ngữ Hà Nội</div>
                            </div>
                            <div class="HomeTeacherItem">
                                <div class="HomeTeacherImg" style="background-image: url(template/data/img/T2.jpg);">
                                    <div class="HomeTeacherMore">
                                        <div class="HTMore">Ngôn ngữ Anh – Trường Đại học Nha Trang</div>
                                        <div class="HTMore">1 năm kinh nghiệm làm việc với các bạn nhỏ</div>
                                        <div class="HTMore">Sở thích của mình là đọc sách, nghe nhạc và đặc biệt mình
                                            yêu thích công việc giảng dạy cho các bạn nhỏ, mình thích làm quen và tìm
                                            hiểu xem các bạn ấy đang quan tâm điều gì về tiếng Anh để từ đó bằng khả
                                            năng của mình sẽ giúp các bạn ấy phát triển và phát huy tối đa thế mạnh, sự
                                            tự tin và năng lực của các bạn ấy. Cảm giác yêu thích là khi đứa trẻ mà mình
                                            nhiệt tình giảng dạy đạt được thành công trong việc học.</div>
                                    </div>
                                </div>
                                <div class="HomeTeacherName">Lê Nguyễn Anh Thư</div>
                                <div class="HomeTeacherInfor">Cử nhân khoa ngôn ngữ Anh - Trường Đại học Nha Trang</div>
                            </div>
                            <div class="HomeTeacherItem">
                                <div class="HomeTeacherImg" style="background-image: url(template/data/img/T4.jpg);">
                                    <div class="HomeTeacherMore"></div>
                                </div>
                                <div class="HomeTeacherName">Võ Trần Thanh Phương</div>
                                <div class="HomeTeacherInfor"></div>
                            </div> -->
                        </div>
                        <a class="HTButton" href="giaovien">Xem thêm</a>
                    </div>
                    <div class="IG">
                        <div class="IG2">
                            <div class="HTT Intro2">
                                <div class="HTT2 fs-40 c-w">Quá trình để trở thành giáo viên tại IGEMS</div>
                                <div class="HTTitle I2Border c-w">
                                    <div class="I2BI" id='I2BI1'>Yêu cầu</div>
                                    <div class="I2BI" id='I2BI2'>Hồ sơ</div>
                                    <div class="I2BI" id='I2BI3'>Vòng dạy thử</div>
                                    <div class="I2BI" id='I2BI4'>Vòng test</div>
                                    <div class="I2TextBar I2TextBar1" id='I2TextBar'></div>
                                </div>
                                <div class="ta-l c-w mt-20" id='I2Content'>Tham gia đào tạo liên tục và được giám sát
                                    chất lượng giảng dạy bởi các chuyên gia sư phạm hàng đầu.</div>
                            </div>
                            <div class="I1 TempDiv"></div>
                        </div>
                    </div>
                    <div class="HCT c-bo">App IGEMS</div>
                    <div class="HFC">
                        <div class="I1 c-sgray fw-bd fs-40">Dễ dàng quản lý lịch học của học viên bằng app IGEMS</div>
                        <div class="I1 I4">
                            <div class="I4Item">
                                <div class="d-flex"><i class="fas fa-road IIcon" aria-hidden="true"></i><span
                                        class="c-sgray fw-bd fs-20">Quản lý lộ trình học của học viên</span></div>
                                <div class="c-gray">Lộ trình học của học viên sẽ được cập nhật liên tục trên app của
                                    IGEMS.</div>
                            </div>
                            <div class="I4Item">
                                <div class="d-flex"><i class="fab fa-leanpub IIcon" aria-hidden="true"></i><span
                                        class="c-sgray fw-bd fs-20">Kết quả học tập</span></div>
                                <div class="c-gray">Kết quả học tập của học viên sẽ được cập nhật trên app.</div>
                            </div>
                            <div class="I4Item">
                                <div class="d-flex"><i class="far fa-calendar-alt IIcon" aria-hidden="true"></i><span
                                        class="c-sgray fw-bd fs-20">Lịch học cụ thể</span></div>
                                <div class="c-gray">Học viên có thể theo dõi lịch học cũng như có thể đăng kí lịch hoặc
                                    hủy buổi học.</div>
                            </div>
                            <div class="I4Item">
                                <div class="d-flex"><i class="fas fa-gift IIcon" aria-hidden="true"></i><span
                                        class="c-sgray fw-bd fs-20">Thông tin ưu đãi</span></div>
                                <div class="c-gray">Thông báo các chương trình ưu đãi cũng như thông báo gia hạn khóa
                                    học.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script src="template/about.js"></script>
