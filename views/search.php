<?php
$key = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
$key = str_replace("%20", " ", $key);
if ($key != '') {
  $search = $data->search_post1($key);
  $tong = count($search);
  $trang = isset($_GET['p']) ? $_GET['p'] : 1;
  $sotrang = ceil($tong / 5);
  $search1 = $data->search_post($key, $trang);
  $khuyenmai = $data->khuyenmai(12);
?>
  <style type="text/css">
    .pagination {
      width: 100%;
      margin-left: 50px;
    }

    .pagination a {
      display: block;
      color: black;
      float: left;
      padding: 8px 16px;
      text-decoration: none;
      transition: background-color .3s;
    }

    .pagination a.active {
      background-color: #0ab1e7;
      color: white;
    }

    .pagination a:hover:not(.active) {
      background-color: #ddd;
    }
  </style>
  <!-- Body -->
  <div class="BG"></div>
  <div class="BodyPage" id='BodyPage'>
    <div class="BodySub bc-s News">
      <div class="HCT c-bo">Tìm kiếm: <?= $key ?></div>
      <div class="d-flex NewsContainer">
        <div class="DifNews d-flex fd-c">
          <div class="DifNews1 d-flex fd-c">
            <form method="post" action="search" id="frmsearch">
              <div class="SearchContainer w-100 d-flex"><input class="SearchInput p-20" placeholder="Nhập từ khóa tìm kiếm" name="keyword" value="" required id='SearchNews'><button type="submit" class="fw-bd c-bo bg-w p-10 SIB pointer" style="height: -webkit-fill-available;border: none;outline: none;">Search</button></div>
              <label class="fw-bd c-w w-100 ta-l">Từ khóa</label>
              <div class="f-w d-flex" id='KeyList'>
              </div>
            </form>
          </div>
          <!--  <script type="text/javascript">
                              function submitsearch(){
                                document.getElementById("frmsearch").submit();
                              }
                            </script> -->
          <div class="DifNews2 d-flex fd-c">
            <div class="w-100 d-flex fd-c"><label class="fw-bd c-w w-100 ta-l">Tin khuyến
                mãi</label>
              <div class="d-flex fd-c PML" id='PML'>
                <?php foreach ($khuyenmai as $value) { ?>
                  <a class="PMI d-flex fd-c m-10 pointer" href="blog/<?= $value['url'] ?>">
                    <div class="HaveImg PMIimg" style="background-image: url(<?= $value['hinh_anh'] ?>);">
                    </div>
                    <div class="f1">
                      <div class="ta-l fw-bd c-w p-10"><?= $value['name'] ?></div>
                      <div class="ta-l c-w fs-14 p-10"><?= $value['ngaydang'] ?></div>
                    </div>
                  </a>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
        <div class="NewsList d-flex fd-c" id='NewsList'>
          <?php foreach ($search1 as $value) { ?>
            <a class="NLI d-flex pointer" href="blog/<?= $value['url'] ?>">
              <div class="HaveImg NLIimg" style="background-image: url(<?= $value['hinh_anh'] ?>);">
              </div>
              <div class="d-flex fd-c f1 jc-sb">
                <div class="c-sgray fw-bd fs-30 p-20 ta-l NLIT"><?= $value['name'] ?></div>
                <div class="c-gray p-20 ta-l mb-20"><?= $value['ngaydang'] ?></div>
              </div>
            </a>
          <?php } ?>
          <div class="pagination">
            <?php if ($trang > 1 && $sotrang > 1) {
              echo '<a href="search?keyword=' . $key . '&p=' . ($trang - 1) . '">&laquo;</a>';
            } else {
              echo '<a>&laquo;</a>';
            } ?>

            <?php for ($i = 1; $i <= $sotrang; $i++) {
              if ($i == $trang) {
                echo '<a class="active" href="search?keyword=' . $key . '&p=' . $i . '">' . $i . '</a>';
              } else {
                echo '<a href="search?keyword=' . $key . '&p=' . $i . '">' . $i . '</a>';
              }
            } ?>
            <?php if ($trang < $sotrang) {
              echo '<a href="search?keyword=' . $key . '&p=' . ($trang + 1) . '">&raquo;</a>';
            } else {
              echo '<a>&raquo;</a>';
            } ?>

          </div>

        </div>

        <div class="DifNews1 d-flex fd-c DifNews3">
          <form method="post" action="search">
            <div class="SearchContainer w-100 d-flex">
              <input class="SearchInput p-20" placeholder="Nhập từ khóa tìm kiếm" name="keyword" required value="" id='SearchNews2'>
              <button class="fw-bd c-bo bg-w p-10 SIB pointer" style="height: -webkit-fill-available;border: none;outline: none;">Search</button>
            </div>
            <label class="fw-bd c-w w-100 ta-l">Từ khóa</label>
            <div class="f-w d-flex" id='KeyList2'>
            </div>
          </form>
        </div>
      </div>
    </div>

  <?php } else {
  echo '<h1>Không tìm thấy trang bạn yêu cầu!</h1>';
} ?>