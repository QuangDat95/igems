<?php
	$danhmuc= $page['data'];
	if (sizeof($danhmuc)>0) {
		$spmoi= $data->spmoi();
    $sanpham=$data->getsanphamdanhmuc($page['id']);
    $tongbv= count($sanpham);
    $sotrang= ceil($tongbv/12);
    $trang= $url[1];
    $danhmuc1= $data->danhmucsp();
?>
<link rel='stylesheet' id='elementor-post-355-css'  href='template/wp-content/uploads/elementor/css/post-355258c.css?ver=1609825262' media='all' />
<div id="content" class="site-content">
            <div class="ast-container">
               <div data-elementor-type="archive" data-elementor-id="355" class="elementor elementor-355 elementor-location-archive" data-elementor-settings="[]">
                  <div class="elementor-section-wrap">
                     <section class="elementor-section elementor-top-section elementor-element elementor-element-b1a3ba9 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="b1a3ba9" data-element_type="section">
                        <div class="elementor-container elementor-column-gap-no">
                           <div class="elementor-row">
                              <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-e20d169" data-id="e20d169" data-element_type="column">
                                 <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                       
                                       <div class="elementor-element elementor-element-1e8f9c3 elementor-widget elementor-widget-theme-archive-title elementor-page-title elementor-widget-heading" data-id="1e8f9c3" data-element_type="widget" data-widget_type="theme-archive-title.default">
                                          <div class="elementor-widget-container">
                                             <h1 class="elementor-heading-title elementor-size-default"><?=$page['title']?></h1><br>
                                             <p><?=$page['description']?></p>
                                          </div>
                                       </div>
                                       <section class="elementor-section elementor-inner-section elementor-element elementor-element-1852bfd elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1852bfd" data-element_type="section">
                                          <div class="elementor-container elementor-column-gap-default">
                                             <div class="elementor-row">
                                                <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-954d644" data-id="954d644" data-element_type="column">
                                                   <div class="elementor-column-wrap elementor-element-populated">
                                                      <div class="elementor-widget-wrap">
                                                         <div class="elementor-element elementor-element-062738d elementor-grid-tablet-3 elementor-grid-3 elementor-grid-mobile-1 elementor-posts--thumbnail-top elementor-widget elementor-widget-archive-posts" data-id="062738d" data-element_type="widget" data-settings="{&quot;archive_classic_columns_tablet&quot;:&quot;3&quot;,&quot;archive_classic_columns&quot;:&quot;3&quot;,&quot;archive_classic_columns_mobile&quot;:&quot;1&quot;,&quot;archive_classic_row_gap&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:35,&quot;sizes&quot;:[]}}" data-widget_type="archive-posts.archive_classic">
                                                            <div class="elementor-widget-container">
                                                               <div class="elementor-posts-container elementor-posts elementor-posts--skin-classic elementor-grid">
                                                                <?php foreach ($danhmuc as $key => $value) { ?>
                                                                  <article class="elementor-post elementor-grid-item post-925 post type-post status-publish format-standard has-post-thumbnail hentry category-tin-tuc tag-tui-vai-bo tag-tui-vai-canvas">
                                                                     <a class="elementor-post__thumbnail__link" href="product/<?=$value['url']?>" >
                                                                        <div class="elementor-post__thumbnail">
                                                                           <img width="586" height="586"   alt="<?=$value['name']?>" loading="lazy" data-srcset="<?=$value['hinh_anh']?>" data-sizes="(max-width: 586px) 100vw, 586px" class="attachment-large size-large lazyload" src="<?=$value['hinh_anh']?>" />
                                                                           <noscript><img width="586" height="586" src="<?=$value['hinh_anh']?>" class="attachment-large size-large" alt="<?=$value['name']?>" loading="lazy" srcset="<?=$value['hinh_anh']?>" sizes="(max-width: 586px) 100vw, 586px" /></noscript>
                                                                        </div>
                                                                     </a>
                                                                     <div class="elementor-post__text">
                                                                        <h3 class="elementor-post__title">
                                                                           <a href="product/<?=$value['url']?>" >
                                                                           <?=$value['name']?>      </a>
                                                                        </h3>
                                                                        <div class="elementor-post__excerpt">
                                                                           <p><?=$value['mo_ta']?></p>
                                                                        </div>
                                                                        <a class="elementor-post__read-more" href="product/<?=$value['url']?>" >
                                                                        Chi thiết »      </a>
                                                                     </div>
                                                                  </article>
                                                                <?php } ?>
                                                               </div>
                                                               <nav class="elementor-pagination" role="navigation" aria-label="Pagination">
                                                                <?php for ($i=1; $i <=$sotrang ; $i++) { 
                                                                   if ($trang==$i) {
                                                                     echo '<span aria-current="page" class="page-numbers current"><span class="elementor-screen-only">Page</span>'.$i.'</span>';
                                                                   }else{
                                                                    echo '<a class="page-numbers" href="product/'.$i.'/'.$url[2].'"><span class="elementor-screen-only">Page</span>'.$i.'</a>';
                                                                   }
                                                                } ?>
                                                                    
                                                               </nav>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-ba05110" data-id="ba05110" data-element_type="column">
                                                   <div class="elementor-column-wrap elementor-element-populated">
                                                      <div class="elementor-widget-wrap">
                                                         <div class="elementor-element elementor-element-f2e78dc elementor-search-form--skin-classic elementor-search-form--button-type-icon elementor-search-form--icon-search elementor-widget elementor-widget-search-form">
                                                            <div class="elementor-widget-container">
                                                               <form class="elementor-search-form" role="search" action="search" method="post">
                                                                  <div class="elementor-search-form__container">
                                                                     <input placeholder="Nhập từ khóa ...." class="elementor-search-form__input" type="search" name="keyword" title="Search" value="" required>
                                                                     <button class="elementor-search-form__submit" type="submit" title="Search" name="btnsearch" aria-label="Search">
                                                                     <i class="fa fa-search" aria-hidden="true"></i>
                                                                     <span class="elementor-screen-only">Search</span>
                                                                     </button>
                                                                  </div>
                                                               </form>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-3d8de04 elementor-widget elementor-widget-heading" data-id="3d8de04" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                               <h4 class="elementor-heading-title elementor-size-default">Sản phẩm mới</h4>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-5b59c61 elementor-widget-divider--view-line elementor-widget elementor-widget-divider" data-id="5b59c61" data-element_type="widget" data-widget_type="divider.default">
                                                            <div class="elementor-widget-container">
                                                               <div class="elementor-divider">
                                                                  <span class="elementor-divider-separator">
                                                                  </span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-ac53f60 elementor-grid-1 elementor-grid-tablet-1 elementor-posts--thumbnail-none elementor-grid-mobile-1 elementor-widget elementor-widget-posts" data-id="ac53f60" data-element_type="widget" data-settings="{&quot;classic_columns&quot;:&quot;1&quot;,&quot;classic_columns_tablet&quot;:&quot;1&quot;,&quot;classic_row_gap&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:20,&quot;sizes&quot;:[]},&quot;classic_columns_mobile&quot;:&quot;1&quot;}" data-widget_type="posts.classic">
                                                            <div class="elementor-widget-container">
                                                               <div class="elementor-posts-container elementor-posts elementor-posts--skin-classic elementor-grid">
                                                                <?php foreach ($spmoi as $key => $value) { ?>
                                                                  <article class="elementor-post elementor-grid-item post-925 post type-post status-publish format-standard has-post-thumbnail hentry category-tin-tuc tag-tui-vai-bo tag-tui-vai-canvas">
                                                                     <div class="elementor-post__text">
                                                                        <h6 class="elementor-post__title">
                                                                           <a href="product/<?=$value['url']?>" >
                                                                           <?=$value['name']?>     </a>
                                                                        </h6>
                                                                     </div>
                                                                  </article>
                                                                <?php } ?>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-da67c17 elementor-widget elementor-widget-heading" data-id="da67c17" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                               <h4 class="elementor-heading-title elementor-size-default">Danh mục</h4>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-ceddac1 elementor-widget-divider--view-line elementor-widget elementor-widget-divider" data-id="ceddac1" data-element_type="widget" data-widget_type="divider.default">
                                                            <div class="elementor-widget-container">
                                                               <div class="elementor-divider">
                                                                  <span class="elementor-divider-separator">
                                                                  </span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-db49a3a elementor-widget elementor-widget-text-editor" data-id="db49a3a" data-element_type="widget" data-widget_type="text-editor.default">
                                                            <div class="elementor-widget-container">
                                                               <div class="elementor-text-editor elementor-clearfix">
                                                                  <ul class="chuyen-muc">
                                                                    <?php foreach ($danhmuc1 as $key => $value) { ?>
                                                                     <li><i class="fa fa-folder-open"></i> <a href="product/1/<?=$value['url']?>"><?=$value['name']?></a></li>
                                                                    <?php } ?>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </section>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
               </div>
            </div>
            <!-- ast-container -->
         </div>
<?php }else{
  echo '<h1>Nội dung đang được cập nhật!</h1>';
} ?>