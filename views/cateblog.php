<?php
  $danhmuc=$page['data'];
  if (sizeof($danhmuc)>0) {
  $baiviet= $data->getallbaiviet();
  $tong= count($baiviet);
  $sotrang= ceil($tong/12); 
  $trang= isset($_GET['p']) ? $_GET['p']:1;
  $baiviet1=$data->getbaivietblog($trang);
  $new= $data->newpost();  
?>
<link rel='stylesheet' id='elementor-post-355-css'  href='template/wp-content/uploads/elementor/css/post-355258c.css?ver=1609825262' media='all' />
<div id="content" class="site-content">
            <div class="ast-container">
               <div data-elementor-type="archive" data-elementor-id="355" class="elementor elementor-355 elementor-location-archive" data-elementor-settings="[]">
                  <div class="elementor-section-wrap">
                     <section class="elementor-section elementor-top-section elementor-element elementor-element-b1a3ba9 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="b1a3ba9" data-element_type="section">
                        <div class="elementor-container elementor-column-gap-no">
                           <div class="elementor-row">
                              <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-e20d169" data-id="e20d169" data-element_type="column">
                                 <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                       <!-- <div class="elementor-element elementor-element-546f932 elementor--h-position-center elementor--v-position-middle elementor-arrows-position-inside elementor-pagination-position-inside elementor-widget elementor-widget-slides" data-id="546f932" data-element_type="widget" data-settings="{&quot;navigation&quot;:&quot;both&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;infinite&quot;:&quot;yes&quot;,&quot;transition&quot;:&quot;slide&quot;,&quot;transition_speed&quot;:500}" data-widget_type="slides.default">
                                          <div class="elementor-widget-container">
                                             <div class="elementor-swiper">
                                                <div class="elementor-slides-wrapper elementor-main-swiper swiper-container" dir="ltr" data-animation="fadeInUp">
                                                   <div class="swiper-wrapper elementor-slides">
                                                      <div class="elementor-repeater-item-268147a swiper-slide">
                                                         <div class="swiper-slide-bg"></div>
                                                         <div class="swiper-slide-inner" >
                                                            <div class="swiper-slide-contents"></div>
                                                         </div>
                                                      </div>
                                                      <div class="elementor-repeater-item-0fe3923 swiper-slide">
                                                         <div class="swiper-slide-bg"></div>
                                                         <div class="swiper-slide-inner" >
                                                            <div class="swiper-slide-contents"></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="swiper-pagination"></div>
                                                   <div class="elementor-swiper-button elementor-swiper-button-prev">
                                                      <i class="eicon-chevron-left" aria-hidden="true"></i>
                                                      <span class="elementor-screen-only">Previous</span>
                                                   </div>
                                                   <div class="elementor-swiper-button elementor-swiper-button-next">
                                                      <i class="eicon-chevron-right" aria-hidden="true"></i>
                                                      <span class="elementor-screen-only">Next</span>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div> -->
                                       <div class="elementor-element elementor-element-1e8f9c3 elementor-widget elementor-widget-theme-archive-title elementor-page-title elementor-widget-heading" data-id="1e8f9c3" data-element_type="widget" data-widget_type="theme-archive-title.default">
                                          <div class="elementor-widget-container">
                                             <h1 class="elementor-heading-title elementor-size-default">Category: Tin Tức</h1>
                                          </div>
                                       </div>
                                       <section class="elementor-section elementor-inner-section elementor-element elementor-element-1852bfd elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1852bfd" data-element_type="section">
                                          <div class="elementor-container elementor-column-gap-default">
                                             <div class="elementor-row">
                                                <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-954d644" data-id="954d644" data-element_type="column">
                                                   <div class="elementor-column-wrap elementor-element-populated">
                                                      <div class="elementor-widget-wrap">
                                                         <div class="elementor-element elementor-element-062738d elementor-grid-tablet-3 elementor-grid-3 elementor-grid-mobile-1 elementor-posts--thumbnail-top elementor-widget elementor-widget-archive-posts" data-id="062738d" data-element_type="widget" data-settings="{&quot;archive_classic_columns_tablet&quot;:&quot;3&quot;,&quot;archive_classic_columns&quot;:&quot;3&quot;,&quot;archive_classic_columns_mobile&quot;:&quot;1&quot;,&quot;archive_classic_row_gap&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:35,&quot;sizes&quot;:[]}}" data-widget_type="archive-posts.archive_classic">
                                                            <div class="elementor-widget-container">
                                                               <div class="elementor-posts-container elementor-posts elementor-posts--skin-classic elementor-grid">
                                                                <?php foreach ($baiviet1 as $key => $value) { ?>
                                                                  <article class="elementor-post elementor-grid-item post-925 post type-post status-publish format-standard has-post-thumbnail hentry">
                                                                     <a class="elementor-post__thumbnail__link" href="blog/<?=$value['url']?>" >
                                                                        <div class="elementor-post__thumbnail">
                                                                           <img width="586" height="586"   alt="<?=$value['name']?>" loading="lazy" data-srcset="<?=$value['hinh_anh']?>" data-sizes="(max-width: 586px) 100vw, 586px" class="attachment-large size-large lazyload" src="<?=$value['hinh_anh']?>" />
                                                                           <noscript><img width="586" height="586" src="<?=$value['hinh_anh']?>" class="attachment-large size-large" alt="<?=$value['name']?>" loading="lazy" srcset="<?=$value['hinh_anh']?>" sizes="(max-width: 586px) 100vw, 586px" /></noscript>
                                                                        </div>
                                                                     </a>
                                                                     <div class="elementor-post__text">
                                                                        <h3 class="elementor-post__title">
                                                                           <a href="blog/<?=$value['url']?>" >
                                                                           <?=$value['name']?> </a>
                                                                        </h3>
                                                                        <div class="elementor-post__excerpt">
                                                                           <p><?=$value['mo_ta']?></p>
                                                                        </div>
                                                                        <a class="elementor-post__read-more" href="blog/<?=$value['url']?>" >
                                                                        Đọc Thêm »      </a>
                                                                     </div>
                                                                  </article>
                                                                <?php } ?>  
                                                               </div>
                                                               <nav class="elementor-pagination" role="navigation" aria-label="Pagination">
                                                                  <?php for ($i=1; $i <=$sotrang ; $i++) { 
                                                                     if ($trang==$i) {
                                                                       echo ' <span aria-current="page" class="page-numbers current"><span class="elementor-screen-only">Page</span>'.$i.'</span>';
                                                                     }else{
                                                                      echo '<a class="page-numbers" href="blog?p='.$i.'"><span class="elementor-screen-only">Page</span>'.$i.'</a>   ';
                                                                     }
                                                                  } ?>
                                                               </nav>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-ba05110" data-id="ba05110" data-element_type="column">
                                                   <div class="elementor-column-wrap elementor-element-populated">
                                                      <div class="elementor-widget-wrap">
                                                         <div class="elementor-element elementor-element-f2e78dc elementor-search-form--skin-classic elementor-search-form--button-type-icon elementor-search-form--icon-search elementor-widget elementor-widget-search-form">
                                                            <div class="elementor-widget-container">
                                                               <form class="elementor-search-form" role="search" action="search" method="post">
                                                                  <div class="elementor-search-form__container">
                                                                     <input placeholder="Nhập từ khóa ...." class="elementor-search-form__input" type="search" name="keyword" title="Search" value="" required>
                                                                     <button class="elementor-search-form__submit" type="submit" name="btnsearch" title="Search" aria-label="Search">
                                                                     <i class="fa fa-search" aria-hidden="true"></i>
                                                                     <span class="elementor-screen-only">Search</span>
                                                                     </button>
                                                                  </div>
                                                               </form>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-3d8de04 elementor-widget elementor-widget-heading" data-id="3d8de04" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                               <h4 class="elementor-heading-title elementor-size-default">Bài viết mới</h4>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-5b59c61 elementor-widget-divider--view-line elementor-widget elementor-widget-divider" data-id="5b59c61" data-element_type="widget" data-widget_type="divider.default">
                                                            <div class="elementor-widget-container">
                                                               <div class="elementor-divider">
                                                                  <span class="elementor-divider-separator">
                                                                  </span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-ac53f60 elementor-grid-1 elementor-grid-tablet-1 elementor-posts--thumbnail-none elementor-grid-mobile-1 elementor-widget elementor-widget-posts" data-id="ac53f60" data-element_type="widget" data-settings="{&quot;classic_columns&quot;:&quot;1&quot;,&quot;classic_columns_tablet&quot;:&quot;1&quot;,&quot;classic_row_gap&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:20,&quot;sizes&quot;:[]},&quot;classic_columns_mobile&quot;:&quot;1&quot;}" data-widget_type="posts.classic">
                                                            <div class="elementor-widget-container">
                                                               <div class="elementor-posts-container elementor-posts elementor-posts--skin-classic elementor-grid">
                                                                <?php foreach ($new as $key => $value) { ?>
                                                                  <article class="elementor-post elementor-grid-item post-925 post type-post status-publish format-standard has-post-thumbnail hentry category-tin-tuc tag-tui-vai-bo tag-tui-vai-canvas">
                                                                     <div class="elementor-post__text">
                                                                        <h6 class="elementor-post__title">
                                                                           <a href="blog/<?=$value['url']?>" >
                                                                           <?=$value['name']?>      </a>
                                                                        </h6>
                                                                     </div>
                                                                  </article>
                                                                <?php } ?>
                                                               </div>
                                                            </div>
                                                         </div>
                                                        <!--  <div class="elementor-element elementor-element-da67c17 elementor-widget elementor-widget-heading" data-id="da67c17" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                               <h4 class="elementor-heading-title elementor-size-default">Chuyên mục</h4>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-ceddac1 elementor-widget-divider--view-line elementor-widget elementor-widget-divider" data-id="ceddac1" data-element_type="widget" data-widget_type="divider.default">
                                                            <div class="elementor-widget-container">
                                                               <div class="elementor-divider">
                                                                  <span class="elementor-divider-separator">
                                                                  </span>
                                                               </div>
                                                            </div>
                                                         </div> -->
                                                         <!-- <div class="elementor-element elementor-element-db49a3a elementor-widget elementor-widget-text-editor" data-id="db49a3a" data-element_type="widget" data-widget_type="text-editor.default">
                                                            <div class="elementor-widget-container">
                                                               <div class="elementor-text-editor elementor-clearfix">
                                                                  <ul class="chuyen-muc">
                                                                     <li><i class="fa fa-folder-open"></i> <a href="../category/tin-tuc/index.html">Tin Tức</a></li>
                                                                     <li><i class="fa fa-folder-open"></i> <a href="../category/tuyen-dung/index.html">Tuyển Dụng</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div> -->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </section>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
               </div>
            </div>
            <!-- ast-container -->
         </div>
		
	
<?php }else{
  echo '<h1>Nội dung đang được cập nhật!</h1>';
} ?>