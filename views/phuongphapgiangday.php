<div class="BG"></div>
            <div class="BodyPage" id='BodyPage'>
                <div class="BodySub bc-s Intro">
                    <h1 class="d-none"><?=$page['title'] ?></h1>
                    <div class="HCT c-bo">Phương pháp giảng dạy</div>
                    <div class="HFC">

                        <div class="I1 c-sgray fw-bd PPT1">Học tiếng Anh online cùng IGEMS

                            <img src="https://igems.com.vn/uploads/ld3.png" alt="Học tiếng anh cùng Igems" href="">
                        </div>
<!--                        <div class="I1 c-sgray fw-bd PPT1">-->
<!--                            -->
<!--                        </div>-->
                        <div class="I1 I4">
                            <div class="I4Item">
                                <div class="d-flex"><i class="far fa-smile IIcon" aria-hidden="true"></i><h2
                                        class="m-0 fsz-u fw-n c-sgray fw-bd fs-20">Tạo cảm hứng</h2></div>
                                <h3 class="m-0 fsz-u fw-n c-gray">Từng chương trình của IGEMS đề được phân tích để phù hợp với từng
                                    học viên.</h3>
                            </div>
                            <div class="I4Item">
                                <div class="d-flex"><i class="fas fa-user-check IIcon" aria-hidden="true"></i><h2
                                        class="m-0 fsz-u fw-n c-sgray fw-bd fs-20">Đánh giá</h2></div>
                                <h3 class="m-0 fsz-u fw-n c-gray">Việc đánh giá sau buổi học giúp học viên có thể nắm bắt được tình
                                    hình học của mình.</h3>
                            </div>
                            <div class="I4Item">
                                <div class="d-flex"><i class="far fa-calendar-alt IIcon" aria-hidden="true"></i><h2
                                        class="m-0 fsz-u fw-n c-sgray fw-bd fs-20">Đặt lịch học và nghỉ</h2></div>
                                <h3 class="m-0 fsz-u fw-n c-gray">Việc đặt lịch học trở nên dễ dàng với app IGEMS.</h3>
                            </div>
                            <div class="I4Item">
                                <div class="d-flex"><i class="fas fa-headset IIcon" aria-hidden="true"></i><h2
                                        class="m-0 fsz-u fw-n c-sgray fw-bd fs-20">Hệ thống hỗ trợ 24/7</h2></div>
                                <h3 class="m-0 fsz-u fw-n c-gray">Tổng đài chăm sóc của IGEMS luôn sẵn sàng hỗ trợ những vướng mắc của
                                    khách hàng.</h3>
                            </div>
                        </div>
                    </div>
                    <img src="template/img/Background.jpg" alt="Học tiếng Anh online cùng IGEMS1" class="d-none">
                    <img src="template/img/Background.jpg" alt="Học tiếng Anh online cùng IGEMS2" class="d-none">
                    <img src="template/img/Background.jpg" alt="Học tiếng Anh online cùng IGEMS3" class="d-none">
                    <div class="IG">
                        <div class="IG2">
                            <div class="HCT HVT PPTT c-w">IGEMS GIÚP HỌC VIÊN HỌC TẬP THEO PHƯƠNG PHÁP TIÊN TIẾN, HIỆU
                                QUẢ</div>
                            <div class="PPTBenefit">
                                <div class="PPTBI">
                                    <div class="d-flex"><i class="far fa-hand-peace IIcon c-bo " aria-hidden="true"></i>
                                        <div class="fw-bd fs-30">Nội dung bài học phong phú, hấp dẫn</div>
                                    </div>
                                    <div class="c-gray">Nội dung bài học được thể hiện bằng slide được thiết kế riêng
                                        biệt, bắt mắt giúp học viên hứng thú với bài học. Đặc biệt nhiều hoạt động được
                                        thiết kế riêng biệt.</div>
                                </div>
                                <div class="PPTBI">
                                    <div class="d-flex"><i class="fas fa-people-arrows IIcon c-bo "
                                            aria-hidden="true"></i>
                                        <div class="fw-bd fs-30">Học trực tuyến 1 kèm 1 với giáo viên</div>
                                    </div>
                                    <div class="c-gray">Lớp học IGEMS giúp học viên tương tác trực tiếp với huấn luyện
                                        viên thông qua màn hình hiển thị slide bài giảng có thể thao tác viết, vẽ,
                                        khoanh, tô màu. Nút giơ tay giúp học viên được thầy cô hỗ trợ kịp thời trong lớp
                                        học.</div>
                                </div>
                                <div class="PPTBI">
                                    <div class="d-flex"><i class="fas fa-skating IIcon c-bo " aria-hidden="true"></i>
                                        <div class="fw-bd fs-30">Luyện tập, vừa học vừa chơi</div>
                                    </div>
                                    <div class="c-gray">Học viên được luyện tập dễ dàng, không giới hạn trên kho bài tập
                                        của IGEMS với hệ thống câu hỏi đa dạng, trực quan.</div>
                                </div>
                                <div class="PPTBI">
                                    <div class="d-flex"><i class="fas fa-expand-arrows-alt IIcon c-bo "
                                            aria-hidden="true"></i>
                                        <div class="fw-bd fs-30">Luyện tập mở rộng</div>
                                    </div>
                                    <div class="c-gray">Với hệ thống bài tập và video được cung cấp. Học viên có thể dễ
                                        dàng ôn luyện ngay tại nhà.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>