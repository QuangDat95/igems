<?php
$giaovien = $data->giaovien1();
?>
<div class="BG"></div>
<div class="BodyPage" id='BodyPage'>
  <div class="TeacherList">
    <h1 class="d-none"><?= $page['title'] ?></h1>
    <h2 class="HCT">ĐỘI NGŨ GIÁO VIÊN IGEMS</h2>
    <div class="TeacherListPage">
      <!-- <div class="PageList">
          <div class="PageID">1</div>
          <div class="PageID">2</div>
      </div> -->
      <?php foreach ($giaovien as $value) { ?>
        <div>
          <div class="HomeTeacherItem TCLI">
            <div class="HomeTeacherImg" style="background-image: url(<?= $value['hinh_anh'] ?>);">
              <img class="d-none" src="<?= $value['hinh_anh'] ?>" alt="<?= $value['name'] ?>">
              <div class="HomeTeacherMore">
                <div class="HTMore"><?= $value['mo_ta'] ?></div>
                <div class="HTMore"></div>
                <div class="HTMore"></div>
              </div>
            </div>
            <h3 class="fsz-u m-0 HomeTeacherName"><?= $value['name'] ?></h3>
            <div class="HomeTeacherInfor"><?= $value['nang_luc'] ?></div>
              <div class="HomeTeacherInfor">
                  <?php
                  if(!empty($value['kinh_nghiem'])) {
                      ?>
                      <strong>Kinh nghiệm:</strong> <?php echo $value['kinh_nghiem']; ?>
                      <?php
                  }
                  ?>
              </div>
              <div class="clearfix"></div>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
  <script src="template/giaovien.js" type="module"></script>