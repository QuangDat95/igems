<style>
    .HWIcon{
        width: 50px;
        margin-left: 268px;
    }
    .HUPC{
        padding: 46px;

    }


    @media all and (max-width: 430px) {
        .HUPP{
            left: -124px;
            bottom: 97px;

        }
        .HWItitle{
            position: absolute;
            top: -10%;
            right: 5px;
            font-weight: bold;
            transition: 0.5s;
            width: 100%;
            display: none;

        }
        .HWI:hover > .HWIcon{
            bottom: 83%;
            transition: 0.5s;
            font-size: 30px;

        }

    }




</style>
<meta >
<div class="BG"></div>
            <div class="BodyPage" id='BodyPage'>
                <div class="BodySub bc-s Intro">
                    <div class="PPSC">
                        <div class="PPSC2">
                            <div class="PPS d-flex fd-c">
                                <h1 class="d-none"><?=$page['title'] ?></h1>
                                <div class="HTT2 c-w ta-c PPSTitle m-0">5 giai đoạn và lợi ích của phương pháp học 1 - 1
                                </div>
                                <div class="PPSL">
                                    <div class="d-flex fd-c PPSItem" id="PPSI1"><i id='PPSI11'
                                            class="far fa-lightbulb c-w fs-60 PPSIcon" aria-hidden="true"></i>
                                        <h2 class="m-0 fw-bd c-bo fs-20">Motivation</h2>
                                        <h3 class="fsz-u m-0 fw-n c-w">Tạo hứng thú</h3>
                                        <div class="c-w">Giúp học viên làm quen với đề tài, kích thích động lực học tập
                                            của học viên.</div>
                                    </div>
                                    <div class="d-flex fd-c PPSItem" id="PPSI2"><i id="PPSI22"
                                            class="fas fa-book c-w fs-60 PPSIcon" aria-hidden="true"></i>
                                        <h2 class="m-0 fw-bd c-bo fs-20">Learning</h2>
                                        <h3 class="fsz-u m-0 fw-n c-w">Tiếp nhận kiến thức</h3>
                                        <div class="c-w">Cung cấp nội dung kiến thức, kỹ năng trong mục tiêu học tập để
                                            học viên đạt được chiến lược học tập hiệu quả.</div>
                                    </div>
                                    <div class="d-flex fd-c PPSItem" id="PPSI3">
                                        <i id="PPSI33"
                                            class="far fa-handshake c-w fs-60 PPSIcon l-38x" aria-hidden="true">

                                        </i>
                                        <h2 class="m-0 fw-bd c-bo fs-20">Interaction</h2>
                                        <h3 class="fsz-u m-0 fw-n c-w">Tương tác</h3>
                                        <div class="c-w">Thông qua trao đổi, phân tích khả năng của học viên so với nội
                                            dung bài học, giúp học viên hiểu sâu mục tiêu học tập.</div>
                                    </div>
                                    <div class="d-flex fd-c PPSItem" id="PPSI4"><i id="PPSI44"
                                            class="fas fa-user c-w fs-60 l-40x PPSIcon" aria-hidden="true"></i>
                                        <h2 class="m-0 fw-bd c-bo fs-20">Practice</h2>
                                        <h3 class="fsz-u m-0 fw-n c-w">Luyện tập</h3>
                                        <div class="c-w">Giúp học viên tập giải quyết một số vấn đề liên quan đến mục
                                            tiêu học tập với sự hỗ trợ của thầy cô. Trao cơ hội để học viên vận dụng
                                            kiến thức, kỹ năng tự giải quyết một số vấn đề trong học tập.</div>
                                    </div>
                                    <div class="d-flex fd-c PPSItem" id="PPSI5"><i id="PPSI55"
                                            class="far fa-id-badge c-w fs-60 PPSIcon" aria-hidden="true"></i>
                                        <h2 class="m-0 fw-bd c-bo fs-20">Evaluation</h2>
                                        <h3 class="fsz-u m-0 fw-n c-w">Đánh giá</h3>
                                        <div class="c-w">Đánh giá năng lực của học viên. Chỉ ra những điểm tốt, điểm yếu
                                            để đặt ra định hướng học tập hiệu quả.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="template/img/Intro3.jpg" alt="Học tiếng Anh online cùng IGEMS1" class="d-none">
                    <img src="template/img/Intro3.jpg" alt="Học tiếng Anh online cùng IGEMS2" class="d-none">
                    <img src="template/img/Intro3.jpg" alt="Học tiếng Anh online cùng IGEMS3" class="d-none">
                    <div class="HomeWhy">
                        <div class="HCT">VÌ SAO NÊN CHỌN IGEMS</div>
                        <div class="HWL">
                            <div class="HWI HUPC" id='why1'><i class="fas fa-user-friends HWIcon HUPP" aria-hidden="true"></i>
                                <div class="HWItext">Lớp học trực tuyến 1 kèm 1 cá nhân hóa</div>
                                <div class="HWItitle">Lớp học trực tuyến 1 kèm 1 cá nhân hóa</div>
                                <div class="HWItext2">Tối ưu hóa thời gian tương tác với huấn luyện viên. Học
                                    viên được tương tác 100% thời lượng lớp học với thầy cô, tăng gấp 5 lần so với học
                                    tại trung tâm. Tham gia lớp học 1-1, học viên được kèm cặp từ những kiến thức nhỏ
                                    nhất mà bản thân còn thiếu bằng sự tận tình của thầy cô. Học viên sẽ được sắp xếp và
                                    xây dựng lộ trình học riêng biệt để phù hợp với khả năng và nhu cầu của mình.</div>
                            </div>
                            <div class="HWI bg-b2 HUPC" id='why2'><i class="fas fa-book-reader HWIcon HUPP"
                                    aria-hidden="true"></i>
                                <div class="HWItext">Lựa chọn huấn luyện viên và giáo trình</div>
                                <div class="HWItitle">Lựa chọn huấn luyện viên và giáo trình</div>
                                <div class="HWItext2">Huấn luyện viên phù hợp là yếu tố rất quan trong để giúp học viên
                                    có thể tiến bộ nhanh nhất và lớp học diễn ra hiệu quả nhất. Mỗi học viên khi tham
                                    gia học tại IGEMS đều được lựa chọn huấn luyện viên mà mình cảm thấy phù hợp nhất.
                                    Tạo được cảm hứng học tập tiếng anh luôn là mục tiêu hàng đầu của huấn luyện viên
                                    tại IGEMS.</div>
                            </div>
                            <div class="HWI HUPC" id='why3'><i class="fas fa-user-graduate HWIcon HUPP" aria-hidden="true"></i>
                                <div class="HWItext">Huấn luyện viên được đào tạo chuẩn Mỹ</div>
                                <div class="HWItitle">Huấn luyện viên được đào tạo chuẩn Mỹ</div>
                                <div class="HWItext2">Tất cả các huấn luyện viên của IGEMS đều được đào tạo bởi chuyên
                                    gia giáo dục hàng đầu thế giới. Đặc biệt hơn hết là các huấn luyện viên được đào tạo
                                    phương pháp giảng dạy từ các chuyên gia giáo dục của tổ chức giáo dục Quantum
                                    Learning – tổ chức giáo dục hàng đầu thế giới.</div>
                            </div>
                            <div class="HWI bg-b2 HUPC" id='why4'><i class="fas fa-globe-asia HWIcon HUPP" aria-hidden="true"></i>
                                <div class="HWItext">Tiên phong công nghệ giáo dục</div>
                                <div class="HWItitle">Tiên phong công nghệ giáo dục</div>
                                <div class="HWItext2">IGEMS tự hào là đơn vị tiên phong áp dụng công nghệ trong công tác
                                    giảng dạy. Mỗi bài giảng đều được áp dụng công nghệ để tạo ra sự húng thú cho học
                                    viên. Đồng thời, các buổi học sẽ được quản lý, theo sát bởi hệ thống quản lý chất
                                    lượng của IGEMS.</div>
                            </div>
                            <div class="HWI HUPC" id='why5'><i class="fas fa-laptop HWIcon HUPP" aria-hidden="true"></i>
                                <div class="HWItext">Theo dõi tiến độ học của học viên với app IGEMS</div>
                                <div class="HWItitle">Theo dõi tiến độ học với app IGEMS</div>
                                <div class="HWItext2">Thấu hiểu nỗi lo của hầu hết học viên và phụ huynh, IGEMS tạo ra
                                    app quản lý tự động để học viên dễ dàng theo dõi tiến độ học tập. Lộ trình học sẽ
                                    được cập nhật liên tục trên app, vì vậy học viên sẽ dễ dàng theo dõi được kiến thức
                                    đã được học để dễ dàng ôn tập và chuẩn bị những bài học sắp tới. Kết quả, đánh giá
                                    buổi học được cập nhật liên tục trên app.</div>
                            </div>
                            <div class="HWI bg-b2 HUPC" id='why6'><i class="fas fa-dollar-sign HWIcon HUPP"
                                    aria-hidden="true"></i>
                                <div class="HWItext">Tiết kiệm thời gian và chi phí học tập</div>
                                <div class="HWItitle">Tiết kiệm thời gian và chi phí học tập</div>
                                <div class="HWItext2">Học tại nhà giúp học viên tiết kiệm thời gian. Học viên sẽ phát
                                    huy tính tự giác của bản thân trong việc lên lớp học cùng thầy cô. Đồng thời, tham
                                    gia học online giúp học viên tiết kiệm chi phí đi lại cũng như chi phí học tập. Chi
                                    phí mỗi buổi học chỉ bằng ½ chi phí học tại trung tâm. IGEMS Cam kết hiệu quả gấp 5
                                    lần so với học trực tiếp.</div>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="template/phuongphaphoc.js" type="module"></script>
