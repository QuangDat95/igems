<?php
  $danhmuc=$page['data'];
  if (sizeof($danhmuc)>0) { 
    $thisurl  = isset($_GET['url']) ? $_GET['url'] : null;
    $url  = rtrim($thisurl, '/');
    $url  = explode('/', $thisurl);
    $baiviet= $data->getblog_ofcate($danhmuc['id']);
    $tongbv= count($baiviet);
    $sotrang= ceil($tongbv/10);
    $trang= $url[1];
    $baiviet1= $data->getblog_ofcate1($danhmuc['id'],$trang);
    $newpost= $data->newpost();
    $khuyenmai= $data->khuyenmai(12);
?>

<style type="text/css">
 
.pagination {
  width: 100%;
  margin-left: 50px;
}
.pagination a {
  display: block;
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
  transition: background-color .3s;
}
 
.pagination a.active {
    background-color: #0ab1e7;
    color: white;
}
 
.pagination a:hover:not(.active) {
  background-color: #ddd;
}
</style>
<!-- Body -->
            <div class="BG"></div>
            <div class="BodyPage" id='BodyPage'>
                <div class="BodySub bc-s News">
                  <h1 class="d-none"><?=$page['title'] ?></h1>
                  <img class="d-none" src="template/img/Background.jpg" alt="<?=$danhmuc['mo_ta']?>">
                  <img class="d-none" src="template/img/Background.jpg" alt="<?=$danhmuc['mo_ta']?>">
                  <img class="d-none" src="template/img/Background.jpg" alt="<?=$danhmuc['mo_ta']?>">
                    <div class="m-0 HCT c-bo"><?=$danhmuc['name']?></div>
                    <div class="d-flex NewsContainer">
                        <div class="DifNews d-flex fd-c">
                            <div class="DifNews1 d-flex fd-c">
                                <form method="post" action="search" id="frmsearch">
                                <div class="SearchContainer w-100 d-flex"><input class="SearchInput p-20"
                                        placeholder="Nhập từ khóa tìm kiếm" name="keyword" value="" required id='SearchNews'><button type="submit" 
                                        class="fw-bd c-bo bg-w p-10 SIB pointer" style="height: -webkit-fill-available;border: none;outline: none;">Search</button></div>
                                <label class="fw-bd c-w w-100 ta-l">Từ khóa</label>
                                <div class="f-w d-flex" id='KeyList'>
                                </div>
                                </form>
                            </div>
                           <!--  <script type="text/javascript">
                              function submitsearch(){
                                document.getElementById("frmsearch").submit();
                              }
                            </script> -->
                            <div class="DifNews2 d-flex fd-c">
                                <div class="w-100 d-flex fd-c"><label class="fw-bd c-w w-100 ta-l">Tin khuyến
                                        mãi</label>
                                    <div class="d-flex fd-c PML" id='PML'>
                                      <?php foreach ($khuyenmai as $key => $value) { ?>
                                        <a class="PMI d-flex fd-c m-10 pointer" href="blog/<?=$value['url']?>">
                                            <div class="HaveImg PMIimg"
                                                style="background-image: url(<?=$value['hinh_anh']?>);">
                                            </div>
                                            <div class="f1">
                                                <div class="ta-l fw-bd c-w p-10"><?=$value['name']?></div>
                                                <div class="ta-l c-w fs-14 p-10">
                                                  <span class="mr-1"><i class="fa fa-user mr-1"></i> <?=$value['tacgia']?></span> -
                                                  <span class="ml-1"><i class="fa fa-calendar-o mr-1"></i> <?=$value['ngaydang']?></span>
                                                </div>
                                            </div>
                                        </a>
                                      <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="NewsList d-flex fd-c" id='NewsList'>
                          <?php foreach ($baiviet1 as $key => $value) { ?>
                          <a class="NLI d-flex pointer" href="blog/<?=$value['url']?>">
                              <div class="HaveImg NLIimg"
                                  style="background-image: url(<?=$value['hinh_anh']?>);">
                              </div>
                              <div class="d-flex fd-c f1 jc-sb">
                                  <h2 class="m-0 fsz-u fw-n c-sgray fw-bd fs-30 p-20 ta-l NLIT"><?=$value['name']?></h2>
                                  <h3 class="m-0 fsz-u fw-n c-gray p-20 ta-l mb-20">
                                    <span class="mr-1"><i class="fa fa-user mr-1"></i> <?=$value['tacgia']?></span> -
                                    <span class="ml-1"><i class="fa fa-calendar-o mr-1"></i> <?=$value['ngaydang']?></span>
                                  </h3>
                              </div>
                          </a>
                          <?php } ?>
                            <div class="pagination">
                              <?php 
                                if ($trang>1 && $sotrang>1)
                                  echo '<a href="blog/1/'.$url[2].'">&laquo;</a>';

                                if ($trang>1 && $sotrang>1)
                                  echo '<a href="blog/'.($trang-1).'/'.$url[2].'">&lsaquo;</a>';

                                if ($trang>3)
                                  echo '<a href="blog/1/'.$url[2].'">1</a>';

                                if ($trang>4)
                                  echo '<a>...</a>';

                                for ($i=1; $i <=$sotrang ; $i++) { 
                                  if ($i >= $trang-2 && $i <= $trang+2) {
                                    if ($i==$trang) {
                                      echo '<a class="active">'.$i.'</a>';
                                    }else{
                                      echo '<a href="blog/'.$i.'/'.$url[2].'">'.$i.'</a>';
                                    }
                                  }
                                } 

                                if ($trang < $sotrang - 3)
                                  echo '<a>...</a>';

                                if ($trang < $sotrang - 2)
                                  echo '<a href="blog/'.($sotrang).'/'.$url[2].'">'.($sotrang).'</a>';

                                if ($trang<$sotrang)
                                  echo '<a href="blog/'.($trang+1).'/'.$url[2].'">&rsaquo;</a>';

                                if ($trang<$sotrang)
                                 echo '<a href="blog/'.($sotrang).'/'.$url[2].'">&raquo;</a>';
                              ?>
                              
                            </div>
                          
                        </div>

                        <div class="DifNews1 d-flex fd-c DifNews3">
                          <form method="post" action="search">
                            <div class="SearchContainer w-100 d-flex">
                                <input class="SearchInput p-20" placeholder="Nhập từ khóa tìm kiếm" name="keyword" required value="" id='SearchNews2'>
                                <button class="fw-bd c-bo bg-w p-10 SIB pointer" style="height: -webkit-fill-available;border: none;outline: none;">Search</button>
                            </div>
                            <label class="fw-bd c-w w-100 ta-l">Từ khóa</label>
                            <div class="f-w d-flex" id='KeyList2'>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
                <!-- <script src="template/tintuc.js" type="module"></script> -->
<?php }else{
  echo '<h1>Nội dung đang được cập nhật!</h1>';
} ?>