<?php
   $tintuc= $data->newpost();
   $giaovien= $data->giaovien();
?>
<!-- Body -->
            <div class="BG"></div>
            <div class="BodyPage" id='BodyPage'>
                <div class="Home">
                    <!-- HRegister -->
                    <div class="HRBG">
                        <div class="HomeRegister">
                            <div class="Why">
                                <h1 class="d-none"><?=$page['title'] ?></h1>
                                <h2 class="m-0 WT">Tiếng Anh trực tuyến số 1 Việt Nam</h2>
                                <img class="d-none" src="template/img/Background.jpg" alt="Tiếng Anh trực tuyến số 1 Việt Nam">
                                <img class="d-none" src="template/img/Background.jpg" alt="Tiếng Anh trực tuyến số 1 Việt Nam">
                                <img class="d-none" src="template/img/Background.jpg" alt="Tiếng Anh trực tuyến số 1 Việt Nam">
                                <div class="WL">
                                    <div><b>IGEMS</b> là chương trình học tiếng Anh trực tuyến 1 kèm 1 hàng đầu Việt Nam tới từ hệ thống Anh ngữ quốc tế</div>
                                    <div><b>GEMS EDU</b> - đối tác độc quyền tại Việt Nam của tổ chứ giáo dục QUANTUM LEARNING ở Mỹ. Mang lại hiệu quả học tập gấp 5 lần khi học tại trung tâm và giá chỉ bằng 1/2.</div>
                                </div>
                                <div><i>*QUANTUM LEARNING là tổ chức giáo dục hàng đầu thế giới với hơn 40 năm phát
                                        triển tại 85 quốc gia</i></div>
                            </div>
                            <form method="post" action="dangky">
                            <div class="FormRegister">
                                <div class="FRT HRT">ĐĂNG KÝ HỌC THỬ MIỄN PHÍ</div><input name="name" class="FRI" type="text" placeholder="Tên" required>
                                <div class="c-r"></div><input name="age" class="FRI" type="number" placeholder="Tuổi" required>
                                <div class="c-r"></div><input name="email" class="FRI" type="email" placeholder="Email" required>
                                <div class="c-r"></div><input name="phone" class="FRI" type="number" placeholder="Số điện thoại" required>
                                <div class="c-r"></div>
                                <button class="FRB" name="btngui" style="border: none;outline: none;">ĐĂNG KÝ</button>
                            </div>
                            </form>
                        </div>
                    </div>

                    <!-- HWhy -->
                    <div class="HomeWhy">
                        <h2 class="m-0 fsz-u fw-n HCT">VÌ SAO NÊN CHỌN IGEMS</h2>
                        <div class="HWL">                            <div id="why1" class="HWI">                                                        <img src="https://igems.com.vn/uploads/home/1.png" alt="" width="100%">                                                                    </div>                            <div id="why1" class="HWI">                                                        <img src="https://igems.com.vn/uploads/home/2.png" alt="" width="100%">                                                                    </div>                            <div id="why1" class="HWI">                                                        <img src="https://igems.com.vn/uploads/home/3.png" alt="" width="100%">                                                                    </div>                            <div id="why1" class="HWI">                                                        <img src="https://igems.com.vn/uploads/home/4.png" alt="" width="100%">                                                                    </div>                            <div id="why1" class="HWI">                                                        <img src="https://igems.com.vn/uploads/home/5.png" alt="" width="100%">                                                                    </div>                            <div id="why1" class="HWI">                                                        <img src="https://igems.com.vn/uploads/home/6.png" alt="" width="100%">                                                                    </div>                        
                    </div>

                    <!-- HTech -->
                    <div class="HomeTech">
                        <div class="HTT">
                            <div class="HTTitle">
                                <h2 class="m-0 fsz-u pointer" id='Tech1'>Công nghệ</h2>
                                <h2 class="m-0 fsz-u pointer" id='Tech2'>Lộ trình</h2>
                                <h2 class="m-0 fsz-u pointer" id='Tech3'>Phương pháp</h2>
                                <div id="HTTBar" class="HTTBar HTTBar1"></div>
                            </div>
                            <div class="HTT2" id='Tech4'>TIÊN PHONG CÔNG NGHỆ GIÁO DỤC</div>
                            <div class="ta-l" id='Tech5'>Với phương pháp giáo dục 1 kèm 1 giúp tăng 5 lần thời lượng tương tác của học viên với thầy cô. Giúp học viên cải thiện rõ 4 kĩ năng đặc biệt là giao tiếp. Đặc biệt ở IGEMS là có app quản lý tiến độ học tập.</div>
                            <div class="HTTButton">
                                <div id='HTTB1' class="HTTB1">ĐĂNG KÝ NGAY</div><a class="HTTB2" href="phuongphaphoc">XEM
                                    THÊM</a>
                            </div>
                        </div>
                        <div class="HTImg"></div>
                    </div>

                    <!-- HomeTeacher -->
                    <div class="HomeTeacher">
                        <h3 class="m-0 fsz-u fw-n HCT">ĐỘI NGŨ GIÁO VIÊN TUYỆT VỜI</h3>
                        <div class="HomeTeacherList">
                            <!-- <div class="HomeTeacherItem">
                                <div class="HomeTeacherImg" style="background-image: url(template/data/img/T1.jpg);">
                                    <div class="HomeTeacherMore"></div>
                                </div>
                                <div class="HomeTeacherName">Megan Krause</div>
                                <div class="HomeTeacherInfor">Cử nhân trường Design School of Southern Africa</div>
                            </div>
                            <div class="HomeTeacherItem">
                                <div class="HomeTeacherImg" style="background-image: url(template/data/img/T3.jpg);">
                                    <div class="HomeTeacherMore"></div>
                                </div>
                                <div class="HomeTeacherName">Trần Ngọc Sơn</div>
                                <div class="HomeTeacherInfor">Cử nhân ngôn ngữ Anh - Đại học ngoại ngữ Hà Nội</div>
                            </div> -->
                            <?php foreach ($giaovien as $value) { ?>
                            <div class="HomeTeacherItem">
                                <div class="HomeTeacherImg" style="background-image: url(<?=$value['hinh_anh']?>);">
                                    <div class="HomeTeacherMore">
                                        <div class="HTMore"><?=$value['mo_ta']?></div>
                                    </div>
                                </div>
                                <div class="HomeTeacherName"><?=$value['name']?></div>
                                <div class="HomeTeacherInfor"><?=$value['nang_luc']?></div>
                            </div>
                            <?php } ?>
                           <!--  <div class="HomeTeacherItem">
                                <div class="HomeTeacherImg" style="background-image: url(template/data/img/T4.jpg);">
                                    <div class="HomeTeacherMore"></div>
                                </div>
                                <div class="HomeTeacherName">Võ Trần Thanh Phương</div>
                                <div class="HomeTeacherInfor"></div>
                            </div> -->
                        </div><a class="HTButton" href="giaovien">Xem thêm</a>
                    </div>

                    <!-- HomePromise -->
                    <div class="HomePromise">
                        <div class="HPIC"></div>
                        <div class="HPT">
                            <h3 class="m-0 fsz-u fw-n WT">Thành quả của IGEMS</h3>
                            <div class="HPL">
                                <div class="WLB WL1"></div>
                                <div class="WLI WL2"><i class="far fa-check-circle c-o" aria-hidden="true"></i><span class="WLIT fw-bd">95% học viên tiến bộ rõ rệt</span></div>
                                <div class="WLB WL3">Học viên cải thiện rõ rệt kĩ năng Tiếng Anh, đặc biệt là giao tiếp
                                </div>
                                <div class="WLI WL4"><i class="far fa-check-circle c-o" aria-hidden="true"></i><span class="WLIT fw-bd">90% học viên cải thiện điểm số</span></div>
                                <div class="WLB WL5">Học viên của IGEMS cải thiện vượt bậc đáng kể điểm số trên lớp
                                </div>
                                <div class="WLI WL6"><i class="far fa-check-circle c-o" aria-hidden="true"></i><span class="WLIT fw-bd">85% học viên tự tin hơn</span></div>
                                <div class="WLB WL5">Trên 85% học viên của IGEMS tự tin giao tiếp và đạt được chiến lược học tập ngoại ngữ của mình</div>
                                <div class="WLI WL4"><i class="far fa-check-circle c-o" aria-hidden="true"></i><span class="WLIT fw-bd">Với hơn 2000 học viên theo học</span></div>
                                <div class="WLB WL7">Hơn 3 năm hoạt động, IGEMS đã tỗ chức thành công hơn 10.000 lớp demo với hơn 2000 học viên theo học tại IGEMS</div>
                            </div>
                        </div>
                    </div>

                    <!-- HoneVideo -->
                    <div class="HomeVideo">
                        <div class="HCT HVT">LỚP HỌC TRỰC TUYẾN TẠI IGEMS DIỄN RA NHƯ THẾ NÀO?</div>
                        <div class="HVC">
                            <iframe src="https://www.youtube.com/embed/6MYPfUA122k" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" class="HVI" title="https://www.youtube.com/embed/6MYPfUA122k"></iframe>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/cBjjj99g2LI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" class="HVI" title="https://www.youtube.com/embed/cBjjj99g2LI"></iframe>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/K3yp4l_ysNg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" class="HVI"
                                    title="https://www.youtube.com/embed/K3yp4l_ysNg"></iframe>
                        </div>
                    </div>

                    <!-- HomeFeel -->
                    <div class="HomeFeel">
                        <div class="HCT">ĐÁNH GIÁ TỪ KHÁCH HÀNG</div>
                        <div class="HFC">
                            <div class="HFT1">
                                <div class="HFT1T">Khách hàng nói gì về IGEMS ?</div>
                                <a href="camnhan">
                                    <div class="HTTB1 HFTB">Xem thêm đánh giá</div>
                                </a>
                            </div>
                            <div class="HFT2">
                                <div id='HFT2C' class="HFT2C HF1">
                                    <div class="HF0">
                                        <div class="HFData">"Ban đầu mình còn rất nghi ngờ về chương trình học này, nhưng sau khi cho con học thử thì mình đã thay đổi hoàn toàn suy nghĩ. Con rất thích học đặc biệt là khả năng giao tiếp tiếng Anh của con đã được cải thiện
                                            rõ rệt."</div>
                                        <div class="HFName"><i class="fas fa-user-tie fs-40" aria-hidden="true"></i>
                                            <div class="HFNameData ta-l">
                                                <div class="fw-bd f-i fs-20">Trần Thương</div>
                                                <div class="f-i">Bố bé Gia Bảo lớp IG3</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="HF0">
                                        <div class="HFData">"Ban đầu thì con rất nhút nhát, còn sợ không muốn học. Nhưng sau 1 thời gian được học với các cô thì con mạnh dạn học lên hẳn. Ở nhà con còn nói tiếng Anh rất nhiều nữa. Mình rất cám ơn các cô của trung tâm."
                                        </div>
                                        <div class="HFName"><i class="fas fa-user-tie fs-40" aria-hidden="true"></i>
                                            <div class="HFNameData ta-l">
                                                <div class="fw-bd f-i fs-20">Chị Minh Phú</div>
                                                <div class="f-i">Mẹ bé Hương lớp IG34</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="HF0">
                                        <div class="HFData">"Trong thời gian dịch bệnh này thì học theo phương pháp online này rất phù hợp. Con mình rất thích học với cô, đặc biệt mình còn theo được sát quá trình học của con. Con mình đã tiến bộ rõ rệt sau 3 tháng học online
                                            1 kèm 1 này."</div>
                                        <div class="HFName"><i class="fas fa-user-tie fs-40" aria-hidden="true"></i>
                                            <div class="HFNameData ta-l">
                                                <div class="fw-bd f-i fs-20">Chị Thùy Châm</div>
                                                <div class="f-i">Mẹ bé Naryn lớp IG54</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="HFSlideButton">
                                    <div class="HFSB bg-w" id='HFSB1'></div>
                                    <div class="HFSB bg-g" id='HFSB2'></div>
                                    <div class="HFSB bg-g" id='HFSB3'></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- HomeNews -->
                    <div class="HomeFeel HomeNews">
                        <div class="HCT">TIN TỨC</div>
                        <div class="HFC">
                            <a class="HNItem HNBig" href="blog/<?=$tintuc[0]['url']?>">
                                <div class="HNBImg" style="background-image: url(<?=$tintuc[0]['hinh_anh']?>);">
                                </div>
                                <div class="HNBTitle"><?=$tintuc[0]['name']?></div>
                                <div class="c-gray ta-l"><?=$tintuc[0]['ngaydang']?></div>
                            </a>
                            <div class="HNItem HNList">
                                 <?php foreach ($tintuc as $key => $value) {
                                     if ($key>0) { ?>
                                <a class="HNLItem" href="blog/<?=$value['url']?>">
                                    <div class="HNLItemImg" style="background-image: url(<?=$value['hinh_anh']?>);">
                                    </div>
                                    <div class="HNLItemName">
                                        <div class="c-sgray fw-bd"> <?=$value['name']?></div>
                                        <div class="c-gray"><?=$value['ngaydang']?></div>
                                    </div>
                                </a>
                               <?php } } ?>
                            </div>
                        </div>
                    </div>
                </div>