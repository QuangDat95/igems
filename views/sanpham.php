<?php
  $baiviet= $page['data'];
  if (sizeof($baiviet)>0) {
   $spmoi= $data->spmoi();
   $danhmuc1= $data->danhmucsp();
   $banchay= $data->banchay();
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel='stylesheet' id='elementor-post-40-css'  href='template/wp-content/uploads/elementor/css/post-409a4c.css?ver=1609812355' media='all' />
      <link rel='stylesheet' id='elementor-post-280-css'  href='template/wp-content/uploads/elementor/css/post-2803eb9.css?ver=1609812700' media='all' />
      <link rel='stylesheet' id='elementor-post-392-css'  href='template/wp-content/uploads/elementor/css/post-39267d3.css?ver=1609814344' media='all' />
<div id="content" class="site-content">
            <div class="ast-container">
               <div class="woocommerce-notices-wrapper"></div>
               <div data-elementor-type="product" data-elementor-id="392" class="elementor elementor-392 elementor-location-single post-443 product type-product status-publish has-post-thumbnail product_cat-tui-vai-khong-det ast-article-single ast-woo-product-no-review align-left box-shadow-0 box-shadow-0-hover ast-product-gallery-layout-horizontal ast-product-gallery-with-no-image ast-product-tabs-layout-horizontal first instock shipping-taxable product-type-simple product" data-elementor-settings="[]">
                  <div class="elementor-section-wrap">
                     <section class="elementor-section elementor-top-section elementor-element elementor-element-2f2086e elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="2f2086e" data-element_type="section">
                        <div class="elementor-container elementor-column-gap-default">
                           <div class="elementor-row">
                              <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-7e351ac" data-id="7e351ac" data-element_type="column">
                                 <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                       <div class="elementor-element elementor-element-52a76ca elementor-widget elementor-widget-woocommerce-breadcrumb" data-id="52a76ca" data-element_type="widget" data-widget_type="woocommerce-breadcrumb.default">
                                          <div class="elementor-widget-container">
                                             <nav class="woocommerce-breadcrumb"><a href="<?=HOME?>">Trang chủ</a>&nbsp;&#47;&nbsp;<a href="product/1/<?=$baiviet['danhmucurl']?>"><?=$baiviet['danhmuc']?></a>&nbsp;&#47;&nbsp;<?=$baiviet['name']?></nav>
                                          </div>
                                       </div>
                                       <section class="elementor-section elementor-inner-section elementor-element elementor-element-fd0c3f3 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="fd0c3f3" data-element_type="section">
                                          <div class="elementor-container elementor-column-gap-default">
                                             <div class="elementor-row">
                                                <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d9b3c30" data-id="d9b3c30" data-element_type="column">
                                                   <div class="elementor-column-wrap elementor-element-populated">
                                                      <div class="elementor-widget-wrap">
                                                         <div class="elementor-element elementor-element-d1461cc yes elementor-widget elementor-widget-woocommerce-product-images" data-id="d1461cc" data-element_type="widget" data-widget_type="woocommerce-product-images.default">
                                                            <div class="elementor-widget-container">
                                                               <div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 0; transition: opacity .25s ease-in-out;">
                                                                  <figure class="woocommerce-product-gallery__wrapper">
                                                                     <div data-thumb="<?=$baiviet['hinh_anh']?>" data-thumb-alt="<?=$baiviet['name']?>" class="woocommerce-product-gallery__image"><a href="<?=$baiviet['hinh_anh']?>"><img width="500" height="500" src="<?=$baiviet['hinh_anh']?>" class="wp-post-image" alt="<?=$baiviet['name']?>" loading="lazy" title="11665781_1469980106648818_3536408585201009355_n" data-caption="" data-src="<?=$baiviet['hinh_anh']?>" data-large_image="<?=$baiviet['hinh_anh']?>" data-large_image_width="500" data-large_image_height="500" srcset="<?=$baiviet['hinh_anh']?>" sizes="(max-width: 500px) 100vw, 500px" /></a></div>
                                                                  </figure>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-2cea58d" data-id="2cea58d" data-element_type="column">
                                                   <div class="elementor-column-wrap elementor-element-populated">
                                                      <div class="elementor-widget-wrap">
                                                         <div class="elementor-element elementor-element-06b7487 elementor-widget elementor-widget-woocommerce-product-title elementor-page-title elementor-widget-heading" data-id="06b7487" data-element_type="widget" data-widget_type="woocommerce-product-title.default">
                                                            <div class="elementor-widget-container">
                                                               <h3 class="product_title entry-title elementor-heading-title elementor-size-default"><?=$baiviet['name']?></h3><br>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-4b66755 elementor-widget elementor-widget-woocommerce-product-price" data-id="4b66755" data-element_type="widget" data-widget_type="woocommerce-product-price.default">
                                                            <div class="elementor-widget-container">
                                                               <p class="price"></p>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-770c085 elementor-widget elementor-widget-woocommerce-product-short-description" data-id="770c085" data-element_type="widget" data-widget_type="woocommerce-product-short-description.default">
                                                            <div class="elementor-widget-container">
                                                               <div class="woocommerce-product-details__short-description">
                                                                  <p><?=$baiviet['mo_ta']?></p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-d6be323 elementor-widget elementor-widget-woocommerce-product-add-to-cart" data-id="d6be323" data-element_type="widget" data-widget_type="woocommerce-product-add-to-cart.default">
                                                            <div class="elementor-widget-container">
                                                               <div class="elementor-add-to-cart elementor-product-simple">
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-1f7f20c elementor-widget elementor-widget-button">
                                                            <div class="elementor-widget-container">
                                                               <div class="elementor-button-wrapper">
                                                                 <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal">
                                                                   Yêu cầu báo giá
                                                                </button>
                                                               </div>

                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-0593fae elementor-woo-meta--view-inline elementor-widget elementor-widget-woocommerce-product-meta" data-id="0593fae" data-element_type="widget" data-widget_type="woocommerce-product-meta.default">
                                                            <div class="elementor-widget-container">
                                                               <div class="product_meta">
                                                                  <span class="posted_in detail-container"><span class="detail-label">Danh mục:</span> <span class="detail-content"><a href="product/1/<?=$baiviet['danhmucurl']?>" rel="tag"><?=$baiviet['danhmuc']?></a></span></span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </section>
                                       <div class="elementor-element elementor-element-d9191fb elementor-widget elementor-widget-woocommerce-product-data-tabs" data-id="d9191fb" data-element_type="widget" data-widget_type="woocommerce-product-data-tabs.default">
                                          <div class="elementor-widget-container">
                                             <div class="woocommerce-tabs wc-tabs-wrapper">
                                                <ul class="tabs wc-tabs" role="tablist" style="list-style: none;">
                                                   <li class="description_tab" id="tab-title-description" role="tab" aria-controls="tab-description">
                                                      <a href="#tab-description" class="elementor-button-link elementor-button elementor-size-sm">
                                                      Mô tả         </a>
                                                   </li>
                                                </ul>
                                                <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="tab-description" role="tabpanel" aria-labelledby="tab-title-description">
                                                   <p><?=$baiviet['noi_dung']?></p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-08bef65" data-id="08bef65" data-element_type="column">
                                 <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                       <section class="elementor-section elementor-inner-section elementor-element elementor-element-f5b7821 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="f5b7821" data-element_type="section">
                                          <div class="elementor-container elementor-column-gap-default">
                                             <div class="elementor-row">
                                                <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-9feb3e4" data-id="9feb3e4" data-element_type="column">
                                                   <div class="elementor-column-wrap elementor-element-populated">
                                                      <div class="elementor-widget-wrap">
                                                         <div class="elementor-element elementor-element-27c47d4 elementor-widget elementor-widget-heading" data-id="27c47d4" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                               <h4 class="elementor-heading-title elementor-size-default">DANH MỤC SẢN PHẨM</h4>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-883f58a elementor-widget elementor-widget-sitemap" data-id="883f58a" data-element_type="widget" data-widget_type="sitemap.default">
                                                            <div class="elementor-widget-container">
                                                               <div class="elementor-sitemap-wrap">
                                                                  <div class="elementor-sitemap-section">
                                                                     <h2 class="elementor-sitemap-title elementor-sitemap-product_cat-title">Product categories</h2>
                                                                     <ul class="elementor-sitemap-list elementor-sitemap-product_cat-list">
                                                                      <?php foreach ($danhmuc1 as $key => $value) { ?>
                                                                        <li class="elementor-sitemap-item elementor-sitemap-item-product_cat cat-item cat-item-26"><a href="product/1/<?=$value['url']?>" title="<?=$value['name']?>"><?=$value['name']?></a></li>
                                                                      <?php } ?>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </section>
                                       <section class="elementor-section elementor-inner-section elementor-element elementor-element-86d35e9 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="86d35e9" data-element_type="section">
                                          <div class="elementor-container elementor-column-gap-default">
                                             <div class="elementor-row">
                                                <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-92354c7" data-id="92354c7" data-element_type="column">
                                                   <div class="elementor-column-wrap elementor-element-populated">
                                                      <div class="elementor-widget-wrap">
                                                         <div class="elementor-element elementor-element-9d82973 elementor-widget elementor-widget-heading" data-id="9d82973" data-element_type="widget" data-widget_type="heading.default">
                                                            <div class="elementor-widget-container">
                                                               <h4 class="elementor-heading-title elementor-size-default">SẢN PHẨM BÁN CHẠY</h4>
                                                            </div>
                                                         </div>
                                                         <div class="elementor-element elementor-element-ca39d27 elementor-products-columns-1 elementor-products-columns-tablet-1 elementor-products-columns-mobile-1 elementor-product-loop-item--align-left elementor-products-grid elementor-wc-products elementor-widget elementor-widget-woocommerce-products" data-id="ca39d27" data-element_type="widget" data-widget_type="woocommerce-products.default">
                                                            <div class="elementor-widget-container">
                                                               <div class="woocommerce columns-1 ">
                                                                  <ul class="products columns-1">
                                                                    <?php foreach ($banchay as $key => $value) { ?>
                                                                     <li class="ast-article-single ast-woo-product-no-review align-left box-shadow-0 box-shadow-0-hover ast-product-gallery-layout-horizontal ast-product-tabs-layout-horizontal product type-product post-569 status-publish first instock product_cat-vo-goi-bang-vai-bo product_tag-san-pham-ban-chay has-post-thumbnail shipping-taxable product-type-simple">
                                                                        <div class="astra-shop-thumbnail-wrap">
                                                                           <a href="product/<?=$value['url']?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                              <img width="300" height="300"   alt="<?=$value['name']?>" loading="lazy" data-srcset="<?=$value['hinh_anh']?>" data-sizes="(max-width: 300px) 100vw, 300px" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail lazyload" src="<?=$value['hinh_anh']?>" />
                                                                              <noscript><img width="300" height="300" src="<?=$value['hinh_anh']?>" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="<?=$value['name']?>" loading="lazy" srcset="<?=$value['hinh_anh']?>" sizes="(max-width: 300px) 100vw, 300px" /></noscript>
                                                                           </a>
                                                                        </div>
                                                                        <div class="astra-shop-summary-wrap">
                                                                           <a href="product/<?=$value['url']?>" class="ast-loop-product__link">
                                                                              <h2 class="woocommerce-loop-product__title"><?=$value['name']?></h2>
                                                                           </a>
                                                                        </div>
                                                                     </li>
                                                                    <?php } ?>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </section>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                     <section class="elementor-section elementor-top-section elementor-element elementor-element-ff3e7f3 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="ff3e7f3" data-element_type="section">
                        <div class="elementor-container elementor-column-gap-default">
                           <div class="elementor-row">
                              <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-c0120ab" data-id="c0120ab" data-element_type="column">
                                 <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                       <div class="elementor-element elementor-element-6890e6e elementor-products-columns-tablet-4 elementor-products-columns-mobile-2 elementor-products-columns-4 elementor-products-grid elementor-wc-products show-heading-yes elementor-widget elementor-widget-woocommerce-product-related" data-id="6890e6e" data-element_type="widget" data-widget_type="woocommerce-product-related.default">
                                          <div class="elementor-widget-container">
                                             <section class="related products">
                                                <h2>Sản phẩm mới</h2>
                                                <ul class="products columns-4" style="list-style: none;">
                                                  <?php foreach ($spmoi as $key => $value) { if ($key<4) { ?>
                                                   <li class="ast-article-single ast-woo-product-no-review align-left box-shadow-0 box-shadow-0-hover ast-product-gallery-layout-horizontal ast-product-tabs-layout-horizontal product type-product post-593 status-publish first instock product_cat-tui-vai-khong-det product_tag-tui-vai-hai-au product_tag-tui-vai-khong-det has-post-thumbnail shipping-taxable product-type-simple">
                                                      <div class="astra-shop-thumbnail-wrap">
                                                         <a href="product/<?=$value['url']?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                            <img width="300" height="300"   alt="<?=$value['name']?>" loading="lazy" data-srcset="<?=$value['hinh_anh']?>" data-sizes="(max-width: 300px) 100vw, 300px" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail lazyload" src="<?=$value['hinh_anh']?>" style="height: 160px;object-fit: cover;" />
                                                            <noscript><img width="300" height="300" src="<?=$value['hinh_anh']?>" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="<?=$value['name']?>" loading="lazy" srcset="<?=$value['hinh_anh']?>" sizes="(max-width: 300px) 100vw, 300px" /></noscript>
                                                         </a>
                                                      </div>
                                                      <div class="astra-shop-summary-wrap">
                                                         <a href="product/<?=$value['url']?>" class="ast-loop-product__link">
                                                            <h2 class="woocommerce-loop-product__title"><?=$value['name']?></h2>
                                                         </a>
                                                      </div>
                                                   </li>
                                                  <?php } } ?>
                                                </ul>
                                             </section>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
               </div>
            </div>
            <!-- ast-container -->
         </div>


         <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form method="post" action="dangky">
    <div class="modal-content">
      <div class="modal-header center">
        <h5 class="modal-title" id="exampleModalLabel">Yêu cầu báo giá</h5>
        <img width="300" height="127" alt="Igems" loading="lazy" data-src="<?=$thongtin[7]['value']?>" class="attachment-large size-large ls-is-cached lazyloaded" src="<?=$thongtin[7]['value']?>">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="elementor-form-fields-wrapper elementor-labels-above">
                <div class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-name elementor-col-100 elementor-field-required">
                  <input size="1" type="text" name="name" id="form-field-name" class="elementor-field elementor-size-sm  elementor-field-textual" placeholder="Họ Tên" required="required" aria-required="true">       
                </div>
                <div class="elementor-field-type-tel elementor-field-group elementor-column elementor-field-group-message elementor-col-100 elementor-field-required" style="margin-top: 15px;">
                  <input size="1" type="tel" name="sdt" id="form-field-message" class="elementor-field elementor-size-sm  elementor-field-textual" placeholder="Số Điện Thoại" required="required" aria-required="true" pattern="[0-9()#&amp;+*-=.]+" title="Only numbers and phone characters (#, -, *, etc) are accepted.">        
                </div>
                <div class="elementor-field-type-email elementor-field-group elementor-column elementor-field-group-email elementor-col-100 elementor-field-required" style="margin-top: 15px;">
                  <input size="1" type="email" name="email" id="form-field-email" class="elementor-field elementor-size-sm  elementor-field-textual" placeholder="Email" required="required" aria-required="true">       
                </div>
                <input type="hidden" name="url" value="<?=$baiviet['name']?>">
                <div class="elementor-field-group elementor-column elementor-field-type-submit elementor-col-100 e-form__buttons">
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="btnbaogia" class="btn btn-primary btn-lg btn-block">Gửi yêu cầu</button>
      </div>
    </div>
    </form>
  </div>
</div>

<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<?php }else{
  echo '<h1>Nội dung đang được cập nhật!</h1>';
} ?>