<?php
    $baiviet= $page['data'];
    if (sizeof($baiviet)>0) {
    $danhmuc= $data->getdanhmucblog();
    $docnhieu=$data->docnhieu();
    $new= $data->newpost();
    $new_views = $data->newView();
?>
 <!-- Body -->
            <div class="BG"></div>
            <div class="BodyPage" id='BodyPage'>
                <div class="BodySub bc-s News">
                    <div class="HCT c-bo">Tin tức và sự kiện</div>
                    <div class="d-flex NewsContainer">
                        <div class="DifNews d-flex fd-c">
                            <div class="DifNews2 d-flex fd-c">
                                <div class="w-100 d-flex fd-c"><label class="fw-bd c-w w-100 ta-l">Bài viết được nhiều người quan tâm</label>
                                    <div class="d-flex fd-c PML">
                                        <?php foreach ($new_views as $key => $value_view) {
                                            $author_name = $data->getAdminById($value_view['author']);
                                            ?>
                                            <a class="PMI d-flex fd-c m-10 pointer"
                                               href="blog/<?=$value_view['url']?>">
                                                <div class="HaveImg PMIimg"
                                                     style="background-image: url(<?=$value_view['hinh_anh']?>);">
                                                </div>
                                                <div class="f1">
                                                    <div class="ta-l fw-bd c-w p-10"><?=$value_view['name']?></div>
                                                    <div class="ta-l c-w fs-14 p-10">
                                                        <span class="mr-1"><i class="fa fa-user mr-1"></i> <?=$author_name['name']?></span> -
                                                        <span class="ml-1"><i class="fa fa-calendar-o mr-1"></i> <?=$value_view['ngay_dang']?></span>
                                                    </div>
                                                </div>
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="DifNews2 d-flex fd-c">
                                <div class="w-100 d-flex fd-c"><label class="fw-bd c-w w-100 ta-l">Tin khác</label>
                                    <div class="d-flex fd-c PML">
                                       <?php foreach ($new as $key => $value) {
                                           $author_name_2 = $data->getAdminById($value['author']);

                                           ?>
                                       <a class="PMI d-flex fd-c m-10 pointer"
                                            href="blog/<?=$value['url']?>">
                                            <div class="HaveImg PMIimg"
                                                style="background-image: url(<?=$value['hinh_anh']?>);">
                                            </div>
                                            <div class="f1">
                                                <div class="ta-l fw-bd c-w p-10"><?=$value['name']?></div>
                                                <div class="ta-l c-w fs-14 p-10">
                                                    <span class="mr-1"><i class="fa fa-user mr-1"></i> <?=$author_name_2['name']?></span> -
                                                    <span class="ml-1"><i class="fa fa-calendar-o mr-1"></i> <?=$value['ngay_dang']?></span>
                                                </div>
                                            </div>
                                        </a>
                                       <?php } ?>
                                     </div>
                                </div>
                            </div>
                        </div>
                        <div class="NewsList">
                            <h1 class="c-sgray ta-l fw-bd PostTitle m-0"><?=$baiviet['name']?></h1><img class="PostImg"
                                src="<?=$baiviet['hinh_anh']?>" alt="<?=$baiviet['name']?>">
                            <div class="PostContent ta-l">
                                <div class="FCC">
                                    <div><?=$baiviet['mo_ta']?></div>
                                    <hr>
                                    <br>
                                    <div>
                                    <?php
                                      $toc = functions::TableOfContents($baiviet['noi_dung']);
                                      if($toc!='') {
                                          echo '<h2>Nội dung bài viết:</h2>';
                                          echo $toc;
                                      }
                                    ?>
                                    </div>
                                    <div><?=$baiviet['noi_dung']?>
                                    </div>
                                    <div>
<!--                                    <hr>-->
<!--                                        Người đăng: --><?//=$baiviet['nhanvien']?><!-- <br>-->
<!--                                        Ngày đăng: --><?//=$baiviet['ngaydang']?>
                                    </div>
                                    <!--  <br>
                                    <div>Đây là cơ hội tuyệt vời để con thể hiện bản thân, trau dồi khả năng tiếng Anh
                                        và hình thành sự tự tin. Vì vậy mong các quý vị phụ huynh cùng phối hợp với giáo
                                        viên của con để hỗ trợ con hoàn thành xuất sắc phần thi của mình.</div>
                                    <hr>
                                    <div>✨✨Để được hỗ trợ tốt nhất quý phụ huynh vui lòng liên hệ theo:</div> <br>
                                    <div>Hotline: <span class="c-bo">0325 135 112</span></div> <br>
                                    <div>Hoặc Website: <a href="http://igems.com.vn">igems.com.vn</a></div>
                                    <div>#IGEMS #ENGLISH #COACHING</div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<?php }else{
  echo '<h1>Nội dung đang được cập nhật!</h1>';
} ?>
