<div class="BG"></div>
            <div class="BodyPage" id='BodyPage'>
                <div class="BodySub bc-s Zoom">
                <h1 class="d-none"><?=$page['title'] ?></h1>
                    <h2 class="fsz-u m-0 fw-n HCT HVT c-bo">Hướng dẫn cài đặt và sử dụng Zoom meeting</h2>
                    <div class="ZoomContent fs-20 c-sgray">
                        <div class="d-flex ZT"><img src="template/data/img/zoom1.png" alt="x" class="ZI1">
                            <div class="p-20">Click vào đường link: http://zoom.us, nhấn vào menu “JOIN A MEETING”, sau
                                đó nhập số phòng được cung cấp trước các buổi Học vào phần “Meeting ID or Personal link
                                name”, ấn tiếp vào “Join” là bạn đã vào phòng học rồi.</div>
                        </div>
                        <h2 class="fsz-u m-0 fw-n HCT HVT c-sgray">MỘT SỐ TÍNH NĂNG CƠ BẢN CỦA PHẦM MỀM ZOOM</h2>
                        <hr><h3 class="fsz-u m-0">1. Tính năng Mute/Unmute (tắt / bật mic)</h3><img src="template/data/img/zoom2.png" alt="x"
                            class="ZI2"><h3 class="fsz-u m-0">2. Tính năng Stop Video/Start Video (tắt / bật video)</h3><img
                            src="template/data/img/zoom3.png" alt="x" class="ZI2"><h3 class="fsz-u m-0">3. Tính năng share (chia sẻ)</h3>
                        <div class="d-flex ZT"><img src="template/data/img/zoom4.png" alt="x" class="ZI3">
                            <div class="p-20">Nhấn vào nút “Share” để thực hiện tính năng chia sẻ của phần mềm tương tác
                                Zoom. Trong đó:</div>
                        </div>
                        <div class="d-flex ZT">
                            <div class="p-20">
                                <p>– Screen: Chia sẻ màn hình Zoom</p>
                                <p>– Whiteboard: Chia sẻ màn hình bẳng trắng (Khi người tham gia viết, vẽ...)</p>
                                <p>– Browser: Chia sẻ màn hình website</p>
                            </div><img src="template/data/img/zoom5.png" alt="x" class="ZI2">
                        </div>
                        <div class="p-20">Sau khi ấn nút Share, có 3 lựa chọn như sau:</div>
                        <div class="d-flex ZT"><img src="template/data/img/zoom6.png" alt="x" class="ZI2"></div>
                        <div class="p-20">
                            <p>– <b>New Share</b>: Chia sẻ nội dung thông tin mới.</p>
                            <p>– <b>Pause Share</b>: Dừng màn hình / nội dung đang được chia sẻ.</p>
                            <p>– <b>Stop Share</b>: Thoát tính năng Share để về màn hình Zoom.</p>
                        </div><h3 class="fsz-u m-0">4. Tính năng Chat( nhắn tin)</h3>
                        <div class="d-flex ZT"><img src="template/data/img/zoom7.png" alt="x" class="ZI3">
                            <div class="p-20">Tính năng Chat hỗ trợ học viên / giáo viên gửi thông tin trong phạm vi lớp
                                học. Người tham gia có thể gửi tin nhắn riêng cho một thành viên (privately) hoặc cho
                                tất cả thành viên tham gia cuộc họp (everyone).</div>
                        </div><img src="template/data/img/zoom8.png" alt="x" class="ZI1"><h3 class="fsz-u m-0">5. Tính năng Raise Hand
                            (giơ tay phát biểu)</h3>
                        <div class="d-flex ZT">
                            <div class="p-20">Để sử dụng tính năng Raise Hand, bấm vào nút Participants. Sau đó nhìn
                                sang phần Participants bên tay phải sẽ thấy nút ” Raise Hand” ở cuối thanh cuối cùng.
                            </div><img src="template/data/img/zoom9.png" alt="x" class="ZI1">
                        </div><h3 class="fsz-u m-0">6. Kết thúc buổi học.</h3>
                        <div class="p-20">Nhấn vào nút “Leave meeting” ở cuối màn hình để kết thúc buổi học.</div>
                        <div class="d-flex ZT"><img src="template/data/img/zoom10.png" alt="x" class="ZI1"></div>
                        <hr>
                    </div>
                </div>