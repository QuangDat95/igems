<?php
$thisurl  = isset($_GET['url']) ? $_GET['url'] : null;
$url  = rtrim($thisurl, '/');
$url  = explode('/', $thisurl);
$data = new model();
if ($url[0]!='') {
    if ($url[0]=='blog')
        if (sizeof($url)==3)
            $view = 'danhmuc';
        elseif (sizeof($url)==2)
            $view = 'baiviet';
        else
            $view = 'cateblog';
    elseif ($url[0]=='khoahoc')
        if (sizeof($url)==3)
            $view = 'cateblog';
        else
            $view = 'khoahoc';
    else
         $view = $url[0];
}
else
    $view = 'index';
$page = $data->pageinfo($url);
if (isset($_SESSION['en']) && $_SESSION['en']>0) {
    $thongtin = $data->thongtin_en();
}else{
    $thongtin = $data->thongtin();
}
$video = $data->video();
// $review = $data->getReview();
$menu =$data->topmenu();
if (file_exists('views/'.$view.'.php')){
    require('layout/header.php');
    require 'views/'.$view.'.php';
    require('layout/footer.php');
}
else{
    require 'views/err.php';
}
?>
