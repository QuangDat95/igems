<?php
   $thisurl  = isset($_GET['url']) ? $_GET['url'] : null;
   $danhmuc= $data->getdanhmuc();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <base href="<?=HOME?>/">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='robots' content='index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1' />
    <link rel="icon" href="template/favicon.ico"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $page['title'] ?></title>
    <meta name="description" content="<?php echo strip_tags($page['description'])?>"/>
    <meta name="keywords" content="<?php echo $page['keywords']?>">
    <link rel="canonical" href="<?=HOME?>" />
    <meta property="og:type" content="website">
    <meta property="og:title" content="<?php echo $page['title'] ?>">
    <meta property="og:description" content="<?php echo strip_tags($page['description'])?>">
    <meta property="og:url" content="<?=HOME.'/'.$thisurl?>">
    <meta property="og:site_name" content="<?php echo $page['title'] ?>">
    <meta property="og:updated_time" content="<?=date('Y-m-d H:i:s')?>">
    <meta property="og:image" content="<?php echo $page['image'] ?>" />
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="<?php echo $page['title'] ?>">
    <meta name="twitter:description" content="<?php echo strip_tags($page['description'])?>">
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v11.0&appId=2861959690584988&autoLogAppEvents=1" nonce="xHfgo18x"></script>
    <link rel="stylesheet" href="template/index.css">
    <link rel="stylesheet" href="template/App.css">
    <link rel="stylesheet" href="template/Home.css">
    <script src="https://kit.fontawesome.com/9d889a3143.js" crossorigin="anonymous"></script>
    <script type="application/ld+json">
    {
      "@context": "https://schema.org/",
      "@type": "Thing",
      "name": "<?php echo $page['title'] ?>",
      "subjectOf": {
        "@type": "Book",
        "name": "<?php echo $page['title'] ?>"
      }
    }
    </script>
    <!-- Global site tag (gtag.js) - Google Ads: 372408052 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-372408052"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-372408052');
    </script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-F5P7438DLX"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-F5P7438DLX');
</script>
</head>

<body>
    <div id='root'>
        <div id="header_body">
            <div id="header_top">
            <div id="header_top_all">
                <a id="logo" href="<?php echo HOME; ?>"><img src="<?php echo HOME; ?>/template/img/logo.png" width="200"></a>
                <div id="menu_top">
                    <ul style="list-style:none">
                        <li><a href="<?=HOME?>"><i class="fas fa-home NBCIcon" aria-hidden="true"></i>Trang chủ</a></li>
                        <li>
                            <a href="about"><i class="fas fa-info-circle NBCIcon" aria-hidden="true"></i>Giới thiệu</a>
                            <ul class="sub_menu">
                                <li><a href="about">Về Igems</a></li>
                                <li><a href="tamnhin">Tầm nhìn, Sứ mệnh</a></li>
                                <li><a href="daotaonhansu">Đào tạo nhân sự</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="phuongphaphoc"><i class="fas fa-map-signs NBCIcon" aria-hidden="true"></i>Các phương pháp</a>
                            <ul class="sub_menu">
                                <li><a href="phuongphaphoc">Phương pháp học</a></li>
                                <li><a href="phuongphapgiangday">Phương pháp giảng dạy</a></li>
                                <li><a href="zoom">Phần mềm ZOOM</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="blog/1/khoa-hoc"><i class="fas fa-map-signs NBCIcon" aria-hidden="true"></i>Khoá học</a>
                            <ul class="sub_menu">
                                <li><a href="khoahoc/chuong-trinh-coaching-english-1-1-cho-nguoi-di-lam">Tiếng anh giao tiếp 1 kèm 1</a></li>
                                <li><a href="khoahoc/chuong-trinh-coaching-english-1-1-cho-tre-tu-5-15-tuoi">Tiếng anh trẻ em 1 kèm 1</a></li>
                                <li><a href="khoahoc/trai-he-quoc-te-supercamp-trai-he-ban-quyen-my-tai-viet-nam">Trại hè SuperCamp</a></li>
                            </ul>
                        </li>
                        <li>
                            <a style="cursor: pointer"><i class="fas fa-newspaper NBCIcon" aria-hidden="true"></i>Tin tức</a>
                            <ul class="sub_menu">
                                <?php
                                    foreach ($danhmuc as $key => $value) {
                                        ?>
                                        <li><a href="blog/1/<?php echo $value['url'];?>"><?php echo $value['name']; ?></a></li>
                                        <?php
                                    }
                                ?>
                            </ul>
                        </li>
                        <li><a href="contact"><i class="fas fa-address-card NBCIcon" aria-hidden="true"></i>Liên hệ</a></li>
                    </ul>
                </div>
                <div id="hotline_header">
                    <a href="dangkyhocthu"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Đăng ký học thử</a>
                    <span><i class="fa fa-mobile" aria-hidden="true"></i> <?= $thongtin[2]['value'] ?></span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
        <div class="App">
            <div class="NavBar" id='NavBar'>
                <a class="NBI Logo" href="<?=HOME?>"></a>
                   <i id="NavShort" class="NavShort fas fa-bars" aria-hidden="true"></i>
                   <div class="Menu">
                     <a class="NBI p-r" href="<?=HOME?>">
                       <div class="NBB"></div>TRANG CHỦ
                     </a>
                     <div class="NBIOut">
                       <a class="NBI" href="about">
                         <div class="NBB"></div>GIỚI THIỆU
                       </a>
                       <div class="NBS NGT">
                         <a class="NBST" href="about">
                           Về IGEMS
                         </a>
                         <a class="NBST NBSTE" href="tamnhin">
                           Tầm nhìn, Sứ mệnh
                         </a>
						 <a class="NBST NBSTE" href="daotaonhansu">
                           Đào tạo nhân sự
                         </a>
                       </div>
                     </div>
                     <div class="NBIOut">
                       <a class="NBI" href="phuongphaphoc">
                         <div class="NBB"></div>CÁC PHƯƠNG PHÁP
                       </a>
                       <div class="NBS NPP">
                         <a class="NBST" href="phuongphaphoc">
                           Phương pháp học
                         </a>
                         <a class="NBST" href="phuongphapgiangday">
                           Phương pháp giảng dạy
                         </a>
                         <a class="NBST NBSTE" href="zoom">
                           Phần mềm ZOOM
                         </a>
                       </div>
                     </div>
					 <div class="NBIOut">
                       <a class="NBI" href="blog/1/khoa-hoc">
                         <div class="NBB"></div>KHÓA HỌC
                       </a>
                       <div class="NBS NPP">
                         <a class="NBST" href="blog/chuong-trinh-coaching-english-1-1-cho-nguoi-di-lam">
                           Tiếng anh giao tiếp 1 kèm 1
                         </a>
                         <a class="NBST" href="blog/chuong-trinh-coaching-english-1-1-cho-tre-tu-5-15-tuoi">
                           Tiếng anh trẻ em 1 kèm 1
                         </a>
                         <a class="NBST NBSTE" href="blog/trai-he-quoc-te-supercamp-trai-he-ban-quyen-my-tai-viet-nam">
                           Trại hè SuperCamp
                         </a>
                       </div>
                     </div>
                     <div class="NBIOut">
                       <a class="NBI" href="#">
                         <div class="NBB"></div>TIN TỨC
                       </a>
                       <div class="NBS NTT">
                        <?php foreach ($danhmuc as $key => $value) {
                           echo '<a class="NBST" href="blog/1/'.$value['url'].'">
                          '.$value['name'].'
                         </a>';
                        } ?>
                         <!-- <a class="NBST" href="blog">
                           Tin tức và Sự kiện
                         </a>
                         <a class="NBST NBSTE" href="/news/cam-nhan-ve-igems">
                           Cảm nhận về IGEMS
                         </a> -->
                       </div>
                     </div>
                     <a class="NBI p-r" href="contact">
                       <div class="NBB"></div>LIÊN HỆ
                     </a>

                     <div class="NBI p-r" id="RegisterButton">
                       <div class="NBB"></div>ĐĂNG KÝ NGAY
                     </div>
                     <!-- <a href="https://igems.com.vn/blog/huong-dan-thanh-toan" class="NBI p-r">
                       <div class="NBB"></div>THANH TOÁN
                     </a> -->
                   </div>
            </div>

            <!-- SBar -->
            <div class="SBar" id='SBar'></div>
            <!-- NBCross -->
            <div id='NavBarCross' class="d-flex fd-c NavBarCross r-300">
               <input type="hidden" name="sl" id="sl" value="<?=count($danhmuc)?>">
                <a class="NBCI" href="<?=HOME?>"><i
                  class="fas fa-home NBCIcon" aria-hidden="true"></i>TRANG CHỦ</a>
               <div class="NBCI" id='NBCIGioithieu'><i class="fas fa-info-circle NBCIcon" aria-hidden="true"></i>GIỚI
                  THIỆU<i class="fas fa-sort-down pos-r" aria-hidden="true"></i>
               </div>
               <div class="NBCS" id='NBCSGioithieu' style="height: 0px;">
                  <a class="NBCI NBCISub" href="about">Về IGEMS</a>
                  <a class="NBCI NBCISub" href="tamnhin">Tầm nhìn, Sứ mệnh</a>
				  <a class="NBCI NBCISub" href="daotaonhansu">Đào tạo nhân sự</a>
               </div>
               <div class="NBCI" id='NBCICacpp'><i class="fas fa-map-signs NBCIcon" aria-hidden="true"></i>CÁC PHƯƠNG
                  PHÁP<i class="fas fa-sort-down pos-r" aria-hidden="true"></i>
               </div>
               <div class="NBCS" id='NBCSCacpp' style="height: 0px;"><a class="NBCI NBCISub"
                  href="phuongphaphoc">Phương pháp
                  học</a><a class="NBCI NBCISub" href="phuongphapgiangday">Phương pháp giảng dạy</a><a
                     class="NBCI NBCISub" href="zoom">Phần mềm ZOOM</a>
               </div>
			  <div class="NBCI" id='NBCIKhoaHoc'><i class="fas fa-map-signs NBCIcon" aria-hidden="true"></i>KHÓA HỌC<i class="fas fa-sort-down pos-r" aria-hidden="true"></i>
               </div>
               <div class="NBCS" id='NBCSKhoaHoc' style="height: 0px;">
                  <a class="NBCI NBCISub" href="blog/chuong-trinh-coaching-english-1-1-cho-nguoi-di-lam">Tiếng anh giao tiếp 1 kèm 1</a>
                  <a class="NBCI NBCISub" href="blog/chuong-trinh-coaching-english-1-1-cho-tre-tu-5-15-tuoi">Tiếng anh trẻ em 1 kèm 1</a>
				  <a class="NBCI NBCISub" href="blog/trai-he-quoc-te-supercamp-trai-he-ban-quyen-my-tai-viet-nam">Trại hè SuperCamp</a>
               </div>
               <div class="NBCI" id='NBCITintuc'><i class="fas fa-newspaper NBCIcon" aria-hidden="true"></i>TIN TỨC<i
                  class="fas fa-sort-down pos-r" aria-hidden="true"></i></div>
               <div class="NBCS" id='NBCSTintuc' style="height: 0px;">
                  <?php foreach ($danhmuc as $key => $value) {
                      echo '<a class="NBCI NBCISub"
                  href="blog/1/'.$value['url'].'">'.$value['name'].'</a>';
                  } ?>
               </div>
               <a class="NBCI" href="contact"><i class="fas fa-address-card NBCIcon" aria-hidden="true"></i>LIÊN
               HỆ</a>


               <!-- <a
                  href="https://igems.com.vn/thanhtoan" class="NBCI"><i class="fas fa-school NBCIcon"
                  aria-hidden="true"></i>THANH TOÁN</a> -->
            </div>
