<!-- Footer -->
<div class="Footer" id='Footer'>
    <div class="F0 F1"><a class="Logo2" href="<?= HOME ?>"></a>
        <div class="RO"></div>
        <div>
            <div class="RO"><i class="fas fa-building ROI" aria-hidden="true"></i>
                <div><b>Trụ sở chính: </b><?= $thongtin[1]['value'] ?></div>
            </div>
            <div class="RO"><i class="fas fa-phone-alt ROI" aria-hidden="true"></i><b>Hotline:&nbsp;</b><?= $thongtin[2]['value'] ?></div>
            <div class="RO"><i class="fas fa-envelope ROI" aria-hidden="true"></i><b>Email:&nbsp;</b><?= $thongtin[3]['value'] ?></div>
        </div>
        <div class="FooterContact"></div>
        <div class="Social"><a href="<?= $thongtin[12]['value'] ?>"><i class="fab fa-facebook c-w" aria-hidden="true"></i></a><a href="<?= $thongtin[10]['value'] ?>"><i class="fab fa-youtube c-w" aria-hidden="true"></i></a></div>
    </div>
    <div class="F0 F2">
        <div class="F21">
            <div class="F2.1">
                <div><b>PHƯƠNG PHÁP</b></div><a href="phuongphaphoc">
                    <div class="c-w">Phương pháp học</div>
                </a><a href="phuongphapgiangday">
                    <div class="c-w">Phương pháp giảng dạy</div>
                </a><a href="zoom">
                    <div class="c-w">Phần mềm Zoom</div>
                </a><a href="<?= HOME ?>"><b class="c-w">GEMS EDU</b></a><a href="contact">
                    <div class="c-w fw-bd">LIÊN HỆ</div>
                </a>
            </div>
            <div class="F2.2">
                <div><b>GIỚI THIỆU</b></div><a href="about">
                    <div class="c-w">Về IGEMS</div>
                </a>
                <!-- <a href="giaovien">
                                    <div class="c-w">Giáo viên IGEMS</div>
                                </a> -->
                <div><b>TIN TỨC</b></div><a href="blog/1/blog">
                    <div class="c-w">Tin tức và sự kiện</div>
                </a><a href="giaovien">
                    <div class="c-w">Giáo viên IGEMS</div>
                </a>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Go Top -->
<div id='GoTop' class="GoTop" title="Go to Top"><i class="fas fa-arrow-up" aria-hidden="true"></i></div>
<!-- FormRegister -->
<div id='FullRegister' class="FullRegister d-none">
    <!--  <div class="FormRegister FFR" id='FullFormRegister'>
                        <div class="FFRT">ĐĂNG KÝ HỌC THỬ MIỄN PHÍ</div><input name="name" class="FRI" type="text"
                            placeholder="Tên">
                        <div class="c-r"></div><input name="age" class="FRI" type="number" placeholder="Tuổi">
                        <div class="c-r"></div><input name="email" class="FRI" type="text" placeholder="Email">
                        <div class="c-r"></div><input name="phone" class="FRI" type="number"
                            placeholder="Số điện thoại">
                        <div class="c-r"></div>
                        <div class="FRB">ĐĂNG KÝ</div>
                    </div> -->
</div>
<div id="zalo-link">
    <a href="https://zalo.me/<?= str_replace(' ','',$thongtin[2]['value']) ?>"><img src="//theme.hstatic.net/1000327709/1000419436/14/zalo-icon.png?v=1492"></a>
</div>
<div id="call-link">
    <a href="tel:<?= str_replace(' ','',$thongtin[2]['value']) ?>"><img src="//theme.hstatic.net/1000327709/1000419436/14/phone-symbol.png?v=1492"></a>
</div>

</div>
</div>
<script src="template/data.js" type="module"></script>
<script src="template/index.js" type="module"></script>
<!-- Messenger Plugin chat Code -->
<div id="fb-root"></div>

<!-- Your Plugin chat code -->
<div id="fb-customer-chat" class="fb-customerchat">
</div>

<script>
    var chatbox = document.getElementById('fb-customer-chat');
    chatbox.setAttribute("page_id", "106910804552617");
    chatbox.setAttribute("attribution", "biz_inbox");

    window.fbAsyncInit = function() {
        FB.init({
            xfbml: true,
            version: 'v11.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

</html>